﻿CREATE DATABASE DTT

GO
SET DATEFORMAT DMY

GO
USE DTT

GO
CREATE TABLE ACCOUNT
(
	Username VARCHAR(100) PRIMARY KEY,
	Password VARCHAR(100) NOT NULL
)

GO
CREATE TABLE ROUND
(
	RoundID TINYINT PRIMARY KEY,
	Name NVARCHAR(100) NOT NULL
)

GO
CREATE TABLE QUESTIONTYPE
(
	QuestionTypeID INT PRIMARY KEY,
	RoundID TINYINT REFERENCES ROUND(RoundID) NOT NULL,
	Points INT NOT NULL,
	Description NVARCHAR(1000),
)

GO
CREATE TABLE QUESTION
(
	QuestionID INT IDENTITY(1,1) PRIMARY KEY,
	Detail NVARCHAR(1000) NOT NULL,
	Answer NVARCHAR(1000) NOT NULL,
	QuestionTypeID INT REFERENCES QUESTIONTYPE(QuestionTypeID) NOT NULL,
	Note NVARCHAR(1000)
)

GO
CREATE TABLE RECORD
(
	RecordID INT IDENTITY(1,1) PRIMARY KEY,
	QuestionID INT REFERENCES QUESTION(QuestionID) NOT NULL,
	Contestant NVARCHAR(1000) NOT NULL,
	Result TINYINT NOT NULL
)

GO
INSERT INTO ACCOUNT(Username, Password) VALUES ('admin','19A2854144B63A8F7617A6F225019B12')

GO
INSERT INTO ROUND(RoundID, Name) VALUES (1, N'Khởi động')
INSERT INTO ROUND(RoundID, Name) VALUES (2, N'Vượt chướng ngại vật')
INSERT INTO ROUND(RoundID, Name) VALUES (3, N'Tăng tốc')
INSERT INTO ROUND(RoundID, Name) VALUES (4, N'Về đích')
INSERT INTO ROUND(RoundID, Name) VALUES (5, N'Giải mã')

GO
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (1, 1, 10, N'Câu hỏi khởi động')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (100, 1, 10, N'Câu hỏi khởi động dự phòng')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (2, 2, 0, N'Câu hỏi VCNV')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (3, 2, 0, N'Câu hỏi từ khóa VCNV')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (41, 3, 0, N'Câu hỏi Tăng tốc (ảnh)')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (42, 3, 0, N'Câu hỏi Tăng tốc (video)')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (400, 3, 0, N'Câu hỏi Tăng tốc dự phòng')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (5, 4, 10, N'Về đích 10 điểm gói 40')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (15, 4, 10, N'Về đích 10 điểm gói 60')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (6, 4, 20, N'Về đích 20 điểm gói 40')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (16, 4, 20, N'Về đích 20 điểm gói 60')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (26, 4, 20, N'Về đích 20 điểm gói 80')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (17, 4, 20, N'Về đích 30 điểm gói 60')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (27, 4, 30, N'Về đích 30 điểm gói 80')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (500, 4, 10, N'Về đích 10 điểm dự phòng')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (600, 4, 20, N'Về đích 20 điểm dự phòng')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (700, 4, 30, N'Về đích 30 điểm dự phòng')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (8, 5, 5, N'Giải mã ô xanh 5 điểm')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (9, 5, 10, N'Giải mã ô vàng 10 điểm')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (10, 5, 15, N'Giải mã ô đỏ 15 điểm')
INSERT INTO QUESTIONTYPE(QuestionTypeID, RoundID, Points, Description) VALUES (11, 5, 0, N'Giải mã ô gợi ý')

/* Khởi động */
/* Thí sinh 1 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Bạn cần phải nhấn tổ hợp nào để căn giữa trang văn bản trong MS Word?', N'Ctrl + E', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Quốc gia nào được mệnh danh là "Xứ sở vạn đảo"?', N'Indonesia', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Các nguyên tố từ natri đến clo, theo chiều tăng của điện tích hạt nhân thì bán kính nguyên tử thay đổi thế nào?', N'Bán kính nguyên tử giảm dần', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Which country did the first person ever to journey into outer space come from?', N'Russia/ USSR', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cá sấu thuộc lớp động vật nào?', N'Bò sát', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Phần thưởng cao nhất của chương trình "Ai là triệu phú" hiện nay là bao nhiêu?', N'150 triệu (đồng)', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Số 50 trong phép ghi của số La Mã được biểu diễn bằng chữ cái nào?', N'L', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tên của một loại thơ độc đáo của Nhật, thường chỉ có 17 âm tiết?', N'Thơ Haiku', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Bài hát "Mặt trời bé con" do nhạc sĩ nào sáng tác?', N'Trần Tiến', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ai là người bác bỏ quan điểm: "vật nặng rơi nhanh hơn vật nhẹ"?', N'Ga-li-le', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Nữ hoàng đế duy nhất trong lịch sử phong kiến của nước ta là vợ của vị  vua nào?', N'Trần Cảnh/Trần Thái Tông', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Nhà máy Formosa đặt tại huyện nào của tỉnh Hà Tĩnh?', N'Huyện Kỳ Anh', 1)

/*Thí sinh 2 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hình thái kinh tế-xã hội đầu tiên của loài người là gì?', N'Công xã nguyên thủy', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Nhịp 4/4 của một bản nhạc được kí hiệu bằng chữ cái gì?', N'C', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Có bao nhiêu vecto khác vecto 0 được tạo ra từ 3 điểm phân biệt ?', N'6', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hợp chất của kim loại nào được sử dụng trong sản xuất ruột phích nước?', N'Bạc', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hãy cho biết loại tế bào máu nào có chức năng sinh kháng thể và thực bào để bạo vệ cơ thể?', N'Bạch cầu', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Đơn vị đo điện dung được kí hiệu là gì?', N'F (Fara)', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cây olive là loài cây đặc trưng cho vùng nào trên thế giới?', N'Địa Trung Hải', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'In chemistry, there are three familiar "states of matter": Gas, liquid and ...', N'Solid', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tập thơ Nhật kí trong tù được Nguyễn Ái Quốc viết bằng chữ gì?', N'Chữ Hán', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hệ điều hành là phần cứng, phần mềm hệ thổng hay phần mềm ứng dụng?', N'Phần mềm hệ thống', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Nhà soạn nhạc nào là tác giả của "Bản giao hưởng định mệnh"', N'Beethoven/ Ludwig van Beethoven', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ai là vận động viên sở hữu nhiều HCV cá nhân nhất ở các kì Olympic cho đến thời điểm hiện tại?', N'Michael Phelps', 1)

/* Thí sinh 3 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Danh họa nổi tiếng Van Gogh là người nước nào?', N'Hà Lan', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ca sĩ nào là đại diện của Việt Nam tham dự Asia Song Festival 2016?', N'Noo Phước Thịnh', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Chùm thơ "Tự tình" của Hồ Xuân Hương gồm bao nhiêu bài?', N'3 bài', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'"Núi Một" là tên gọi khác của ngọn núi nào?', N'Núi Bà Đen (Tây Ninh)', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Mặt trời tồn tại ở dạng vật chất nào?', N'Plasma', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Sự chênh lệch giữa số người xuất cư và nhập cư của một quốc gia được gọi là gì?', N'Gia tăng cơ học', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cho biết dạng lai hóa của nguyên tử oxi trong phân tử nước?', N'Lai hóa sp3', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'What does the A in abbreviation ATM stand for?', N'Automated/ Automatic', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Chế độ phân biệt chủng tộc ở Nam Phi có tên là gì?', N'Apartheid', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trong môn bóng chuyền, mỗi đội được phép chạm bóng tối đa mấy lần liên tiếp?', N'3', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'2 tam giác đồng dạng với tỉ số k . Tìm tỉ số chu vi và diện tích của 2 tam giác đó?', N'k, k^2.', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Đơn phân cấu tạo nên protein là gì?', N'Axit amin', 1)

/* Thí sinh 4 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trong quá trình tăng trưởng của cơ thể người, 2 giai đoạn có tốc độ phát triển thể chất mạnh nhất là thời kì sau khi sinh và thời kì nào?', N'Thời kì dậy thì', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Which continent has the largest population?', N'Asia', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Bộ phim nào được chuyển thể từ tiểu thuyết cùng tên đã giành giải thưởng Bông sen vàng tại Liên hoan phim Việt Nam lần thứ 19', N'Tôi thấy hoa vàng trên cỏ xanh', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trong chiến tranh thế giới lần thứ nhất, khối quân sự đối đầu với phe Liên minh gọi là gì?', N'Phe Hiệp ước', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Năng lượng của một tụ điện tích điện là dạng năng lượng gì?', N'Năng lượng điện trường', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trên bàn phím laptop, có 2 phím có gờ nổi lên để giúp người sử dụng định vị ngón khi gõ, 2 phím đó là 2 phím nào?', N'F và J', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Bài thơ "Cảnh ngày hè" do ai sáng tác?', N'Nguyễn Trãi', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cánh đồng Chum là khu vực văn hóa lịch sử của quốc gia nào?', N'Lào', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Vecto khác vecto 0 và vuông góc với vecto chỉ phương của đường thẳng d thì được gọi là gì của đường thẳng d?', N'Vecto pháp tuyến', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Khi tiến hành nhiệt phân hoàn toàn Fe(NO3)2 các sản phẩm thu được là gì?', N'Fe2O3, NO2 và O2', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Đến thời điểm hiện tại, quốc gia nào đang giữ quyền đăng cai tổ chức World cup 2018', N'Nga', 1)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ai đóng vai dì ghẻ trong bộ phim "Tấm Cám: Chuyện chưa kể"?', N'Ngô Thanh Vân', 1)

/* Vượt chướng ngại vật */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Biểu tượng của hoàng gia của đất nước này là bông hoa cúc và trong lịch sử, đất nước của họ còn có tên gọi là Phù Tang. Đây là nước nào?', N'NHẬT BẢN', 2)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Theo lời của Chủ tịch Hồ Chí Minh thì cái gì là thứ quý nhất?', N'ĐỘC LẬP', 2)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Vị vua cuối cùng của nhà Nguyễn có tên là gì?', N'BẢO ĐẠI', 2)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tổ chức này được thành lập vào ngày 19 tháng 5 năm 1941 với mục đích Liên hiệp tất cả các tầng lớp dân chúng yêu nước đánh đuổi Nhật - Pháp, làm cho Việt Nam hoàn toàn độc lập. Đây là tổ chức nào?', N'VIỆT MINH', 2)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Chiến tranh thế giới thứ hai kết thúc vào năm nào?', N'1945', 3)

/* Tăng tốc */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Sắp xếp tên các quốc gia tương ứng với các kì quan thế giới cổ đại sau:', N'ACDB', 41)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Đây là quốc gia nào?', N'Pháp', 42)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tìm hình phù hợp với quy luật để điền vào chỗ trống', N'E', 41)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Sắp xếp hình sau theo đúng thứ tự (trái sang phải, trên xuống dưới, 2x3) để hoàn chỉnh bức tranh', N'CEFDBA', 41)

/* Về đích */
/* Thí sinh 1 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Những câu thơ sau nói về dãy núi nào? "Một dãy núi mà hai màu mây/ Nơi nắng mưa, khí trời cũng khác"', N'dãy Trường Sơn', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'trong bảng tuần hoàn, Cs là kí hiệu của nguyên tố hóa học nào?', N'xêsi', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Theo thuyết Địa kiến tạo mảng, Trái Đất gồm bao nhiêu mảng kiến tạo chính?', N'8', 6)

INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trương Thanh Hằng là vận động viên trong môn thể thao nào?', N'điền kinh', 15)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Để mắc được bộ nguồn từ x nguồn giống nhau và điện trở của bộ nguồn bằng điện trở một nguồn thì số x phải là số như thế nào?', N'Số chính phương', 16)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'I really want to travel to the USA, and the first city I want to go if I have the chance to do so is New York, because I would like to see the Statue of Liberty firsthand. The question for you is, the Statue was built by whom? A hint for you is that the most famous tower in France,  which was also built by him, was named after him.', N'Eiffel ', 17)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hai ô tô cùng chuyển động thẳng đều trên một đoạn đường khởi hành đồng thời ở 2 địa điểm cách nhau 20km. Nếu đi ngược chiều thì sau 15 phút chúng gặp nhau. Nếu đi cùng chiều sau 30 phút thì chúng đuổi kịp nhau. Vận tốc của hai xe đó là bao nhiêu?', N'60km/h và 20km/h', 26)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'G7 is a group of seven leading industrialized nations of the world, including France, Germany, Italy, Japan, UK, USA and Canada. Until 2014, its name was G8 but one country was suspended from the group due to political conflicts with other members. Can you tell me what country is it?', N'Russia', 27)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Để bảo quản cho các trứng gia cầm được lâu, không bị hư, người ta thường ngâm chúng vào nước vôi trong, loãng. Đó là do 2 nguyên nhân chính nào?', N'Nước vôi trong có tính sát trùng,ngoài ra chúng còn phản ứng với khí Cacbonic thoát ra từ quá trình hô hấp, tạo thành Canxi cacbonat và chính những chất này sẽ bịt kín các lỗ nhỏ trên bề mặt trứng ngăn cho vi khuẩn không xâm nhập vào làm hư trứng ', 27)


/* Thí sinh 2 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ai được coi là người có công lớn nhất trong việc hình thành nên hệ thống chữ Quốc ngữ mà người Việt Nam sử dụng ngày nay?', N'Alexandre de Rhodes', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tác giả Thân Nhân Trung của bài "Hiền tài là nguyên khí quốc gia" là người thuộc triều đại nào?', N'Lê Sơ/ Hậu Lê', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cho đến nay, bảng tuần hoàn các nguyên tố hoá học gồm bao nhiêu nguyên tổ?', N'118', 6)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Carbon có mấy dạng thù hình, bao gồm những dạng nào?', N'3: Kim cương, than chì và carbon vô định hình', 15)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cấu trúc địa hình nước ta gồm hai hướng chính nào?', N'Hướng Tây Bắc - Đông Nam, hướng Vòng cung', 16)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Chess is a nerve-wrecking sport, and is definitely not suitable for careless people. In a chess table, each side starts with 16 pieces, named "king", "queen", "pawn", etc. So, can you tell me what is the name of the pair of chess pieces that located at the corner of a chess table at the beginning of the game?', N'Rooks (quân xe)', 17)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hai con vật nào được nhắc đến trong câu ca ca dao sau:..mà nuôi con …/Đến khi nó lớn nó quện nhau đi?', N'Tò vò - Nhện ', 26)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trần Cảnh là vị vua đầu tiên của triều đại nhà Trần. Tuy nhiên khi mất, ông lại được truy tôn là Trần Thái Tông, thay vì Thái Tổ như các vị vua khai sáng triều đại nhà Lý, nhà Mạc hay nhà Lê. Tại sao lại có sự khác biệt này?', N'Ông lên ngôi khi cha ông, Trần Thừa, đang còn sống, do đó dưới triều đại Trần Thái Tông thì cha ông làm Thái Thượng Hoàng và được truy phong là Thái Tổ.', 27)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'One of my favourite novels of all time is "The old man and the sea". The novel was written by Earnest Hemmingway, depicting the struggle of an old man trying to catch a big marlin. He received the Nobel Prize in Literature in 1954 for making this masterpiece. Can you tell me where did the story in this novel take place? ', N'Cuba', 27)


/* Thí sinh 3 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Hiện tượng vật lí gì làm cho các vật có màu sắc khác nhau?', N'Vật hấp thụ ánh sáng nhưng lại không hấp thụ tia sáng màu của vật, nên ánh sáng màu đó phản xạ lại và ta nhìn thấy chính là màu của vật đó', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Chứng xơ vữa động mạch do việc dư thừa chất nào gây ra?', N'Colesteron ', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Cho 4 chữ số: 1; 2; 3; 6. Viết được tất cả bao nhiêu số có ba chữ số khác nhau chia hết cho 3 từ 4 chữ số đã cho?', N'12', 6)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Kể tên mối quan hệ giữa các cá thể trong quần thể?', N'Hỗ trợ và cạnh tranh (Đối kháng) ', 15)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Vì sao hơ con dao ướt lên ngọn lửa, con dao sẽ có màu xanh?', N'Ở nhiệt độ cao, sắt và nước tác dụng với nhau tạo nên oxit sắt từ (Fe3O4) lấp lánh màu lam.', 16)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ai là chủ tịch đầu tiên của Mặt trận giải phóng Miền Nam Việt Nam?', N'Nguyễn Hữu Thọ', 17)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tìm thương của 2 số, biết thương đó gấp 7 lần số nhỏ nhưng chỉ bằng một nửa số lớn?', N'14', 26)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Nước ta có tên Cộng hòa Xã hội Chủ nghĩa Việt Nam vào ngày tháng năm nào?', N'Ngày 2-7-1976', 27)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'These waterfalls are one of the most famous sightseeing spots in the world, both for their beauty and as a valuable source of hydroelectric power It is located in the border between USA and Canada. Can you tell me the name of this waterfalls?', N'Niagara Falls', 27)

/* Thí sinh 4 */
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'"Đừng thấy bóng của mình trên tường rất to mà tưởng mình vĩ đại" là câu nói của nhà toán học cổ Hy Lạp nổi tiếng nào?', N'Py-ta-go', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Trong đoạn đầu của lời bài hát "Tháng ba Tây Nguyên" của nhạc sĩ Văn Thắng, có những con vật nào được nhắc đến?', N'Con ong và con voi', 5)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Tại sao các đồ dùng bằng bạc lại bị đen sau một khoảng thời gian sử dụng?', N'Bởi vì bạc tác dụng với hidro sulfua và carbon dioxide trong không khí tạo thành muối bạc sulfua không tan có màu đen kết trên bề mặt', 6)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Loại mô nào ở giữa hầu hết các xương, và cũng là nơi sản xuat hầu hết các tế bào máu', N'Mô tuỷ   ', 15)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Theo quan niệm của Nho giáo, "Tam Tòng, Tứ Đức" mà người phụ nữ Trung Quốc truyền thống bắt buộc phải thực hiện bao gồm những gì?', N'Tam tòng: Tại gia tòng phụ, Xuất giá tòng phu, Phu tử tòng tử; Tứ đức: Công, dung, ngôn, hạnh', 16)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Ông là một nhà giáo, thầy thuốc, đại quan thuộc triều đại nhà Trần trong lịch sử Việt Nam. Là người chính trực, đã từng đỗ Thái học sinh nhưng không ra làm quan mà mở trường dạy học, có công lớn trong việc truyền bá, giáo dục tư tưởng đạo đức Khổng giáo vào Việt Nam. Nổi tiếng nhất trong cuộc đời ông là sự kiện ông dâng Thất trảm sớ nhưng không được chấp nhận, nên ông từ quan về quê dạy học. Ông là ai?', N'Chu Văn An', 17)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Khi cắt mặt nón tròn xoay bằng một mặt phẳng thì ta có thể thu được những nghiệm hình nào?', N'đường tròn, hình elip, parabol, hyperbol và một điểm ', 26)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'Bốn đường thẳng chia mặt phẳng thành ít nhất và nhiều nhất bao nhiêu phần?', N'5 phần - 11 phần', 27)
INSERT INTO QUESTION(Detail, Answer, QuestionTypeID) VALUES (N'I''m a huge fan of this singer. He has been a famous singer since the era of Elvis Presley. Many of his songs were considered as anthems for America''s anti-Vietnam war movement, and he once had a concert in Vietnam in 2011. Also, he is the winner of this year''s Nobel Prize in Literature. Who is he?', N'Bob Dylan', 27)

/* Khởi động */
GO
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (1, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (2, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (3, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (4, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (5, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (6, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (7, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (8, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (9, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (10, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (11, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (12, N'TS1', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (13, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (14, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (15, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (16, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (17, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (18, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (19, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (20, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (21, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (22, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (23, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (24, N'TS2', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (25, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (26, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (27, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (28, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (29, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (30, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (31, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (32, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (33, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (34, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (35, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (36, N'TS3', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (37, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (38, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (39, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (40, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (41, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (42, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (43, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (44, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (45, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (46, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (47, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (48, N'TS4', 1)

/* Vượt chướng ngại vật */
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (49, N'VCNV', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (50, N'VCNV', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (51, N'VCNV', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (52, N'VCNV', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (53, N'VCNV', 1)

/* Tăng tốc */
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (54, N'TT', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (55, N'TT', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (56, N'TT', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (57, N'TT', 1)

/* Về đích */
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (58, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (59, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (60, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (61, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (62, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (63, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (64, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (65, N'TS1', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (66, N'TS1', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (67, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (68, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (69, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (70, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (71, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (72, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (73, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (74, N'TS2', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (75, N'TS2', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (76, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (77, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (78, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (79, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (80, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (81, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (82, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (83, N'TS3', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (84, N'TS3', 1)

INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (85, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (86, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (87, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (88, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (89, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (90, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (91, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (92, N'TS4', 1)
INSERT INTO RECORD(QuestionID, Contestant, Result) VALUES (93, N'TS4', 1)

GO
CREATE TABLE DECODEQUESTION
(
	RowNo TINYINT NOT NULL,
	ColNo TINYINT NOT NULL,
	CellType TINYINT NOT NULL,	/* 10: xanh, câu hỏi; 11: xanh, từ khóa; 20: vàng, câu hỏi; 21: vàng, từ khóa; 30: đỏ */
	Question NVARCHAR(1000),
	Answer NVARCHAR(1000),
	CONSTRAINT [PK_DECODEQUESTION] PRIMARY KEY CLUSTERED 
    ([RowNo] ASC, [ColNo] ASC)
)

INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (1, 1, 20, N'This country name can be understood as “the land which is below the sea level”. Which country is it?', N'Netherlands')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (1, 3, 10, N'What is the name of the ivory-white marble mausoleum build under India emperor Shah Jahal’s reign?', N'Taj Mahal')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (1, 4, 11, N'Gợi ý 3', N'Đáp án 3')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (1, 6, 20, N'Which country did the first person ever to journey into outer space come from?', N'Russia/ USSR')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (1, 8, 30, N'Tìm số tự nhiên nhỏ nhất sao cho khi nhân số đó với 15873 ta được một số gồm toàn chữ số 7. Số cần tìm là?', N'49')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (2, 6, 10, N'Kiểu dữ liệu số nguyên dương chỉ từ 0 đến 255 trong Pascal được kí hiệu là gì?', N'byte')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (2, 7, 30, N'Tương truyền, câu "Một vai gánh vác cả đôi sơn hà" nói về nhân vật lịch sử nào sống dưới thời Đinh-Tiền Lê?', N'Dương Vân Nga')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (3, 1, 11, N'Gợi ý 8', N'Đáp án 8')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (3, 3, 21, N'Gợi ý 9', N'Đáp án 9')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (3, 4, 11, N'Gợi ý 10', N'Đáp án 10')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (3, 6, 20, N'Ngọc trai là hỗn hợp của Protein và ...', N'CaCO3')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (3, 8, 20, N'Chữ Nôm là chữ viết cổ của người Việt, dựa vào chữ nào mà đặt ra?', N'Chữ Hán')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (4, 4, 10, N'Đây là định luật mang tên nhà bác học nào: "hiệu điện thế trên hai đầu vật dẫn luôn tỷ lệ thuận với cường độ dòng điện"?', N'Ôm')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (4, 7, 10, N'When we mix red with blue, what color will we get?', N'Violet/ Purple')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (5, 1, 30, N'Vị trạng nguyên nào đã làm bài phú Ngọn tỉnh liên (Hoa sen trong giếng ngọc) để ví mình như hoa sen?', N'Mạc Đĩnh Chi')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (5, 3, 10, N'When I say that you look good in photoshoots, that means you are...?', N'Photogenic')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (5, 5, 20, N'Trong hiện tượng thẩm thấu, nước luôn chuyển động về phía dung dịch... Nghĩa là về phía dung dịch có nồng độ chất hòa tan...', N'Ưu trương/ cao hơn')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (6, 4, 21, N'Gợi ý 18', N'Đáp án 18')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (6, 6, 11, N'Gợi ý 19', N'Đáp án 19')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (6, 7, 10, N'Một ống nghiệm hình trụ có một ít hơi brom. Muốn hơi thoát ra nhanh cần đặt ống đứng thẳng ngửa hay úp ngược ống?', N'Úp ngược ống (vì brom nặng hơn không khí)')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (6, 8, 20, N'Để tăng độ giòn và trong của bánh, dưa chua; làm mềm nhanh các loại đậu trắng, đậu đỏ... người ta thường dùng nước tro tàu. Thành phần của nước tro tàu là?', N'Hỗn hợp K2CO3 và Na2CO3')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (7, 1, 11, N'Gợi ý 22', N'Đáp án 22')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (7, 8, 30, N'Tìm chữ số tận cùng : 2017x2018 + 2018x2019 + 2019x2020 + 2020x2021 + 2021x2022', N'0')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (8, 1, 30, N'Mặt ngăn cách hai khối khí khác biệt nhau về tính chật vật lí có tên là gì?', N'Frông')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (8, 4, 11, N'Gợi ý 25', N'Đáp án 25')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (8, 5, 11, N'Gợi ý 26', N'Đáp án 26')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (9, 2, 10, N'Cấu trúc chương trình trong lập trình máy tính bao gồm:', N'Phần khai báo và phần thân')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (9, 3, 30, N'Sự thay đổi có quy luật của tất cả các thành phần địa lí và cảnh quan địa lí theo vĩ độ được gọi là quy luật gì?', N'Quy luật địa đới')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (9, 5, 20, N'Trần Tế Xương là tên thật của nhà thơ nổi tiếng nào?', N'Tú Xương')
INSERT INTO DECODEQUESTION(RowNo, ColNo, CellType, Question, Answer)
VALUES (9, 8, 21, N'Gợi ý 30', N'Đáp án 30')
