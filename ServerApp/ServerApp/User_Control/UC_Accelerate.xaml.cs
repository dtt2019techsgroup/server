﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using ServerApp.Slide.Accelerate_Round;
using ServerApp.Utilities;

namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UC_Accelerate.xaml
    /// </summary>
    public partial class UC_Accelerate : UserControl
    {
        public readonly MainWindow Main;
        private double TimeLeft = 30;
        bool first_time = true;
        EW_Accelerate eW_Accelerate = new EW_Accelerate();
        List<Student> students;
        public DateTime dateTime;
        public TimeSpan timeElapsed;
        public TimeSpan timeElapsed2;
        public TimeSpan timeElapsed3;
        public TimeSpan timeElapsed4;
        public float ftime1;
        public float ftime2;
        public float ftime3;
        public float ftime4;


        public struct AccelerateStudentInfo
        {
            public string name;
            public string answer;
            public float time;
            public AccelerateStudentInfo(string n, string a, float t)
            {
                name = n;
                answer = a;
                time = t;
            }
        }

        public List<AccelerateStudentInfo> SortedStudents = new List<AccelerateStudentInfo>();
        List<Question> questions;
        String answer = "ANSWER";
        public int cur_question = 1;

        //Class Dem Thoi Gian
        DispatcherTimer Timer = new DispatcherTimer();

        string videoPath = Directory.GetCurrentDirectory();
        public UC_Accelerate(MainWindow main, List<Student> students, EW_Accelerate ew)
        {
            Main = main;
            this.students = students;
            this.eW_Accelerate = ew;

            InitializeComponent();

            questions = new List<Question>();
            questions = DAO.QuestionDAO.Instance.getAccelerateQuestion();

            StartSound.Source = new Uri(videoPath + @"\Resources\3_AmThanhBatDau.mp3", UriKind.RelativeOrAbsolute);
            TrueSound.Source = new Uri(videoPath + @"\Resources\3_AmThanhTraLoiDung.mp3", UriKind.RelativeOrAbsolute);


            Timer.Interval = TimeSpan.FromMilliseconds(1000);   //Cho nay rang buoc (interval) theo Don Vi Thoi Gian
            Timer.Tick += WorksInTick;                       //Duoc chay khi Interval ket thuc  

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            AnswerStudentName1.Text = students[0].Name;
            AnswerStudentName2.Text = students[1].Name;
            AnswerStudentName3.Text = students[2].Name;
            AnswerStudentName4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        private void Intro_Play(object sender, RoutedEventArgs e)
        {
            eW_Accelerate.IntroVideo.Visibility = Visibility.Visible;
            eW_Accelerate.IntroVideo.Play();
        }

        private void Question1_Click(object sender, RoutedEventArgs e)
        {
            eW_Accelerate.ContentText.Visibility = Visibility.Hidden;

            Question1.IsEnabled = false;

            cur_question = 1;
            eW_Accelerate.IntroQuestion.Visibility = Visibility.Visible;
            eW_Accelerate.IntroQuestion.Play();
            eW_Accelerate.Question.Text = questions[cur_question - 1].Detail;
            Main.BroadcastData("03_01_" + cur_question + "_" + questions[cur_question - 1].Detail);
            if (questions[cur_question - 1].QuestionTypeID == 42)
            {
                eW_Accelerate.QuestionImage.Source = null;
                eW_Accelerate.QuestionVideo.Source = new Uri(videoPath + @"\Resources\3_Question1.mp4", UriKind.Relative);
            }
            else
            {
                eW_Accelerate.QuestionImage.Source = new BitmapImage(new Uri(videoPath + @"\Resources\3_QuestionImage1.jpg"));
                eW_Accelerate.QuestionVideo.Source = null;
            }
            Start.IsEnabled = true;
        }

        //Question 2: 
        private void Question2_Click(object sender, RoutedEventArgs e)
        {
            Question2.IsEnabled = false;

            cur_question = 2;
            eW_Accelerate.IntroQuestion.Visibility = Visibility.Visible;
            eW_Accelerate.IntroQuestion.Play();
            eW_Accelerate.Question.Text = questions[cur_question - 1].Detail;
            Main.BroadcastData("03_01_" + cur_question + "_" + questions[cur_question - 1].Detail);
            if (questions[cur_question - 1].QuestionTypeID == 42)
            {
                eW_Accelerate.QuestionImage.Source = null;
                eW_Accelerate.QuestionVideo.Source = new Uri(videoPath + @"\Resources\3_Question2.mp4", UriKind.Relative);
            }
            else
            {
                eW_Accelerate.QuestionImage.Source = new BitmapImage(new Uri(videoPath + @"\Resources\3_QuestionImage2.jpg"));
                eW_Accelerate.QuestionVideo.Source = null;
            }
            Start.IsEnabled = true;
        }

        //Question 3:
        private void Question3_Click(object sender, RoutedEventArgs e)
        {
            Question3.IsEnabled = false;

            cur_question = 3;
            eW_Accelerate.IntroQuestion.Visibility = Visibility.Visible;
            eW_Accelerate.IntroQuestion.Play();
            eW_Accelerate.Question.Text = questions[cur_question - 1].Detail;
            Main.BroadcastData("03_01_" + cur_question + "_" + questions[cur_question - 1].Detail);
            if (questions[cur_question - 1].QuestionTypeID == 42)
            {
                eW_Accelerate.QuestionImage.Source = null;
                eW_Accelerate.QuestionVideo.Source = new Uri(videoPath + @"\Resources\3_Question3.mp4", UriKind.Relative);
            }
            else
            {
                eW_Accelerate.QuestionImage.Source = new BitmapImage(new Uri(videoPath + @"\Resources\3_QuestionImage3.jpg"));
                eW_Accelerate.QuestionVideo.Source = null;
            }
            Start.IsEnabled = true;
        }

        //Question4

        private void Question4_Click(object sender, RoutedEventArgs e)
        {
            Question4.IsEnabled = false;

            cur_question = 4;
            eW_Accelerate.IntroQuestion.Visibility = Visibility.Visible;
            eW_Accelerate.IntroQuestion.Play();
            eW_Accelerate.Question.Text = questions[cur_question - 1].Detail;
            Main.BroadcastData("03_01_" + cur_question + "_" + questions[cur_question - 1].Detail);
            if (questions[cur_question - 1].QuestionTypeID == 42)
            {
                eW_Accelerate.QuestionImage.Source = null;
                eW_Accelerate.QuestionVideo.Source = new Uri(videoPath + @"\Resources\3_Question4.mp4", UriKind.Relative);
            }
            else
            {
                eW_Accelerate.QuestionImage.Source = new BitmapImage(new Uri(videoPath + @"\Resources\3_QuestionImage4.jpg"));
                eW_Accelerate.QuestionVideo.Source = null;
            }
            Start.IsEnabled = true;
        }

        private void ShowSolution_Click(object sender, RoutedEventArgs e)
        {
            if (questions[cur_question - 1].QuestionTypeID == 41)
            {
                string AnswerImagePath;
                switch (cur_question)
                {
                    case 1:
                        AnswerImagePath = videoPath + @"\Resources\3_QuestionAnswer1.jpg";
                        if (File.Exists(AnswerImagePath))
                            eW_Accelerate.QuestionAnswer.Source = new BitmapImage(new Uri(AnswerImagePath));
                        break;
                    case 2:
                        AnswerImagePath = videoPath + @"\Resources\3_QuestionAnswer2.jpg";
                        if (File.Exists(AnswerImagePath))
                            eW_Accelerate.QuestionAnswer.Source = new BitmapImage(new Uri(AnswerImagePath));
                        break;
                    case 3:
                        AnswerImagePath = videoPath + @"\Resources\3_QuestionAnswer3.jpg";
                        if (File.Exists(AnswerImagePath))
                            eW_Accelerate.QuestionAnswer.Source = new BitmapImage(new Uri(AnswerImagePath));
                        break;
                    case 4:
                        AnswerImagePath = videoPath + @"\Resources\3_QuestionAnswer4.jpg";
                        if (File.Exists(AnswerImagePath))
                            eW_Accelerate.QuestionAnswer.Source = new BitmapImage(new Uri(AnswerImagePath));
                        break;
                    default:
                        break;
                }
            }

            eW_Accelerate.ShowAns.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionVideo.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionImage.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionBackground.Visibility = Visibility.Visible;
            eW_Accelerate.Question.Visibility = Visibility.Visible;
            eW_Accelerate.QuestionAnswer.Visibility = Visibility.Visible;
        }

        private void ShowAnswers_Click(object sender, RoutedEventArgs e)
        {
            AccelerateStudentInfo asi1 = new AccelerateStudentInfo(students[0].Name, this.Answer1.Text, ftime1);
            AccelerateStudentInfo asi2 = new AccelerateStudentInfo(students[1].Name, this.Answer2.Text, ftime2);
            AccelerateStudentInfo asi3 = new AccelerateStudentInfo(students[2].Name, this.Answer3.Text, ftime3);
            AccelerateStudentInfo asi4 = new AccelerateStudentInfo(students[3].Name, this.Answer4.Text, ftime4);

            SortedStudents = new List<AccelerateStudentInfo>();

            SortedStudents.Add(asi1);
            SortedStudents.Add(asi2);
            SortedStudents.Add(asi3);
            SortedStudents.Add(asi4);

            SortedStudents.Sort((s1, s2) => s1.time.CompareTo(s2.time));

            if (!SortedStudents[0].answer.Equals(string.Empty))
                eW_Accelerate.Time1.Text = SortedStudents[0].time.ToString("0.00");
            else
                eW_Accelerate.Time1.Text = "0s";

            if (!SortedStudents[1].answer.Equals(string.Empty))
                eW_Accelerate.Time2.Text = SortedStudents[1].time.ToString("0.00");
            else
                eW_Accelerate.Time2.Text = "0s";

            if (!SortedStudents[2].answer.Equals(string.Empty))
                eW_Accelerate.Time3.Text = SortedStudents[2].time.ToString("0.00");
            else
                eW_Accelerate.Time3.Text = "0s";

            if (!SortedStudents[3].answer.Equals(string.Empty))
                eW_Accelerate.Time4.Text = SortedStudents[3].time.ToString("0.00");
            else
                eW_Accelerate.Time4.Text = "0s";

            eW_Accelerate.StudentName1.Text = SortedStudents[0].name;
            eW_Accelerate.StudentName2.Text = SortedStudents[1].name;
            eW_Accelerate.StudentName3.Text = SortedStudents[2].name;
            eW_Accelerate.StudentName4.Text = SortedStudents[3].name;

            eW_Accelerate.Answer1.Text = SortedStudents[0].answer;
            eW_Accelerate.Answer2.Text = SortedStudents[1].answer;
            eW_Accelerate.Answer3.Text = SortedStudents[2].answer;
            eW_Accelerate.Answer4.Text = SortedStudents[3].answer;

            eW_Accelerate.IntroAnswer.Visibility = Visibility.Visible;
            eW_Accelerate.IntroAnswer.Play();
        }

        private void CountDown_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = false;
            dateTime = DateTime.Now;
            TimeLeft = 30; //Nhan Start them lan nua de Restart
            Timer.Start();
            eW_Accelerate.TimeVideo.Visibility = Visibility.Visible;
            eW_Accelerate.TimeVideo.Play();
            if (questions[cur_question - 1].QuestionTypeID == 42)
            {
                eW_Accelerate.QuestionVideo.Visibility = Visibility.Visible;
                eW_Accelerate.QuestionVideo.Play();
            }
            Main.BroadcastData("03_02");
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            eW_Accelerate.ShowAns.Visibility = Visibility.Hidden;
            eW_Accelerate.Question.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionImage.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionAnswer.Visibility = Visibility.Hidden;
            eW_Accelerate.QuestionBackground.Visibility = Visibility.Hidden;
            eW_Accelerate.TimeVideo.Visibility = Visibility.Hidden;
            eW_Accelerate.IntroQuestion.Visibility = Visibility.Hidden;

            switch (++cur_question)
            {
                case 2:
                    Question1.IsEnabled = false;
                    Question2.IsEnabled = true;
                    break;
                case 3:
                    Question2.IsEnabled = false;
                    Question3.IsEnabled = true;
                    break;
                case 4:
                    Question3.IsEnabled = false;
                    Question4.IsEnabled = true;
                    break;
            }

            ClearTextBox();
        }

        //Timer                         
        private void WorksInTick(object sender, EventArgs e)
        {

            if (TimeLeft != 0)
            {
                TimeLeft -= 1;
            }
            TimeLeft = Math.Round(TimeLeft, 2); //System.Math de khong bi 2.9999999999
            TimerUI.Text = TimeLeft.ToString();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            students[0].AddPoint(int.Parse(Point1.Text.ToString()));
            students[1].AddPoint(int.Parse(Point2.Text.ToString()));
            students[2].AddPoint(int.Parse(Point3.Text.ToString()));
            students[3].AddPoint(int.Parse(Point4.Text.ToString()));

            if ((!Point1.Text.ToString().Equals("0") && !Point1.Text.ToString().Equals(string.Empty)) ||
                (!Point2.Text.ToString().Equals("0") && !Point2.Text.ToString().Equals(string.Empty)) ||
                (!Point3.Text.ToString().Equals("0") && !Point3.Text.ToString().Equals(string.Empty)) ||
                (!Point4.Text.ToString().Equals("0") && !Point4.Text.ToString().Equals(string.Empty)))
            {
                TrueSound.Stop();
                TrueSound.Play();
            }


            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);

            Point1.Text = String.Empty;
            Point2.Text = String.Empty;
            Point3.Text = String.Empty;
            Point4.Text = String.Empty;

            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private int[] getPointsPerQuest()
        {
            string[] pointsString = new string[]
            {
               (string)((ComboBoxItem)((ComboBox) FindName("Point1")).SelectedItem).Content,
               (string)((ComboBoxItem)((ComboBox) FindName("Point2")).SelectedItem).Content,
               (string)((ComboBoxItem)((ComboBox) FindName("Point3")).SelectedItem).Content,
               (string)((ComboBoxItem)((ComboBox) FindName("Point4")).SelectedItem).Content
            };

            int[] ps = new int[pointsString.Length];
            for (int i = 0; i < pointsString.Length; i++)
            {
                int.TryParse(pointsString[i], out ps[i]);
            }
            return ps;
        }

        public void Luu_Click(object sender, RoutedEventArgs e)
        {
            students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
            students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
            students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
            students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

            students[0].Point = int.Parse(Points1.Text);
            students[1].Point = int.Parse(Points2.Text);
            students[2].Point = int.Parse(Points3.Text);
            students[3].Point = int.Parse(Points4.Text);

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            AnswerStudentName1.Text = students[0].Name;
            AnswerStudentName1.Text = students[1].Name;
            AnswerStudentName1.Text = students[2].Name;
            AnswerStudentName1.Text = students[3].Name;
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        public void NhanTinHieuTraLoi(int position, string answer, int time)
        {
            switch (position)
            {
                case 1:
                    Answer1.Text = answer;
                    timeElapsed = DateTime.Now - dateTime;
                    Time1.Text = timeElapsed.TotalSeconds.ToString("0.00");
                    ftime1 = float.Parse(timeElapsed.TotalSeconds.ToString("0.00"), provider: System.Globalization.CultureInfo.InvariantCulture);
                    //Time1.Text = time.ToString();
                    break;
                case 2:
                    Answer2.Text = answer;
                    timeElapsed2 = DateTime.Now - dateTime;
                    Time2.Text = timeElapsed2.TotalSeconds.ToString("0.00");
                    ftime2 = float.Parse(timeElapsed2.TotalSeconds.ToString("0.00"), provider: System.Globalization.CultureInfo.InvariantCulture);
                    //Time2.Text = time.ToString();
                    break;
                case 3:
                    Answer3.Text = answer;
                    timeElapsed3 = DateTime.Now - dateTime;
                    Time3.Text = timeElapsed3.TotalSeconds.ToString("0.00");
                    ftime3 = float.Parse(timeElapsed3.TotalSeconds.ToString("0.00"), provider: System.Globalization.CultureInfo.InvariantCulture);
                    //Time3.Text = time.ToString();
                    break;
                case 4:
                    Answer4.Text = answer;
                    timeElapsed4 = DateTime.Now - dateTime;
                    Time4.Text = timeElapsed4.TotalSeconds.ToString("0.00");
                    ftime4 = float.Parse(timeElapsed4.TotalSeconds.ToString("0.00"), provider: System.Globalization.CultureInfo.InvariantCulture);
                    //Time4.Text = time.ToString();
                    break;
            }
        }

        public void ClearTextBox()
        {
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;

            Time1.Text = String.Empty;
            Time2.Text = String.Empty;
            Time3.Text = String.Empty;
            Time4.Text = String.Empty;
        }

        private void HideAll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void StartSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            StartSound.Stop();
        }

        private void TrueSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            TrueSound.Stop();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
