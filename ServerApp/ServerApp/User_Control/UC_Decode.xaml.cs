﻿using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using ServerApp.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UC_Decode.xaml
    /// </summary>
    public partial class UC_Decode : UserControl
    {
        public EW_Decode eW_Decode = new EW_Decode();
        public List<DecodeQuestion> DecodeQuestions = DecodeQuestionDAO.Instance.getQuestions();
        public Button[,] txt = new Button[9, 8];
        public int[,] value = new int[9, 8];
        public int selectedRow = 0;
        public int selectedColumn = 0;
        public string videoPath = Directory.GetCurrentDirectory();
        public List<Student> students = new List<Student>();
        public readonly MainWindow Main;
        public ObservableCollection<string> AddedPoint { get; set; }
        SoundPlayer TrueSound;
        SoundPlayer FalseSound;
        SoundPlayer HintClickedSound;

        public UC_Decode(MainWindow main, List<Student> students, EW_Decode eW_Decode)
        {
            InitializeComponent();

            MatrixOpenedSound.Source = new Uri(videoPath + @"\Resources\2_AmThanhDanhSachHangNgang.mp3", UriKind.RelativeOrAbsolute);
            QuestionClickedSound.Source = new Uri(videoPath + @"\Resources\2_AmThanhChonHangNgang.mp3", UriKind.RelativeOrAbsolute);
            DisableQuestionElementSound.Source = new Uri(videoPath + @"\Resources\2_AmThanhTraLoiSaiCNV.mp3", UriKind.RelativeOrAbsolute);
            KeyAnswerSound.Source = new Uri(videoPath + @"\Resources\2_AmThanhTinHieuTraLoi.mp3", UriKind.RelativeOrAbsolute);
            CorrectKeySound.Source = new Uri(videoPath + @"\Resources\2_AmThanhTraLoiDungCNV.mp3", UriKind.RelativeOrAbsolute);

            HintClickedSound = new SoundPlayer(videoPath + @"\Resources\2_AmThanhMoHinhAnhGoiY.wav");
            TrueSound = new SoundPlayer(videoPath + @"\Resources\1_AmThanhTraLoiDung.wav");
            FalseSound = new SoundPlayer(videoPath + @"\Resources\1_AmThanhTraLoiSai.wav");

            this.Main = main;
            this.students = students;
            this.eW_Decode = eW_Decode;

            #region Thiết lập combobox
            Point1.Items.Add("0");
            Point1.Items.Add("5");
            Point1.Items.Add("10");
            Point1.Items.Add("15");
            Point1.Items.Add("30");
            Point2.Items.Add("0");
            Point2.Items.Add("5");
            Point2.Items.Add("10");
            Point2.Items.Add("15");
            Point2.Items.Add("30");
            Point3.Items.Add("0");
            Point3.Items.Add("5");
            Point3.Items.Add("10");
            Point3.Items.Add("15");
            Point3.Items.Add("30");
            Point4.Items.Add("0");
            Point4.Items.Add("5");
            Point4.Items.Add("10");
            Point4.Items.Add("15");
            Point4.Items.Add("30");
            #endregion

            #region Thiết lập thuộc tính cho ma trận
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    value[i, j] = 0;

                    txt[i, j] = new Button();
                    txt[i, j].Name = "element_" + i + "_" + j;
                    txt[i, j].Content = String.Empty;
                    txt[i, j].Background = Brushes.LightCyan;
                    txt[i, j].FontSize = 15;
                    txt[i, j].VerticalAlignment = VerticalAlignment.Stretch;
                    txt[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                    txt[i, j].HorizontalAlignment = HorizontalAlignment.Stretch;
                    txt[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                    txt[i, j].BorderThickness = new Thickness(1, 1, 1, 1);
                    txt[i, j].BorderBrush = Brushes.Navy;
                    txt[i, j].Click += MatrixElement_Click;
                    Grid.SetRow(txt[i, j], i);
                    Grid.SetColumn(txt[i, j], j);
                }
            }
            #endregion

            #region Set thuộc tính ô gợi ý và câu hỏi (x)
            foreach (DecodeQuestion n in DecodeQuestions)
            {
                if (n.CellType == 10 || n.CellType == 11)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 30, 160, 117));
                    if (n.CellType == 11)
                    {
                        txt[n.RowNo - 1, n.ColNo - 1].Content = "💡";
                        value[n.RowNo - 1, n.ColNo - 1] = 1;
                    }
                }
                else if (n.CellType == 20 || n.CellType == 21)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 230, 250, 10));
                    if (n.CellType == 21)
                    {
                        txt[n.RowNo - 1, n.ColNo - 1].Content = "💡";
                        value[n.RowNo - 1, n.ColNo - 1] = 1;
                    }
                }
                else if (n.CellType == 30)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 197, 10, 36));
                }
            }
            #endregion

            #region Set thuộc tính ô bình thường
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (txt[i, j].Background == Brushes.LightCyan)
                    {
                        txt[i, j].Content = numberOfDots(value, i, j).ToString();
                        eW_Decode.txt[i, j].Content = numberOfDots(value, i, j).ToString();
                        txt[i, j].Foreground = Brushes.Navy;
                        txt[i, j].IsEnabled = false;
                    }
                }
            }
            #endregion

            #region Thêm danh sách button vào grid chính
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    MatrixContent.Children.Add(txt[i, j]);
                }
            }
            #endregion
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        public int numberOfDots(int[,] a, int row_i, int col_i)
        {
            int n = 0;

            #region Chặn 4 góc
            if (row_i == 0 && col_i == 0)
            {
                if (a[0, 1] == 1)
                    n++;
                if (a[1, 0] == 1)
                    n++;
                if (a[1, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 0 && col_i == 7)
            {
                if (a[0, 6] == 1)
                    n++;
                if (a[1, 7] == 1)
                    n++;
                if (a[1, 6] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8 && col_i == 0)
            {
                if (a[7, 0] == 1)
                    n++;
                if (a[8, 1] == 1)
                    n++;
                if (a[7, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8 && col_i == 7)
            {
                if (a[7, 6] == 1)
                    n++;
                if (a[7, 7] == 1)
                    n++;
                if (a[8, 6] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Chặn 4 cạnh
            else if (row_i == 0)
            {
                if (a[0, col_i - 1] == 1)
                    n++;
                if (a[0, col_i + 1] == 1)
                    n++;
                if (a[1, col_i - 1] == 1)
                    n++;
                if (a[1, col_i] == 1)
                    n++;
                if (a[1, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8)
            {
                if (a[8, col_i - 1] == 1)
                    n++;
                if (a[8, col_i + 1] == 1)
                    n++;
                if (a[7, col_i - 1] == 1)
                    n++;
                if (a[7, col_i] == 1)
                    n++;
                if (a[7, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 0)
            {
                if (a[row_i - 1, 0] == 1)
                    n++;
                if (a[row_i + 1, 0] == 1)
                    n++;
                if (a[row_i - 1, 1] == 1)
                    n++;
                if (a[row_i, 1] == 1)
                    n++;
                if (a[row_i + 1, 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 7)
            {
                if (a[row_i - 1, 7] == 1)
                    n++;
                if (a[row_i + 1, 7] == 1)
                    n++;
                if (a[row_i - 1, 6] == 1)
                    n++;
                if (a[row_i, 6] == 1)
                    n++;
                if (a[row_i + 1, 6] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Xét 8 hướng
            else
            {
                if (a[row_i - 1, col_i - 1] == 1)
                    n++;
                if (a[row_i - 1, col_i] == 1)
                    n++;
                if (a[row_i - 1, col_i + 1] == 1)
                    n++;
                if (a[row_i, col_i - 1] == 1)
                    n++;
                if (a[row_i, col_i + 1] == 1)
                    n++;
                if (a[row_i + 1, col_i - 1] == 1)
                    n++;
                if (a[row_i + 1, col_i] == 1)
                    n++;
                if (a[row_i + 1, col_i + 1] == 1)
                    n++;
                return n;
            }
            #endregion
        }

        private void ShowMatrix_Click(object sender, RoutedEventArgs e)
        {
            MatrixOpenedSound.Stop();
            MatrixOpenedSound.Play();
            eW_Decode.MainMatrix.Visibility = Visibility.Visible;
        }

        private void MatrixElement_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.isKeyQuestion = false;

            Button clickedButton = (Button)e.Source;
            clickedButton.IsEnabled = false;

            selectedRow = int.Parse(clickedButton.Name.Split('_').ToList().ElementAt(1));
            selectedColumn = int.Parse(clickedButton.Name.Split('_').ToList().ElementAt(2));

            if (txt[selectedRow, selectedColumn].Content.Equals("💡"))
            {
                HintClickedSound.Load();
                HintClickedSound.Play();
                if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FF1EA075"))
                    eW_Decode.txt[selectedRow, selectedColumn].Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FFC50A24"))
                    eW_Decode.txt[selectedRow, selectedColumn].Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

                eW_Decode.txt[selectedRow, selectedColumn].Content = "💡";
                txt[selectedRow, selectedColumn].Content = "💡";
                ShowQuestionVideo.IsEnabled = false;
            }
            else
            {
                QuestionClickedSound.Stop();
                QuestionClickedSound.Play();
                if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FF1EA075"))
                    eW_Decode.txt[selectedRow, selectedColumn].Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FFC50A24"))
                    eW_Decode.txt[selectedRow, selectedColumn].Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

                eW_Decode.txt[selectedRow, selectedColumn].Content = "？";
                txt[selectedRow, selectedColumn].Content = "？";
                ShowQuestionVideo.IsEnabled = true;
            }
        }

        private void ShowQuestionVideo_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.QuestionVideo.Visibility = Visibility.Visible;
            eW_Decode.QuestionVideo.Play();
            string questionDetail = DecodeQuestionDAO.Instance.getQuestion(selectedRow + 1, selectedColumn + 1).Question;
            eW_Decode.QuestionContent.Text = questionDetail;

            Main.BroadcastData("04_01_" + questionDetail);
        }
        private void CountDown_Click(object sender, RoutedEventArgs e)
        {
            if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FF1EA075"))
            {
                eW_Decode.CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_15s.mp4", UriKind.Relative);
                Main.BroadcastData("04_02_15");
            }
            if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FFE6FA0A"))
            {
                eW_Decode.CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_20s.mp4", UriKind.Relative);
                Main.BroadcastData("04_02_20");
            }
            if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FFC50A24"))
            {
                eW_Decode.CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_25s.mp4", UriKind.Relative);
                Main.BroadcastData("04_02_25");
            }

            eW_Decode.CountDownVideo.Visibility = Visibility.Visible;
            eW_Decode.CountDownVideo.Play();
        }

        public void Luu_Click(object sender, RoutedEventArgs e)
        {
            students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
            students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
            students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
            students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

            students[0].Point = int.Parse(Points1.Text);
            students[1].Point = int.Parse(Points2.Text);
            students[2].Point = int.Parse(Points3.Text);
            students[3].Point = int.Parse(Points4.Text);

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);

            StudentName1.Text = students[0].Name;
            StudentName2.Text = students[1].Name;
            StudentName3.Text = students[2].Name;
            StudentName4.Text = students[3].Name;

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

        }

        private void MatrixOpenedSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            MatrixOpenedSound.Stop();
        }
        private void QuestionClickedSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            QuestionClickedSound.Stop();
        }
        private void HintClickedSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            HintClickedSound.Stop();
        }
        private void TrueSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            TrueSound.Stop();
        }
        private void FalseSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            FalseSound.Stop();
        }
        private void DisableQuestionElementSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            DisableQuestionElementSound.Stop();
        }
        private void KeyAnswerSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            KeyAnswerSound.Stop();
        }
        private void CorrectKeySound_MediaEnded(object sender, RoutedEventArgs e)
        {
            CorrectKeySound.Stop();
        }

        private void ShowStudentAnswer_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.StudentName1.Text = students[0].Name;
            eW_Decode.StudentName2.Text = students[1].Name;
            eW_Decode.StudentName3.Text = students[2].Name;
            eW_Decode.StudentName4.Text = students[3].Name;

            eW_Decode.Answer1.Text = Answer1.Text.ToUpper();
            eW_Decode.Answer2.Text = Answer2.Text.ToUpper();
            eW_Decode.Answer3.Text = Answer3.Text.ToUpper();
            eW_Decode.Answer4.Text = Answer4.Text.ToUpper();

            eW_Decode.AnswerVideo.Visibility = Visibility.Visible;
            eW_Decode.AnswerVideo.Play();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if ((!Point1.Text.Equals("0") && !Point1.Text.Equals("30") && !Point1.Text.Equals(String.Empty)) ||
                (!Point2.Text.Equals("0") && !Point2.Text.Equals("30") && !Point2.Text.Equals(String.Empty)) ||
                (!Point3.Text.Equals("0") && !Point3.Text.Equals("30") && !Point3.Text.Equals(String.Empty)) ||
                (!Point4.Text.Equals("0") && !Point4.Text.Equals("30") && !Point4.Text.Equals(String.Empty)))
            {
                TrueSound.Load();
                TrueSound.Play();
            }
            else if ((Point1.Text.Equals("0") || Point1.Text.Equals(String.Empty)) &&
                (Point2.Text.Equals("0") || Point2.Text.Equals(String.Empty)) &&
                (Point3.Text.Equals("0") || Point3.Text.Equals(String.Empty)) &&
                (Point4.Text.Equals("0") || Point4.Text.Equals(String.Empty)))
            {
                FalseSound.Load();
                FalseSound.Play();
            }

            else if (Point1.Text.Equals("30") || Point2.Text.Equals("30") || Point3.Text.Equals("30") || Point4.Text.Equals("30"))
            {
                CorrectKeySound.Stop();
                CorrectKeySound.Play();
            }

            students[0].AddPoint(Point1.Text.Equals(string.Empty) ? 0 : int.Parse(Point1.Text));
            students[1].AddPoint(Point2.Text.Equals(string.Empty) ? 0 : int.Parse(Point2.Text));
            students[2].AddPoint(Point3.Text.Equals(string.Empty) ? 0 : int.Parse(Point3.Text));
            students[3].AddPoint(Point4.Text.Equals(string.Empty) ? 0 : int.Parse(Point4.Text));

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);

            Point1.Text = String.Empty;
            Point2.Text = String.Empty;
            Point3.Text = String.Empty;
            Point4.Text = String.Empty;

            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;

        }

        public void NhanTinHieuTraLoi(int position, string answer)
        {
            switch (position)
            {
                case 1:
                    Answer1.Text = answer;
                    break;
                case 2:
                    Answer2.Text = answer;
                    break;
                case 3:
                    Answer3.Text = answer;
                    break;
                case 4:
                    Answer4.Text = answer;
                    break;
                default:
                    break;
            }
        }

        public void NhanTinHieuTraLoiTuKhoa(int position)
        {
            KeyAnswerSound.Stop();
            KeyAnswerSound.Play();
            switch (position)
            {
                case 1:
                    eW_Decode.ObstacleText1.Text = students[0].Name;
                    ObstacleFromContestant1.Visibility = Visibility.Visible;
                    eW_Decode.ObstacleFromContestant1.Visibility = Visibility.Visible;
                    break;
                case 2:
                    eW_Decode.ObstacleText2.Text = students[1].Name;
                    ObstacleFromContestant2.Visibility = Visibility.Visible;
                    eW_Decode.ObstacleFromContestant2.Visibility = Visibility.Visible;
                    break;
                case 3:
                    eW_Decode.ObstacleText3.Text = students[2].Name;
                    ObstacleFromContestant3.Visibility = Visibility.Visible;
                    eW_Decode.ObstacleFromContestant3.Visibility = Visibility.Visible;
                    break;
                case 4:
                    eW_Decode.ObstacleText4.Text = students[3].Name;
                    ObstacleFromContestant4.Visibility = Visibility.Visible;
                    eW_Decode.ObstacleFromContestant4.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.CountDownVideo.Visibility = Visibility.Hidden;
            eW_Decode.QuestionContent.Visibility = Visibility.Hidden;
            eW_Decode.QuestionBackground.Visibility = Visibility.Hidden;
            eW_Decode.ShowAnswer.Visibility = Visibility.Hidden;
            eW_Decode.QuestionHintImage.Visibility = Visibility.Hidden;
        }

        private void True_Click(object sender, RoutedEventArgs e)
        {
            if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FF1EA075"))
                eW_Decode.txt[selectedRow, selectedColumn].Foreground = Brushes.Azure;
            if (eW_Decode.txt[selectedRow, selectedColumn].Background.ToString().Equals("#FFC50A24"))
                eW_Decode.txt[selectedRow, selectedColumn].Foreground = Brushes.Azure;
            eW_Decode.txt[selectedRow, selectedColumn].Content = numberOfDots(value, selectedRow, selectedColumn).ToString();
        }

        private void DisableQuestionElement_Click(object sender, RoutedEventArgs e)
        {
            DisableQuestionElementSound.Stop();
            DisableQuestionElementSound.Play();
            txt[selectedRow, selectedColumn].Content = "x";
            eW_Decode.txt[selectedRow, selectedColumn].Content = "x";
        }

        private void CountDownKey_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_10s.mp4", UriKind.Relative);

            eW_Decode.CountDownVideo.Visibility = Visibility.Visible;
            eW_Decode.CountDownVideo.Play();
            Main.BroadcastData("04_02_10");
        }

        private void DisableElement_Click(object sender, RoutedEventArgs e)
        {
            if (!DisabledRow.Text.Equals(string.Empty) && !DisabledColumn.Text.Equals(string.Empty))
            {
                DisableQuestionElementSound.Stop();
                DisableQuestionElementSound.Play();
                txt[selectedRow, selectedColumn].Content = "x";
                eW_Decode.txt[int.Parse(DisabledRow.Text), int.Parse(DisabledColumn.Text)].Content = "x";
            }
        }

        private void ShowKeyVideo_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.isKeyQuestion = true;

            eW_Decode.QuestionContent.Visibility = Visibility.Hidden;
            eW_Decode.QuestionVideo.Visibility = Visibility.Visible;
            eW_Decode.QuestionVideo.Play();
        }

        private void ShowHint_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.QuestionBackground.Visibility = Visibility.Visible;
            if (File.Exists(videoPath + @"\Resources\4_QuestionHint_" + (selectedRow + 1).ToString() + "_" + (selectedColumn + 1).ToString() + ".PNG"))
                eW_Decode.QuestionHintImage.Source = new BitmapImage(new Uri(videoPath + @"\Resources\4_QuestionHint_" + (selectedRow + 1).ToString() + "_" + (selectedColumn + 1).ToString() +".PNG"));
            eW_Decode.QuestionHintImage.Visibility = Visibility.Visible;

            eW_Decode.CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_10s.mp4", UriKind.Relative);

            eW_Decode.CountDownVideo.Visibility = Visibility.Visible;
            eW_Decode.CountDownVideo.Play();
            Main.BroadcastData("04_02_10");
        }

        private void HideSignal_Click(object sender, RoutedEventArgs e)
        {
            eW_Decode.ObstacleFromContestant1.Visibility = Visibility.Hidden;
            eW_Decode.ObstacleFromContestant2.Visibility = Visibility.Hidden;
            eW_Decode.ObstacleFromContestant3.Visibility = Visibility.Hidden;
            eW_Decode.ObstacleFromContestant4.Visibility = Visibility.Hidden;

            ObstacleFromContestant1.Visibility = Visibility.Hidden;
            ObstacleFromContestant2.Visibility = Visibility.Hidden;
            ObstacleFromContestant3.Visibility = Visibility.Hidden;
            ObstacleFromContestant4.Visibility = Visibility.Hidden;

        }
    }
}
