﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using ServerApp.Slide.Obstacle_Round;
using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.Utilities;


namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1
    {
        public List<Student> students;
        private List<Question> listQuestion;
        static String keyname { get; set; }
        static String Image { get; set; }
        private static int qs_count { get; set; }

        const int DEFAULT_TIME = 15;
        const int MAXIMUM_STDS_NUM = 4;
        const int POINTS_PER_NQUESTION = 10;

        //Time-related
        int cur_time = DEFAULT_TIME;
        int cur_score = 0;
        int cur_question = 1;
        private QuestionDAO quesDAO;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        Obstacle_Slide obs_slide;

        public UserControl1(List<Student> stds, Obstacle_Slide obs_slide)
        {
            qs_count = 0;
            this.obs_slide = obs_slide;
            quesDAO = new QuestionDAO();
            listQuestion = new List<Question>();
            this.getQuestions();
            this.sendAnswerToSlide();
            InitializeComponent();

            //DispatcherTimer
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        }

        //Logical method for Start button
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            obs_slide.OpenVideo.Visibility = Visibility.Hidden;
            obs_slide.showObstacle();
            Submit.IsEnabled = false;
            dispatcherTimer.Start();
        }

        //Logical method for Next buttons
        private void Next_Click(object sender, RoutedEventArgs e)
        {

        }

        //Logical method for BigRed buttons
        private void BigRed_Click(object sender, RoutedEventArgs e)
        {
            if (qs_count <= 1)
            {

            }
            else if (qs_count == 2)
            {

            }
            else if (qs_count == 3)
            {

            }
            else if (qs_count == 4)
            {

            }
            else
            {

            }
        }

        //Logical method for ImageX buttons
        private void Image_Checked(object sender, RoutedEventArgs e)
        {
            Image = String.Copy(((ToggleButton)e.Source).Name);
            switch (Image)
            {
                case "Image1":

                    Image1.IsEnabled = false;
                    break;
                case "Image2":

                    Image2.IsEnabled = false;
                    break;
                case "Image3":

                    Image3.IsEnabled = false;
                    break;
                case "Image4":

                    Image4.IsEnabled = false;
                    break;
            }

        }

        //Show question method
        //

        //Logical method for Key buttons
        private void KeyButton_Checked(object sender, RoutedEventArgs e)
        {
            keyname = String.Copy(((ToggleButton)e.Source).Name);
            switch (keyname)
            {
                case "Key1":
                    Key2.IsChecked = false;
                    Key3.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key2":
                    Key1.IsChecked = false;
                    Key3.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key3":
                    Key2.IsChecked = false;
                    Key1.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key4":
                    Key2.IsChecked = false;
                    Key3.IsChecked = false;
                    Key1.IsChecked = false;
                    break;
            }
            if (Key1.IsChecked == Key2.IsChecked == Key3.IsChecked == Key4.IsChecked)
            {
                keyname = "";
            }
        }

        //Logical method for Submit buttons
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            switch (keyname)
            {
                case "Key1":
                    students = new List<Student>(AddKeyPoint(qs_count, students, 0));
                    break;
                case "Key2":
                    students = new List<Student>(AddKeyPoint(qs_count, students, 1));
                    break;
                case "Key3":
                    students = new List<Student>(AddKeyPoint(qs_count, students, 2));
                    break;
                case "Key4":
                    students = new List<Student>(AddKeyPoint(qs_count, students, 3));
                    break;
                default:
                    break;
            }


            if (Ob_Point1.IsChecked == true)
            {
                students[0].Point = students[0].Point + POINTS_PER_NQUESTION;
            }
            if (Ob_Point2.IsChecked == true)
            {
                students[1].Point = students[1].Point + POINTS_PER_NQUESTION;
            }
            if (Ob_Point3.IsChecked == true)
            {
                students[1].Point = students[1].Point + POINTS_PER_NQUESTION;
            }
            if (Ob_Point4.IsChecked == true)
            {
                students[1].Point = students[1].Point + POINTS_PER_NQUESTION;
            }
            qs_count++;
        }

        //Add point when any Key is held
        private List<Student> AddKeyPoint(int count, List<Student> student, int i)
        {
            //if (count<=1)
            //{
            //    student[i].setPoint(student[i].getPoint() + 8* POINTS_PER_NQUESTION);
            //    BigRed.IsEnabled = true;
            //}
            //else if (count==2)
            //{
            //    student[i].setPoint(student[i].getPoint() + 6* POINTS_PER_NQUESTION);
            //    BigRed.IsEnabled = true;
            //}
            //else if (count == 3)
            //{
            //    student[i].setPoint(student[i].getPoint() + 4* POINTS_PER_NQUESTION);
            //    BigRed.IsEnabled = true;
            //}
            //else if (count == 4)
            //{
            //    student[i].setPoint(student[i].getPoint() + 2* POINTS_PER_NQUESTION);
            //    BigRed.IsEnabled = true;
            //}
            //else
            //{
            //    student[i].setPoint(student[i].getPoint() + POINTS_PER_NQUESTION);
            //    BigRed.IsEnabled = true;
            //}
            return student;
        }

        //Return the Students List 
        public List<Student> getStudent()
        {
            return students;
        }
        private void Load_Question(object sender, RoutedEventArgs e)
        {

            Button btn = e.Source as Button;
            string ts = btn.Content.ToString();
            obs_slide.QuestionVideo.Visibility = Visibility.Visible;
            obs_slide.openVideo(obs_slide.QuestionVideo);
            switch (ts)
            {
                case "1":
                    obs_slide.txtQuestion.Text = listQuestion[0].Detail;

                    foreach (Shape s in obs_slide.Question1.Children)
                    {
                        s.Stroke = Brushes.Black;
                        s.Fill = Brushes.Blue;
                    }
                    break;
                case "2":
                    obs_slide.txtQuestion.Text = listQuestion[1].Detail;

                    foreach (Shape s in obs_slide.Question2.Children)
                    {
                        s.Stroke = Brushes.Black;
                        s.Fill = Brushes.Blue;
                    }
                    break;
                case "3":
                    obs_slide.txtQuestion.Text = listQuestion[2].Detail;

                    foreach (Shape s in obs_slide.Question3.Children)
                    {
                        s.Stroke = Brushes.Black;
                        s.Fill = Brushes.Blue;
                    }

                    break;
                case "4":
                    obs_slide.txtQuestion.Text = listQuestion[3].Detail;

                    foreach (Shape s in obs_slide.Question4.Children)
                    {
                        s.Stroke = Brushes.Black;
                        s.Fill = Brushes.Blue;
                    }
                    break;
                case "Ans1":
                    String ans1 = listQuestion[0].Answer;
                    obs_slide.ShowAnswer(ans1, 0);
                    break;
                case "Ans2":
                    String ans2 = listQuestion[1].Answer;
                    obs_slide.ShowAnswer(ans2, 1);
                    break;
                case "Ans3":
                    String ans3 = listQuestion[1].Answer;
                    obs_slide.ShowAnswer(ans3, 2);
                    break;
                case "Ans4":
                    String ans4 = listQuestion[1].Answer;
                    obs_slide.ShowAnswer(ans4, 3);
                    break;

            }

        }
        public void getQuestions()
        {

            try
            {
                //UtilityMethods utility = new UtilityMethods();
                for (int i = 7; i <= 10; i++)
                {
                    //Question question = quesDAO.getQuestion(i);
                    //listQuestion.Add(question);
                    //_listQA.Add(utility.fromQuestionToQA(question));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void sendAnswerToSlide()
        {
            List<Question> QuestionList = new List<Question>();
            foreach (Question q in listQuestion)
            {
                QuestionList.Add(q);
            }
            obs_slide.receiveAnswer(QuestionList);
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (cur_time > 0)
            {
                cur_time--;
                //TbkTime.Text = cur_time.ToString();
                if (cur_time == DEFAULT_TIME - 1)
                {
                    /*TbkQuestionNo.Text = cur_question.ToString();
                    TbkQuestion.Text = question[cur_question - 1];*/
                }
            }
            else
            {
                Submit.IsEnabled = true;
                dispatcherTimer.Stop();
                /*TbkTime.Text = "Hoàn thành";
                TbkQuestionNo.Text = String.Empty;
                TbkQuestion.Text = String.Empty;*/
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            obs_slide.CountVideo.Visibility = Visibility.Visible;
            obs_slide.openVideo(obs_slide.CountVideo);
        }
    }
}
