﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using ServerApp.Slide.Start_Round;
using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using System.IO;
using System.Windows.Forms;
using ServerApp.Utilities;
using System.Net.Sockets;
//using System.Windows.Threading;

namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UC_Start.xaml
    /// 
    /// </summary>
    /// 

    public partial class UC_Start : System.Windows.Controls.UserControl
    {
        EW_Start eW_Start;
        string currentStudentName = string.Empty;
        int currentStudentID = 0;
        int currentQuestion = 1;
        int currentPort = 0;
        string currentIP = "";
        List<Question> question = new List<Question>();
        List<Student> students = new List<Student>();
        private readonly MainWindow Main;

        public string student1IP = "127.0.0.1:59001";
        public string student2IP = "127.0.0.1:59002";
        public string student3IP = "127.0.0.1:59003";
        public string student4IP = "127.0.0.1:59004";

        public List<Socket> clients { get; set; }

        static string soundPath = Directory.GetCurrentDirectory();
        SoundPlayer TrueSound = new SoundPlayer(soundPath + @"\Resources\1_AmThanhTraLoiDung.wav");
        SoundPlayer FalseSound = new SoundPlayer(soundPath + @"\Resources\1_AmThanhTraLoiSai.wav");

        public UC_Start(MainWindow main, List<Student> stds, EW_Start ew)
        {
            InitializeComponent();
            Main = main;

            eW_Start = ew;
            students = stds;
            StartSound.Source = new Uri(soundPath + @"\Resources\1_AmThanhBatDau.mp3", UriKind.RelativeOrAbsolute);
            FinishSound.Source = new Uri(soundPath + @"\Resources\1_AmThanhKetThuc.mp3", UriKind.RelativeOrAbsolute);
        }

        public void SetIP()
        {
            if (Main.text.Equals("192.168.1.100:59000"))
            {
                student1IP = "192.168.1.101:59001";
                student2IP = "192.168.1.102:59002";
                student3IP = "192.168.1.103:59003";
                student4IP = "192.168.1.104:59004";
            }
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        private void Intro_Click(object sender, RoutedEventArgs e)
        {
            eW_Start.IntroVideo.BringIntoView();
            eW_Start.IntroVideo.Play();
        }

        private void StartSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            StartSound.Stop();
        }

        private void FinishSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            eW_Start.PointText.Text = String.Empty;
            eW_Start.QuestionText.Text = String.Empty;

            eW_Start.QuestionBackground.Visibility = Visibility.Hidden;
            eW_Start.QuestionText.Visibility = Visibility.Hidden;
            eW_Start.TimeVideo.Visibility = Visibility.Hidden;
            eW_Start.IntroVideo.Visibility = Visibility.Hidden;
            eW_Start.StudentVideo.Visibility = Visibility.Hidden;
            eW_Start.PointText.Visibility = Visibility.Hidden;
            eW_Start.StudentName1.Visibility = Visibility.Hidden;
            eW_Start.StudentName2.Visibility = Visibility.Hidden;
            eW_Start.StudentName3.Visibility = Visibility.Hidden;
            eW_Start.StudentName4.Visibility = Visibility.Hidden;
            eW_Start.ContentText.Text = "KHỞI ĐỘNG";

            FinishSound.Stop();
        }

        private void StartStudent1_Click(object sender, RoutedEventArgs e)
        {
            StartStudent1.IsEnabled = false;
            Finish.IsEnabled = true;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentName = students[0].Name;
            currentStudentID = 1;
            currentIP = student1IP;
            currentPort = 59001;
            question = DAO.QuestionDAO.Instance.getStartQuestion(currentStudentID);

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Start.ContentText.Text = currentStudentName.ToUpper();
            eW_Start.StudentName1.FontWeight = FontWeights.ExtraBold;
        }

        private void StartStudent2_Click(object sender, RoutedEventArgs e)
        {
            StartStudent2.IsEnabled = false;
            Finish.IsEnabled = true;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentName = students[1].Name;
            currentStudentID = 2;
            currentIP = student2IP;
            currentPort = 59002;
            question = DAO.QuestionDAO.Instance.getStartQuestion(currentStudentID);

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Start.ContentText.Text = currentStudentName.ToUpper();
            eW_Start.StudentName2.FontWeight = FontWeights.ExtraBold;
        }

        private void StartStudent3_Click(object sender, RoutedEventArgs e)
        {
            StartStudent3.IsEnabled = false;
            Finish.IsEnabled = true;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentName = students[2].Name;
            currentStudentID = 3;
            currentIP = student3IP;
            currentPort = 59003;
            question = DAO.QuestionDAO.Instance.getStartQuestion(currentStudentID);

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Start.ContentText.Text = currentStudentName.ToUpper();
            eW_Start.StudentName3.FontWeight = FontWeights.ExtraBold;
        }

        private void StartStudent4_Click(object sender, RoutedEventArgs e)
        {
            StartStudent4.IsEnabled = false;
            Finish.IsEnabled = true;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentName = students[3].Name;
            currentStudentID = 4;
            currentIP = student4IP;
            currentPort = 59004;
            question = DAO.QuestionDAO.Instance.getStartQuestion(currentStudentID);

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Start.ContentText.Text = currentStudentName.ToUpper();
            eW_Start.StudentName4.FontWeight = FontWeights.ExtraBold;
        }

        private void StudentIntro_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = true;
            StudentIntro.IsEnabled = false;

            eW_Start.StudentName1.Visibility = Visibility.Hidden;
            eW_Start.StudentName2.Visibility = Visibility.Hidden;
            eW_Start.StudentName3.Visibility = Visibility.Hidden;
            eW_Start.StudentName4.Visibility = Visibility.Hidden;

            eW_Start.ContentText.Visibility = Visibility.Visible;
            eW_Start.QuestionText.Visibility = Visibility.Hidden;
            eW_Start.PointText.Visibility = Visibility.Hidden;
            eW_Start.QuestionBackground.Visibility = Visibility.Hidden;

            eW_Start.StudentName1.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[0].Name.ToLower());
            eW_Start.StudentName2.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[1].Name.ToLower());
            eW_Start.StudentName3.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[2].Name.ToLower());
            eW_Start.StudentName4.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[3].Name.ToLower());

            eW_Start.StudentVideo.Visibility = Visibility.Visible;
            eW_Start.StudentVideo.BringIntoView();
            eW_Start.StudentVideo.Play();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            QuestionIndexText.Text = currentQuestion.ToString();
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            AnswerTextBox.Text = question[currentQuestion - 1].Answer;
            eW_Start.QuestionText.Text = question[currentQuestion - 1].Detail;
            eW_Start.PointText.Visibility = Visibility.Visible;
            eW_Start.TimeVideo.Visibility = Visibility.Visible;
            eW_Start.TimeVideo.Play();
            Main.ClientCollection.Where(c => c.IP == currentIP).Single().Tag.Send("01_01_" + question[currentQuestion - 1].Detail);
            True.IsEnabled = true;
            False.IsEnabled = true;
        }

        private void True_Click(object sender, RoutedEventArgs e)
        {
            TrueSound.Load();
            TrueSound.Play();
            students[currentStudentID - 1].AddPoint(10);
            switch (currentStudentID)
            {
                case 1:
                    Points1.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 2:
                    Points2.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 3:
                    Points3.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 4:
                    Points4.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                default:
                    break;
            }

            eW_Start.PointText.Text = students[currentStudentID - 1].Point.ToString();
            currentQuestion++;

            if (currentQuestion > 12)
            {
                True.IsEnabled = false;
                False.IsEnabled = false;
                ResetControls();
                Main.ClientCollection.Where(c => c.IP == currentIP).Single().Tag.Send("01_02_0");
                return;
            }

            QuestionIndexText.Text = currentQuestion.ToString();
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            AnswerTextBox.Text = question[currentQuestion - 1].Answer;
            eW_Start.QuestionText.Text = question[currentQuestion - 1].Detail;
            Main.ClientCollection.Where(c => c.IP == currentIP).Single().Tag.Send("01_02_" + question[currentQuestion - 1].Detail);
        }

        private void False_Click(object sender, RoutedEventArgs e)
        {
            FalseSound.Load();
            FalseSound.Play();
            currentQuestion++;

            if (currentQuestion > 12)
            {
                True.IsEnabled = false;
                False.IsEnabled = false;
                ResetControls();
                Main.ClientCollection.Where(c => c.IP == currentIP).Single().Tag.Send("01_03_0");
                return;
            }

            QuestionIndexText.Text = currentQuestion.ToString();
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            AnswerTextBox.Text = question[currentQuestion - 1].Answer;
            eW_Start.QuestionText.Text = question[currentQuestion - 1].Detail;
            Main.ClientCollection.Where(c => c.IP == currentIP).Single().Tag.Send("01_03_" + question[currentQuestion - 1].Detail);
        }

        private void ResetControls()
        {
            CurrentStudent.Text = "Thí sinh hiện tại: ";
            CurrentID.Text = "Vị trí thí sinh: ";

            QuestionTextBox.Text = String.Empty;
            AnswerTextBox.Text = String.Empty;
            QuestionIndexText.Text = String.Empty;
            currentStudentName = String.Empty;

            StartSound.Stop();

            eW_Start.TimeVideo.Stop();
            eW_Start.IntroVideo.Stop();
            eW_Start.StudentVideo.Stop();
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            Finish.IsEnabled = false;

            Main.BroadcastData("01_04_" + currentStudentID + "_" + students[currentStudentID - 1].Point);

            eW_Start.StudentName1.FontWeight = FontWeights.Normal;
            eW_Start.StudentName2.FontWeight = FontWeights.Normal;
            eW_Start.StudentName3.FontWeight = FontWeights.Normal;
            eW_Start.StudentName4.FontWeight = FontWeights.Normal;

            Start.IsEnabled = false;

            QuestionTextBox.Text = String.Empty;
            AnswerTextBox.Text = String.Empty;

            eW_Start.TimeVideo.Stop();
            FinishSound.Play();

            currentStudentID = 0;
            currentQuestion = 1;
            QuestionTextBox.Text = String.Empty;
            AnswerTextBox.Text = String.Empty;
            QuestionIndexText.Text = String.Empty;
            currentStudentName = String.Empty;
        }

        public List<Student> getStudent()
        {
            return students;
        }

        public void Luu_Click(object sender, RoutedEventArgs e)
        {
            students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
            students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
            students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
            students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

            students[0].Point = int.Parse(Points1.Text);
            students[1].Point = int.Parse(Points2.Text);
            students[2].Point = int.Parse(Points3.Text);
            students[3].Point = int.Parse(Points4.Text);

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }
    }
}
