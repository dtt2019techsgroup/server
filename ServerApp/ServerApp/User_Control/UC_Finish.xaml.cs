﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using ServerApp.Slide.Start_Round;
using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using System.IO;
using System.Windows.Forms;
using ServerApp.Utilities;

namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UC_Start.xaml
    /// 
    /// </summary>
    /// 

    public partial class UC_Finish : System.Windows.Controls.UserControl
    {
        public EW_Finish eW_Finish;
        public string currentStudentName = string.Empty;
        public int currentStudentID = 0;
        public int currentQuestion = 1;
        public int currentQuestionPoint = 0;
        public int currentSet = 0;
        public int thiSinhConLaiID = 0;
        public bool ngoiSaoHyVong = false;
        List<Question> question = new List<Question>();
        List<Student> students = new List<Student>();

        static string soundPath = Directory.GetCurrentDirectory();
        static string videoPath = Directory.GetCurrentDirectory();

        private readonly MainWindow Main;

        public UC_Finish(MainWindow main, List<Student> stds, EW_Finish ew)
        {
            InitializeComponent();

            Main = main;

            eW_Finish = ew;
            students = stds;
            StartSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhBatDau.mp3", UriKind.RelativeOrAbsolute);
            FinishSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhKetThuc.mp3", UriKind.RelativeOrAbsolute);
            TraLoiDungSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhTraLoiDung.mp3", UriKind.RelativeOrAbsolute);
            TraLoiSaiSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhTraLoiSai.mp3", UriKind.RelativeOrAbsolute);
            ThiSinhConLaiSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhThiSinhConLai.mp3", UriKind.RelativeOrAbsolute);
            ThiSinhConLaiBamChuongSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhThiSinhConLaiBamChuong.mp3", UriKind.RelativeOrAbsolute);
            NgoiSaoHyVongSound.Source = new Uri(soundPath + @"\Resources\4_AmThanhNgoiSaoHyVong.mp3", UriKind.RelativeOrAbsolute);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        private void Intro_Click(object sender, RoutedEventArgs e)
        {
            eW_Finish.IntroVideo.BringIntoView();
            eW_Finish.IntroVideo.Play();
        }

        private void StartStudent1_Click(object sender, RoutedEventArgs e)
        {
            StartStudent1.IsEnabled = false;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentID = 1;
            currentStudentName = students[currentStudentID - 1].Name;


            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Finish.ContentText.Visibility = Visibility.Visible;
            eW_Finish.ContentText.Text = currentStudentName.ToUpper();
            eW_Finish.StudentName1.FontWeight = FontWeights.ExtraBold;
            eW_Finish.StudentName2.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName3.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName4.FontWeight = FontWeights.DemiBold;

            eW_Finish.PointText.Text = students[currentStudentID - 1].Point.ToString();

            NgoiSaoHyVongButton.IsEnabled = true;
        }

        private void StartStudent2_Click(object sender, RoutedEventArgs e)
        {
            StartStudent2.IsEnabled = false;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentID = 2;
            currentStudentName = students[currentStudentID - 1].Name;

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Finish.ContentText.Visibility = Visibility.Visible;
            eW_Finish.ContentText.Text = currentStudentName.ToUpper();
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName2.FontWeight = FontWeights.ExtraBold;
            eW_Finish.StudentName3.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName4.FontWeight = FontWeights.DemiBold;

            NgoiSaoHyVongButton.IsEnabled = true;
        }

        private void StartStudent3_Click(object sender, RoutedEventArgs e)
        {
            StartStudent3.IsEnabled = false;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentID = 3;
            currentStudentName = students[currentStudentID - 1].Name;

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Finish.ContentText.Visibility = Visibility.Visible;
            eW_Finish.ContentText.Text = currentStudentName.ToUpper();
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName2.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName3.FontWeight = FontWeights.ExtraBold;
            eW_Finish.StudentName4.FontWeight = FontWeights.DemiBold;

            NgoiSaoHyVongButton.IsEnabled = true;
        }

        private void StartStudent4_Click(object sender, RoutedEventArgs e)
        {
            StartStudent4.IsEnabled = false;

            StartSound.Stop();
            StartSound.Play();

            //Bật Button StudentIntro
            StudentIntro.IsEnabled = true;

            //Set thông tin thí sinh và câu hỏi
            currentStudentID = 4;
            currentStudentName = students[currentStudentID - 1].Name;

            //Set nội dung cho control
            CurrentStudent.Text = "Thí sinh hiện tại: " + currentStudentName;
            CurrentID.Text = "Vị trí thí sinh: " + currentStudentID;
            eW_Finish.ContentText.Visibility = Visibility.Visible;
            eW_Finish.ContentText.Text = currentStudentName.ToUpper();
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName2.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName3.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName4.FontWeight = FontWeights.ExtraBold;

            NgoiSaoHyVongButton.IsEnabled = true;
        }

        private void StudentIntro_Click(object sender, RoutedEventArgs e)
        {
            StudentIntro.IsEnabled = false;

            eW_Finish.StudentName1.Visibility = Visibility.Hidden;
            eW_Finish.StudentName2.Visibility = Visibility.Hidden;
            eW_Finish.StudentName3.Visibility = Visibility.Hidden;
            eW_Finish.StudentName4.Visibility = Visibility.Hidden;

            eW_Finish.ContentText.Visibility = Visibility.Visible;
            eW_Finish.QuestionText.Visibility = Visibility.Hidden;
            eW_Finish.PointText.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground40.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground60.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground80.Visibility = Visibility.Hidden;

            eW_Finish.StudentName1.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[0].Name.ToLower());
            eW_Finish.StudentName2.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[1].Name.ToLower());
            eW_Finish.StudentName3.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[2].Name.ToLower());
            eW_Finish.StudentName4.Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(students[3].Name.ToLower());

            eW_Finish.StudentVideo.Visibility = Visibility.Visible;
            eW_Finish.StudentVideo.BringIntoView();
            eW_Finish.StudentVideo.Play();
        }

        private void CountDown_Click(object sender, RoutedEventArgs e)
        {
            QuestionIndexText.Text = currentQuestion.ToString();
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            AnswerTextBox.Text = question[currentQuestion - 1].Answer;
            eW_Finish.QuestionText.Text = question[currentQuestion - 1].Detail;
            eW_Finish.PointText.Visibility = Visibility.Visible;
            eW_Finish.TimeVideo.Visibility = Visibility.Visible;
            eW_Finish.TimeVideo.Play();

            True.IsEnabled = true;
            ThiSinhConLai.IsEnabled = true;
        }


        private void Set40_Click(object sender, RoutedEventArgs e)
        {
            currentSet = 40;
            question = QuestionDAO.Instance.getFinishQuestions(currentStudentID, currentSet);

            eW_Finish.currentSet = 40;
            eW_Finish.QuestionSelectionVideo.Source = new Uri(soundPath + @"\Resources\4_Video3.mp4", UriKind.Relative);

            eW_Finish.QuestionBackground60.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground80.Visibility = Visibility.Hidden;

            eW_Finish.QuestionSelectionBackground.Visibility = Visibility.Hidden;
            eW_Finish.QuestionSelectionVideo.Visibility = Visibility.Visible;
            eW_Finish.QuestionSelectionVideo.Play();
        }

        private void Set60_Click(object sender, RoutedEventArgs e)
        {
            currentSet = 60;
            question = QuestionDAO.Instance.getFinishQuestions(currentStudentID, currentSet);

            eW_Finish.currentSet = 60;
            eW_Finish.QuestionSelectionVideo.Source = new Uri(soundPath + @"\Resources\4_Video4.mp4", UriKind.Relative);

            eW_Finish.QuestionBackground40.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground80.Visibility = Visibility.Hidden;

            eW_Finish.QuestionSelectionBackground.Visibility = Visibility.Hidden;
            eW_Finish.QuestionSelectionVideo.Visibility = Visibility.Visible;
            eW_Finish.QuestionSelectionVideo.Play();
        }

        private void Set80_Click(object sender, RoutedEventArgs e)
        {
            currentSet = 80;
            question = QuestionDAO.Instance.getFinishQuestions(currentStudentID, currentSet);

            eW_Finish.currentSet = 80;
            eW_Finish.QuestionSelectionVideo.Source = new Uri(soundPath + @"\Resources\4_Video5.mp4", UriKind.Relative);


            eW_Finish.QuestionBackground40.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground60.Visibility = Visibility.Hidden;

            eW_Finish.QuestionSelectionBackground.Visibility = Visibility.Hidden;
            eW_Finish.QuestionSelectionVideo.Visibility = Visibility.Visible;
            eW_Finish.QuestionSelectionVideo.Play();
        }

        private void Question1_Click(object sender, RoutedEventArgs e)
        {
            switch (currentSet)
            {
                case 40:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video6.mp4", UriKind.Relative);
                    currentQuestionPoint = 10;
                    break;
                case 60:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video6.mp4", UriKind.Relative);
                    currentQuestionPoint = 10;
                    break;
                case 80:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video7.mp4", UriKind.Relative);
                    currentQuestionPoint = 20;
                    break;
                default:
                    break;
            }

            currentQuestion = 1;
            eW_Finish.QuestionText.Text = question[currentQuestion - 1].Detail;
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            QuestionIndexText.Text = currentQuestion.ToString();
            Main.BroadcastData("05_01_" + question[currentQuestion - 1].Detail);
        }

        private void Question2_Click(object sender, RoutedEventArgs e)
        {
            switch (currentSet)
            {
                case 40:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video6.mp4", UriKind.Relative);
                    currentQuestionPoint = 10;
                    break;
                case 60:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video7.mp4", UriKind.Relative);
                    currentQuestionPoint = 20;
                    break;
                case 80:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video8.mp4", UriKind.Relative);
                    currentQuestionPoint = 30;
                    break;
                default:
                    break;
            }

            currentQuestion = 2;
            eW_Finish.QuestionText.Text = question[currentQuestion - 1].Detail;
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            QuestionIndexText.Text = currentQuestion.ToString();
            Main.BroadcastData("05_01_" + question[currentQuestion - 1].Detail);
        }

        private void Question3_Click(object sender, RoutedEventArgs e)
        {
            switch (currentSet)
            {
                case 40:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video7.mp4", UriKind.Relative);
                    currentQuestionPoint = 20;
                    break;
                case 60:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video8.mp4", UriKind.Relative);
                    currentQuestionPoint = 30;
                    break;
                case 80:
                    eW_Finish.TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video8.mp4", UriKind.Relative);
                    currentQuestionPoint = 30;
                    break;
                default:
                    break;
            }

            currentQuestion = 3;
            eW_Finish.QuestionText.Text = question[currentQuestion - 1].Detail;
            QuestionTextBox.Text = question[currentQuestion - 1].Detail;
            QuestionIndexText.Text = currentQuestion.ToString();
            Main.BroadcastData("05_01_" + question[currentQuestion - 1].Detail);
        }

        private void True_Click(object sender, RoutedEventArgs e)
        {
            TraLoiDungSound.Stop();
            TraLoiDungSound.Play();
            if (ngoiSaoHyVong)
                students[currentStudentID - 1].AddPoint(currentQuestionPoint * 2);
            else
                students[currentStudentID - 1].AddPoint(currentQuestionPoint);


            switch (currentStudentID)
            {
                case 1:
                    Points1.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 2:
                    Points2.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 3:
                    Points3.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                case 4:
                    Points4.Text = students[currentStudentID - 1].Point.ToString();
                    break;
                default:
                    break;
            }

            eW_Finish.PointText.Text = students[currentStudentID - 1].Point.ToString();

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            FinishSound.Stop();
            FinishSound.Play();

            QuestionTextBox.Text = String.Empty;
            AnswerTextBox.Text = String.Empty;
            QuestionIndexText.Text = String.Empty;
            currentStudentName = String.Empty;

            eW_Finish.QuestionText.Text = String.Empty;

            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
            eW_Finish.StudentName1.FontWeight = FontWeights.DemiBold;
        }

        private void ThiSinhConLai_Click(object sender, RoutedEventArgs e)
        {
            ThiSinhConLaiSound.Stop();
            ThiSinhConLaiSound.Play();

            if (ngoiSaoHyVong)
            {
                students[currentStudentID - 1].AddPoint(-currentQuestionPoint);
                students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
                students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
                students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
                students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

                students[0].Point = int.Parse(Points1.Text);
                students[1].Point = int.Parse(Points2.Text);
                students[2].Point = int.Parse(Points3.Text);
                students[3].Point = int.Parse(Points4.Text);

                UpdateInfo();

                eW_Finish.PointText.Text = students[currentStudentID - 1].Point.ToString();
            }

            Main.BroadcastData("05_02");
        }

        private void AnCauHoi_Click(object sender, RoutedEventArgs e)
        {
            eW_Finish.QuestionText.Text = String.Empty;
            ThiSinhConLaiName.Visibility = Visibility.Hidden;
            eW_Finish.NgoiSaoHyVongImage.Visibility = Visibility.Hidden;

            eW_Finish.StudentName1Grid.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            eW_Finish.StudentName2Grid.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            eW_Finish.StudentName3Grid.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            eW_Finish.StudentName4Grid.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));

            Main.BroadcastData("05_03");
        }

        #region MediaEnded
        private void StartSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            StartSound.Stop();
        }

        private void FinishSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            FinishSound.Stop();

            eW_Finish.PointText.Text = String.Empty;
            eW_Finish.QuestionText.Text = String.Empty;

            eW_Finish.QuestionSelectionVideo.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground40.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground60.Visibility = Visibility.Hidden;
            eW_Finish.QuestionBackground80.Visibility = Visibility.Hidden;
            eW_Finish.QuestionText.Visibility = Visibility.Hidden;
            eW_Finish.TimeVideo.Visibility = Visibility.Hidden;
            eW_Finish.IntroVideo.Visibility = Visibility.Hidden;
            eW_Finish.StudentVideo.Visibility = Visibility.Hidden;
            eW_Finish.PointText.Visibility = Visibility.Hidden;
            eW_Finish.StudentName1.Visibility = Visibility.Hidden;
            eW_Finish.StudentName2.Visibility = Visibility.Hidden;
            eW_Finish.StudentName3.Visibility = Visibility.Hidden;
            eW_Finish.StudentName4.Visibility = Visibility.Hidden;
            eW_Finish.ContentText.Text = "VỀ ĐÍCH";

        }

        private void TraLoiDungSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            TraLoiDungSound.Stop();
        }
        private void TraLoiSaiSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            TraLoiSaiSound.Stop();
        }

        private void ThiSinhConLaiSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            ThiSinhConLaiSound.Stop();
        }

        private void ThiSinhConLaiBamChuongSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            FinishSound.Stop();
        }
        private void NgoiSaoHyVongSound_MediaEnded(object sender, RoutedEventArgs e)
        {
            NgoiSaoHyVongSound.Stop();
        }
        #endregion

        private void ResetControls()
        {
            CurrentStudent.Text = "Thí sinh hiện tại: ";
            CurrentID.Text = "Vị trí thí sinh: ";

            QuestionTextBox.Text = String.Empty;
            AnswerTextBox.Text = String.Empty;
            QuestionIndexText.Text = String.Empty;
            currentStudentName = String.Empty;

            currentStudentID = 0;
            currentQuestion = 1;

            StartSound.Stop();

            eW_Finish.TimeVideo.Stop();
            eW_Finish.IntroVideo.Stop();
            eW_Finish.StudentVideo.Stop();
        }

        public List<Student> getStudent()
        {
            return students;
        }

        private void NgoiSaoHyVongButton_Click(object sender, RoutedEventArgs e)
        {
            ngoiSaoHyVong = true;
            eW_Finish.NgoiSaoHyVongImage.Visibility = Visibility.Visible;
            NgoiSaoHyVongButton.IsEnabled = false;
            NgoiSaoHyVongSound.Stop();
            NgoiSaoHyVongSound.Play();
        }

        private void ThiSinhConLaiDung_Click(object sender, RoutedEventArgs e)
        {
            TraLoiDungSound.Stop();
            TraLoiDungSound.Play();
            students[thiSinhConLaiID - 1].AddPoint(currentQuestionPoint);

            if (!ngoiSaoHyVong)
            {
                students[currentStudentID - 1].AddPoint(-currentQuestionPoint);
                eW_Finish.PointText.Text = students[currentStudentID - 1].Point.ToString();
            }

            UpdateInfo();
            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);
        }

        private void ThiSinhConLaiSai_Click(object sender, RoutedEventArgs e)
        {
            TraLoiSaiSound.Stop();
            TraLoiSaiSound.Play();
            students[thiSinhConLaiID - 1].AddPoint(-(currentQuestionPoint/2));
            UpdateInfo();
            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);
        }

        public void Luu_Click(object sender, RoutedEventArgs e)
        {
            students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
            students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
            students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
            students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

            students[0].Point = int.Parse(Points1.Text);
            students[1].Point = int.Parse(Points2.Text);
            students[2].Point = int.Parse(Points3.Text);
            students[3].Point = int.Parse(Points4.Text);

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        public void NhanTinHieuThiSinhConLaiTraLoi(int position)
        {
            ThiSinhConLaiBamChuongSound.Stop();
            ThiSinhConLaiBamChuongSound.Play();
            thiSinhConLaiID = position;
            ThiSinhConLaiText.Text = students[position - 1].Name;
            ThiSinhConLaiName.Visibility = Visibility.Visible;
            switch (position)
            {
                case 1:
                    eW_Finish.StudentName1Grid.Background = new SolidColorBrush(Color.FromArgb(255, 130, 30, 3));
                    break;
                case 2:
                    eW_Finish.StudentName2Grid.Background = new SolidColorBrush(Color.FromArgb(255, 130, 30, 3));
                    break;
                case 3:
                    eW_Finish.StudentName3Grid.Background = new SolidColorBrush(Color.FromArgb(255, 130, 30, 3));
                    break;
                case 4:
                    eW_Finish.StudentName4Grid.Background = new SolidColorBrush(Color.FromArgb(255, 130, 30, 3));
                    break;
            }
            Main.BroadcastData("05_04");
        }
    }
}
