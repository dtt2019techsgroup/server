﻿using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using ServerApp.Slide.Obstacle_Round;
using ServerApp.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ServerApp.User_Control
{
    /// <summary>
    /// Interaction logic for UC_Obstacle.xaml
    /// </summary>
    public partial class UC_Obstacle : UserControl
    {
        EW_Obstacle eW_Obstacle;
        public readonly MainWindow Main;
        List<Student> students;
        private List<Question> listQuestion;
        static String keyname { get; set; }
        static String Image { get; set; }
        private static int qs_count { get; set; }

        const int DEFAULT_TIME = 15;
        const int MAXIMUM_STDS_NUM = 4;
        const int POINTS_PER_NQUESTION = 10;

        //Time-related
        int cur_time = DEFAULT_TIME;
        int cur_score = 0;
        int cur_question = 0;
        private QuestionDAO quesDAO;
        public bool IsWrongAnswer = false;

        Border[][] WordsBorder = new Border[4][];
        TextBlock[][] Words = new TextBlock[4][];

        //Sound
        static string soundPath = Directory.GetCurrentDirectory();
        SoundPlayer OpenPartSound = new SoundPlayer(soundPath + @"\Resources\2_AmThanhMoHinhAnhGoiY.wav");
        

        public UC_Obstacle(MainWindow main, List<Student> stds, EW_Obstacle ew)
        {
            InitializeComponent();

            Main = main;
            eW_Obstacle = ew;
            qs_count = 0;
            students = stds;
            quesDAO = new QuestionDAO();
            listQuestion = new List<Question>();

            listQuestion = DAO.QuestionDAO.Instance.getObstacleQuestion();

            StudentIDList.Items.Add("1");
            StudentIDList.Items.Add("2");
            StudentIDList.Items.Add("3");
            StudentIDList.Items.Add("4");

            //mp3 sound
            WordsSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhDanhSachHangNgang.mp3", UriKind.RelativeOrAbsolute);
            LineSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhChonHangNgang.mp3", UriKind.RelativeOrAbsolute);
            StartSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhBatDau.mp3", UriKind.RelativeOrAbsolute);
            TrueSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhTraLoiDungHangNgang.mp3", UriKind.RelativeOrAbsolute);
            FalseSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhTraLoiSaiCNV.mp3", UriKind.RelativeOrAbsolute);
            VictorySound.Source = new Uri(soundPath + @"\Resources\2_AmThanhTraLoiDungCNV.mp3", UriKind.RelativeOrAbsolute);
            KeyAnswerSound.Source = new Uri(soundPath + @"\Resources\2_AmThanhTinHieuTraLoi.mp3", UriKind.RelativeOrAbsolute);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        private void Intro_Click(object sender, RoutedEventArgs e)
        {
            eW_Obstacle.IntroVideo.BringIntoView();
            eW_Obstacle.IntroVideo.Play();
        }

        public int getActualWordLength(List<Question> questions, int curQ)
        {
            int length = listQuestion[curQ].Answer.Length;
            for (int j = 0; j < listQuestion[curQ].Answer.Length; j++)
            {
                if (listQuestion[curQ].Answer[j].Equals(' '))
                    length -= 1;
            }
            return length;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            switch (eW_Obstacle.first_time)
            {
                case true:
                    eW_Obstacle.ContentText.Visibility = Visibility.Hidden;
                    eW_Obstacle.first_time = false;
                    eW_Obstacle.QuestionVideo.BringIntoView();
                    eW_Obstacle.QuestionVideo.Play();

                    WordsSound.Stop();
                    WordsSound.Play();

                    for (int i = 0; i < 4; ++i)
                    {
                        int length = getActualWordLength(listQuestion, i);
                        WordsBorder[i] = new Border[length];
                        Words[i] = new TextBlock[length];
                        for (int j = 0; j < length; j++)
                        {
                            WordsBorder[i][j] = new Border();
                            WordsBorder[i][j].Background = Brushes.White;
                            WordsBorder[i][j].Margin = new Thickness(5);
                            WordsBorder[i][j].Width = 60;
                            WordsBorder[i][j].Height = 60;
                            WordsBorder[i][j].VerticalAlignment = VerticalAlignment.Center;
                            WordsBorder[i][j].HorizontalAlignment = HorizontalAlignment.Center;

                            Words[i][j] = new TextBlock();                            
                            Words[i][j].Background = Brushes.Transparent;
                            Words[i][j].FontSize = 35;
                            Words[i][j].VerticalAlignment = VerticalAlignment.Center;
                            Words[i][j].HorizontalAlignment = HorizontalAlignment.Center;
                            switch (i)
                            {
                                case 0:
                                    WordsBorder[i][j].Child = Words[i][j];
                                    eW_Obstacle.Word1.Children.Add(WordsBorder[i][j]);
                                    break;
                                case 1:
                                    WordsBorder[i][j].Child = Words[i][j];
                                    eW_Obstacle.Word2.Children.Add(WordsBorder[i][j]);
                                    break;
                                case 2:
                                    WordsBorder[i][j].Child = Words[i][j];
                                    eW_Obstacle.Word3.Children.Add(WordsBorder[i][j]);
                                    break;
                                case 3:
                                    WordsBorder[i][j].Child = Words[i][j];
                                    eW_Obstacle.Word4.Children.Add(WordsBorder[i][j]);
                                    break;
                            }
                        }
                    }

                    //for (int i = 0; i < listQuestion[0].Answer.Length; ++i)
                    //{

                    //}
                    //for (int i = 0; i < listQuestion[1].Answer.Length; ++i)
                    //{
                    //    eW_Obstacle.Word2.Children.Add(Words[1][i]);
                    //}
                    //for (int i = 0; i < listQuestion[2].Answer.Length; ++i)
                    //{
                    //    eW_Obstacle.Word3.Children.Add(Words[2][i]);
                    //}
                    //for (int i = 0; i < listQuestion[3].Answer.Length; ++i)
                    //{
                    //    eW_Obstacle.Word4.Children.Add(Words[3][i]);
                    //}

                    Start.IsEnabled = false;
                    break;
                default:
                    eW_Obstacle.TimeVideo.Play();
                    Submit.IsEnabled = false;
                    Main.BroadcastData("02_02");
                    break;
            }
        }

        private void Question1_Click(object sender, RoutedEventArgs e)
        {
            Question1.IsEnabled = false;

            cur_question = 1;
            IsWrongAnswer = false;
            for (int i = 0; i < getActualWordLength(listQuestion, cur_question - 1); ++i)
                WordsBorder[0][i].Background = Brushes.Red;
            LineSound.Stop();
            LineSound.Play();
            Thread.Sleep(1000);
            Question.Text = eW_Obstacle.QuestionText.Text = listQuestion[cur_question - 1].Detail;
            Answer.Text = listQuestion[cur_question - 1].Answer;
            Start.IsEnabled = true;
            Main.BroadcastData("02_01_1_" + listQuestion[cur_question - 1].Detail);
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private void Question2_Click(object sender, RoutedEventArgs e)
        {
            Question2.IsEnabled = false;

            cur_question = 2;
            IsWrongAnswer = false;
            for (int i = 0; i < getActualWordLength(listQuestion, cur_question - 1); ++i)
                WordsBorder[1][i].Background = Brushes.Red;
            LineSound.Stop();
            LineSound.Play();
            Thread.Sleep(1000);
            Question.Text = eW_Obstacle.QuestionText.Text = listQuestion[cur_question - 1].Detail;
            Answer.Text = listQuestion[cur_question - 1].Answer;
            Start.IsEnabled = true;
            Main.BroadcastData("02_01_2_" + listQuestion[cur_question - 1].Detail);
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private void Question3_Click(object sender, RoutedEventArgs e)
        {
            Question3.IsEnabled = false;

            cur_question = 3;
            IsWrongAnswer = false;
            for (int i = 0; i < getActualWordLength(listQuestion, cur_question - 1); ++i)
                WordsBorder[2][i].Background = Brushes.Red;
            LineSound.Stop();
            LineSound.Play();
            Thread.Sleep(1000);
            Question.Text = eW_Obstacle.QuestionText.Text = listQuestion[cur_question - 1].Detail;
            Answer.Text = listQuestion[cur_question - 1].Answer;
            Start.IsEnabled = true;
            Main.BroadcastData("02_01_3_" + listQuestion[cur_question - 1].Detail);
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private void Question4_Click(object sender, RoutedEventArgs e)
        {
            Question4.IsEnabled = false;

            cur_question = 4;
            IsWrongAnswer = false;
            for (int i = 0; i < getActualWordLength(listQuestion, cur_question - 1); ++i)
                WordsBorder[3][i].Background = Brushes.Red;
            LineSound.Stop();
            LineSound.Play();
            Thread.Sleep(1000);

            Question.Text = eW_Obstacle.QuestionText.Text = listQuestion[cur_question - 1].Detail;
            Answer.Text = listQuestion[cur_question - 1].Answer;
            Start.IsEnabled = true;
            Main.BroadcastData("02_01_4_" + listQuestion[cur_question - 1].Detail);
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private void Question5_Click(object sender, RoutedEventArgs e)
        {
            Question5.IsEnabled = false;

            cur_question = 5;
            IsWrongAnswer = false;
            LineSound.Stop();
            LineSound.Play();
            Thread.Sleep(1000);

            Question.Text = eW_Obstacle.QuestionText.Text = listQuestion[cur_question - 1].Detail;
            Answer.Text = listQuestion[cur_question - 1].Answer;
            Start.IsEnabled = true;
            Main.BroadcastData("02_01_5_" + listQuestion[cur_question - 1].Detail);
            Answer1.Text = String.Empty;
            Answer2.Text = String.Empty;
            Answer3.Text = String.Empty;
            Answer4.Text = String.Empty;
        }

        private void Answers_Click(object sender, RoutedEventArgs e)
        {
            eW_Obstacle.Upper_part.Visibility = Visibility.Hidden;
            eW_Obstacle.Lower_part.Visibility = Visibility.Hidden;
            eW_Obstacle.QuestionBackground.Visibility = Visibility.Hidden;
            eW_Obstacle.AnswerVideo.Visibility = Visibility.Visible;

            eW_Obstacle.StudentName1.Text = students[0].Name;
            eW_Obstacle.StudentName2.Text = students[1].Name;
            eW_Obstacle.StudentName3.Text = students[2].Name;
            eW_Obstacle.StudentName4.Text = students[3].Name;

            eW_Obstacle.Answer1.Text = Answer1.Text.ToUpper();
            eW_Obstacle.Answer2.Text = Answer2.Text.ToUpper();
            eW_Obstacle.Answer3.Text = Answer3.Text.ToUpper();
            eW_Obstacle.Answer4.Text = Answer4.Text.ToUpper();

            eW_Obstacle.AnswerVideo.BringIntoView();
            eW_Obstacle.AnswerVideo.Play();

            Submit.IsEnabled = true;
        }

        //Logical method for Key buttons
        private void KeyButton_Checked(object sender, RoutedEventArgs e)
        {
            keyname = String.Copy(((ToggleButton)e.Source).Name);
            switch (keyname)
            {
                case "Key1":
                    Key2.IsChecked = false;
                    Key3.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key2":
                    Key1.IsChecked = false;
                    Key3.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key3":
                    Key2.IsChecked = false;
                    Key1.IsChecked = false;
                    Key4.IsChecked = false;
                    break;
                case "Key4":
                    Key2.IsChecked = false;
                    Key3.IsChecked = false;
                    Key1.IsChecked = false;
                    break;
            }
            if (Key1.IsChecked == Key2.IsChecked == Key3.IsChecked == Key4.IsChecked)
            {
                keyname = "";
            }
        }

        //Logical method for Submit buttons
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            switch (keyname)
            {
                case "Key1":
                    AddKeyPoint(qs_count, 0);
                    VictorySound.Play();
                    break;
                case "Key2":
                    AddKeyPoint(qs_count, 1);
                    VictorySound.Play();
                    break;
                case "Key3":
                    AddKeyPoint(qs_count, 2);
                    VictorySound.Play();
                    break;
                case "Key4":
                    AddKeyPoint(qs_count, 3);
                    VictorySound.Play();
                    break;
                default:
                    break;
            }


            if (Point1.IsChecked == true)
            {
                students[0].Point = students[0].Point + POINTS_PER_NQUESTION;
            }
            if (Point2.IsChecked == true)
            {
                students[1].Point = students[1].Point + POINTS_PER_NQUESTION;
            }
            if (Point3.IsChecked == true)
            {
                students[2].Point = students[2].Point + POINTS_PER_NQUESTION;
            }
            if (Point4.IsChecked == true)
            {
                students[3].Point = students[3].Point + POINTS_PER_NQUESTION;
            }

            if (Point1.IsChecked == true || Point2.IsChecked == true || Point3.IsChecked == true || Point4.IsChecked == true)
            {
                TrueSound.Stop();
                FalseSound.Stop();
                TrueSound.Play();
                OpenParts.IsEnabled = true;
                Main.BroadcastData("02_03_" + cur_question + "_" + listQuestion[cur_question - 1].Answer);
                IsWrongAnswer = false;
            }

            if (Point1.IsChecked == false && Point2.IsChecked == false && Point3.IsChecked == false && Point4.IsChecked == false)
            {
                TrueSound.Stop();
                FalseSound.Stop();
                FalseSound.Play();
                Main.BroadcastData("02_04_" + cur_question);
                IsWrongAnswer = true;
            }
            qs_count++;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        private void OpenParts_Click(object sender, RoutedEventArgs e)
        {
            eW_Obstacle.ShowAns.Visibility = Visibility.Hidden;
            eW_Obstacle.AnswerImagesLayout.Visibility = Visibility.Visible;


            Thread.Sleep(2000);
            OpenPartSound.Stop();
            OpenPartSound.Play();

            switch (cur_question)
            {
                case 1:
                    eW_Obstacle.AnsImg1.Visibility = Visibility.Hidden;
                    AddWords(cur_question);
                    break;
                case 2:
                    eW_Obstacle.AnsImg2.Visibility = Visibility.Hidden;
                    AddWords(cur_question);
                    break;
                case 3:
                    eW_Obstacle.AnsImg3.Visibility = Visibility.Hidden;
                    AddWords(cur_question);
                    break;
                case 4:
                    eW_Obstacle.AnsImg4.Visibility = Visibility.Hidden;
                    AddWords(cur_question);
                    break;
            }
            OpenParts.IsEnabled = false;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            eW_Obstacle.QuestionText.Text = String.Empty;
            Point1.IsChecked = Point2.IsChecked = Point3.IsChecked = Point4.IsChecked = false;

            eW_Obstacle.TimeVideo.Stop();
            eW_Obstacle.AnswerVideo.Stop();
            eW_Obstacle.TimeVideo.Visibility = Visibility.Visible;
            eW_Obstacle.AnswerImagesLayout.Visibility = Visibility.Hidden;
            eW_Obstacle.ShowAns.Visibility = Visibility.Hidden;
            eW_Obstacle.Upper_part.Visibility = Visibility.Visible;
            eW_Obstacle.Lower_part.Visibility = Visibility.Visible;
            eW_Obstacle.QuestionBackground.Visibility = Visibility.Visible;

            if (IsWrongAnswer)
            {
                for (int i = 0; i < listQuestion[cur_question - 1].Answer.Length - 1; i++)
                {
                    WordsBorder[cur_question - 1][i].Background = Brushes.DarkGray;
                }
            }
        }

        private void ShowImg_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn mở chướng ngại vật?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question)
                == MessageBoxResult.Yes)
            {
                eW_Obstacle.ShowAns.Visibility = Visibility.Hidden;
                eW_Obstacle.AnswerImagesLayout.Visibility = Visibility.Visible;
                eW_Obstacle.Layer1.Visibility = Visibility.Hidden;
                eW_Obstacle.Layer2.Visibility = Visibility.Hidden;
            }
        }

        public void AddWords(int cur_question)
        {
            int length = 0;
            int curr = 0;
            switch (cur_question)
            {
                case 1:
                    eW_Obstacle.Word1 = new StackPanel();
                    break;
                case 2:
                    eW_Obstacle.Word2 = new StackPanel();
                    break;
                case 3:
                    eW_Obstacle.Word3 = new StackPanel();
                    break;
                case 4:
                    eW_Obstacle.Word4 = new StackPanel();
                    break;
            }
            length = 0;
            curr = 0;
            while (curr < listQuestion[cur_question - 1].Answer.Length)
            {
                if (listQuestion[cur_question - 1].Answer[curr].Equals(' ') != true)
                {
                    WordsBorder[cur_question - 1][length].Background = Brushes.Azure;
                    Words[cur_question - 1][length].Foreground = Brushes.MidnightBlue;
                    Words[cur_question - 1][length].FontSize = 35;
                    Words[cur_question - 1][length].FontWeight = FontWeights.Bold;
                    Words[cur_question - 1][length].Text = listQuestion[cur_question - 1].Answer[curr].ToString();
                    length++;
                }
                curr++;

            }
        }
        //Add point when any Key is held
        private void AddKeyPoint(int count, int i)
        {
            switch (count)
            {
                case 1:
                    students[i].Point = students[i].Point + 8 * POINTS_PER_NQUESTION;
                    break;
                case 2:
                    students[i].Point = students[i].Point + 6 * POINTS_PER_NQUESTION;
                    break;
                case 3:
                    students[i].Point = students[i].Point + 4 * POINTS_PER_NQUESTION;
                    break;
                case 4:
                    students[i].Point = students[i].Point + 2 * POINTS_PER_NQUESTION;
                    break;
                default:
                    students[i].Point = students[i].Point + POINTS_PER_NQUESTION;
                    break;
            }

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();

            Main.BroadcastData("06_"
            + students[0].Name + "_"
            + students[0].Point + "_"
            + students[1].Name + "_"
            + students[1].Point + "_"
            + students[2].Name + "_"
            + students[2].Point + "_"
            + students[3].Name + "_"
            + students[3].Point);
        }

        //Return the Students List 
        public List<Student> getStudent()
        {
            return students;
        }

        public void getQuestions()
        {

            try
            {
                //UtilityMethods utility = new UtilityMethods();
                for (int i = 7; i <= 10; i++)
                {
                    //Question question = quesDAO.getQuestion(i);
                    //listQuestion.Add(question);
                    //_listQA.Add(utility.fromQuestionToQA(question));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void Luu_Click(object sender, RoutedEventArgs e)
        {
            students[0].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name1.Text.ToLower());
            students[1].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name2.Text.ToLower());
            students[2].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name3.Text.ToLower());
            students[3].Name = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name4.Text.ToLower());

            students[0].Point = int.Parse(Points1.Text);
            students[1].Point = int.Parse(Points2.Text);
            students[2].Point = int.Parse(Points3.Text);
            students[3].Point = int.Parse(Points4.Text);

            Main.BroadcastData("06_"
                + students[0].Name + "_"
                + students[0].Point + "_"
                + students[1].Name + "_"
                + students[1].Point + "_"
                + students[2].Name + "_"
                + students[2].Point + "_"
                + students[3].Name + "_"
                + students[3].Point);

            Name1.Text = students[0].Name;
            Name2.Text = students[1].Name;
            Name3.Text = students[2].Name;
            Name4.Text = students[3].Name;

            Points1.Text = students[0].Point.ToString();
            Points2.Text = students[1].Point.ToString();
            Points3.Text = students[2].Point.ToString();
            Points4.Text = students[3].Point.ToString();
        }

        public void NhanTinHieuTraLoi(int position, string answer)
        {
            switch (position)
            {
                case 1:
                    Answer1.Text = answer;
                    break;
                case 2:
                    Answer2.Text = answer;
                    break;
                case 3:
                    Answer3.Text = answer;
                    break;
                case 4:
                    Answer4.Text = answer;
                    break;
                default:
                    break;
            }
        }

        public void NhanTinHieuTraLoiCNV(int position)
        {
            KeyAnswerSound.Stop();
            KeyAnswerSound.Play();
            switch (position)
            {
                case 1:
                    ObstacleText1.Text = students[0].Name;
                    eW_Obstacle.ObstacleText1.Text = students[0].Name;
                    ObstacleFromContestant1.Visibility = Visibility.Visible;
                    eW_Obstacle.ObstacleFromContestant1.Visibility = Visibility.Visible;
                    break;
                case 2:
                    ObstacleText2.Text = students[1].Name;
                    eW_Obstacle.ObstacleText2.Text = students[1].Name;
                    ObstacleFromContestant2.Visibility = Visibility.Visible;
                    eW_Obstacle.ObstacleFromContestant2.Visibility = Visibility.Visible;
                    break;
                case 3:
                    ObstacleText3.Text = students[2].Name;
                    eW_Obstacle.ObstacleText3.Text = students[2].Name;
                    ObstacleFromContestant3.Visibility = Visibility.Visible;
                    eW_Obstacle.ObstacleFromContestant3.Visibility = Visibility.Visible;
                    break;
                case 4:
                    ObstacleText4.Text = students[3].Name;
                    eW_Obstacle.ObstacleText4.Text = students[3].Name;
                    ObstacleFromContestant4.Visibility = Visibility.Visible;
                    eW_Obstacle.ObstacleFromContestant4.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
        }

        private void AnTinHieuTraLoiCNV_Click(object sender, RoutedEventArgs e)
        {
            ObstacleFromContestant1.Visibility = Visibility.Hidden;
            ObstacleFromContestant2.Visibility = Visibility.Hidden;
            ObstacleFromContestant3.Visibility = Visibility.Hidden;
            ObstacleFromContestant4.Visibility = Visibility.Hidden;

            eW_Obstacle.ObstacleFromContestant1.Visibility = Visibility.Hidden;
            eW_Obstacle.ObstacleFromContestant2.Visibility = Visibility.Hidden;
            eW_Obstacle.ObstacleFromContestant3.Visibility = Visibility.Hidden;
            eW_Obstacle.ObstacleFromContestant4.Visibility = Visibility.Hidden;
        }

        private void TraLoiDungCNV_Click(object sender, RoutedEventArgs e)
        {
            VictorySound.Stop();
            VictorySound.Play();
        }

        private void TraLoiSaiCNV_Click(object sender, RoutedEventArgs e)
        {
            FalseSound.Stop();
            FalseSound.Play();
            Main.ClientCollection.Where(c => c.Position == int.Parse(StudentIDList.SelectedValue.ToString())).SingleOrDefault().Tag.Send("02_05");
        }
    }
}
