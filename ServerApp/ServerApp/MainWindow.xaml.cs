﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ServerApp.User_Control;
using ServerApp.DAO;
using ServerApp.DTO;
using ServerApp.ExtendedWindows;
using ServerApp.Utilities;
using System.Net.Sockets;
using System.Collections.ObjectModel;
using System.IO;

namespace ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        TabItem _tabUserPage;

        public List<Student> students = new List<Student>();
        Student s1 = new Student("Thí sinh 1", "10", 0, 1);
        Student s2 = new Student("Thí sinh 2", "10", 0, 1);
        Student s3 = new Student("Thí sinh 3", "10", 0, 1);
        Student s4 = new Student("Thí sinh 4", "10", 0, 1);

        //Init Extended windows
        public ExtendedWindow extendedWindow = new ExtendedWindow();
        public EW_Start eW_Start = new EW_Start();
        public EW_Obstacle eW_Obstacle = new EW_Obstacle();
        public EW_Accelerate eW_Accelerate = new EW_Accelerate();
        public EW_Decode eW_Decode = new EW_Decode();
        public EW_Finish eW_Finish = new EW_Finish();
        public EW_Summary eW_Summary;

        //User control variables 
        public UC_Input uC_Input;
        public UC_Start uC_Start;
        public UC_Obstacle uC_Obstacle;
        public UC_Accelerate uC_Accelerate;
        public UC_Decode uC_Decode;
        public UC_Finish uC_Finish;

        public int index = -1; //(?)

        private readonly Listener listener;
        
        //Linh's comment: not static, may be the cause to the bug that cause system shutdown whenever any clients got disconnected.
        public List<Socket> clients = new List<Socket>(); // store all the clients into a list

        public ObservableCollection<ClientInfo> ClientCollection { get; set; }

        public string text = string.Empty; //(?)

        public MainWindow()
        {
            InitializeComponent();

            students.Add(s1);
            students.Add(s2);
            students.Add(s3);
            students.Add(s4);

            eW_Summary = new EW_Summary(students);

            uC_Input = new UC_Input(this, students);
            uC_Start = new UC_Start(this, students, eW_Start);
            uC_Obstacle = new UC_Obstacle(this, students, eW_Obstacle);
            uC_Accelerate = new UC_Accelerate(this, students, eW_Accelerate);
            uC_Decode = new UC_Decode(this, students, eW_Decode);
            uC_Finish = new UC_Finish(this, students, eW_Finish);

            MainTab.Items.Clear();
            _tabUserPage = new TabItem { Content = uC_Input };
            MainTab.Items.Add(_tabUserPage);
            MainTab.Items.Refresh();

            this.DataContext = this;

            string directory = Directory.GetCurrentDirectory();

            //Read IP Addresses from the .txt file
            text = System.IO.File.ReadAllText(directory + @"\IP.txt");
            string ip;
            int port;
            if (!text.Equals(string.Empty))
            {
                ip = text.Split(':')[0];
                port = int.Parse(text.Split(':')[1]);
            }
            else
            {
                ip = "0.0.0.0";
                port = 59000;
            }

            listener = new Listener(ip, port);
            listener.SocketAccepted += listener_SocketAccepted;

            ClientCollection = new ObservableCollection<ClientInfo>();
        }

        public void BroadcastData(string data) // send to all clients
        {
            foreach (var socket in clients)
            {
                try
                {
                    socket.Send(Encoding.Unicode.GetBytes(data));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        // SocketAcceptedHandler
        private void listener_SocketAccepted(Socket e)
        {
            var client = new Client(e);
            client.Received += client_Received;
            client.Disconnected += client_Disconnected;
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                string ip = client.Ip.ToString();
                ClientInfo clientInfo = new ClientInfo(ip, " ", " ", client);
                ClientCollection.Add(clientInfo);
                //MessageBox.Show(clientInfo.IP + " has joined.");
                clients.Add(e);
            });
        }

        private void client_Disconnected(Client sender)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                for (int i = 0; i < ClientCollection.Count; i++)
                {
                    var client = ClientCollection[i].Tag as Client;
                    if (client.Ip == sender.Ip)
                    {
                        //MessageBox.Show(ClientCollection[i].IP + " has left.");
                        ClientCollection.RemoveAt(i);
                    }
                }
            });
        }

        private void client_Received(Client sender, byte[] data)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                for (int i = 0; i < ClientCollection.Count; i++)
                {
                    var client = ClientCollection[i].Tag as Client;
                    if (client == null || client.Ip != sender.Ip) continue;
                    var command = Encoding.Unicode.GetString(data).Split('_');
                    switch (command[0])
                    {
                        case "10":
                            {
                                switch (command[1])
                                {
                                    case "00":
                                        uC_Finish.NhanTinHieuThiSinhConLaiTraLoi(1);
                                        break;
                                    case "01":
                                        uC_Obstacle.NhanTinHieuTraLoi(1, command[2]);
                                        break;
                                    case "02":
                                        uC_Obstacle.NhanTinHieuTraLoiCNV(1);
                                        break;
                                    case "03":
                                        uC_Accelerate.NhanTinHieuTraLoi(1, command[2], int.Parse(command[3]));
                                        break;
                                    case "04":
                                        uC_Decode.NhanTinHieuTraLoi(1, command[2]);
                                        break;
                                    case "05":
                                        uC_Decode.NhanTinHieuTraLoiTuKhoa(1);
                                        break;
                                }
                            }
                            break;
                        case "20":
                            {
                                switch (command[1])
                                {
                                    case "00":
                                        uC_Finish.NhanTinHieuThiSinhConLaiTraLoi(2);
                                        break;
                                    case "01":
                                        uC_Obstacle.NhanTinHieuTraLoi(2, command[2]);
                                        break;
                                    case "02":
                                        uC_Obstacle.NhanTinHieuTraLoiCNV(2);
                                        break;
                                    case "03":
                                        uC_Accelerate.NhanTinHieuTraLoi(2, command[2], int.Parse(command[3]));
                                        break;
                                    case "04":
                                        uC_Decode.NhanTinHieuTraLoi(2, command[2]);
                                        break;
                                    case "05":
                                        uC_Decode.NhanTinHieuTraLoiTuKhoa(2);
                                        break;
                                }
                            }
                            break;
                        case "30":
                            {
                                switch (command[1])
                                {
                                    case "00":
                                        uC_Finish.NhanTinHieuThiSinhConLaiTraLoi(3);
                                        break;
                                    case "01":
                                        uC_Obstacle.NhanTinHieuTraLoi(3, command[2]);
                                        break;
                                    case "02":
                                        uC_Obstacle.NhanTinHieuTraLoiCNV(3);
                                        break;
                                    case "03":
                                        uC_Accelerate.NhanTinHieuTraLoi(3, command[2], int.Parse(command[3]));
                                        break;
                                    case "04":
                                        uC_Decode.NhanTinHieuTraLoi(3, command[2]);
                                        break;
                                    case "05":
                                        uC_Decode.NhanTinHieuTraLoiTuKhoa(3);
                                        break;
                                }
                            }
                            break;
                        case "40":
                            {
                                switch (command[1])
                                {
                                    case "00":
                                        uC_Finish.NhanTinHieuThiSinhConLaiTraLoi(4);
                                        break;
                                    case "01":
                                        uC_Obstacle.NhanTinHieuTraLoi(4, command[2]);
                                        break;
                                    case "02":
                                        uC_Obstacle.NhanTinHieuTraLoiCNV(4);
                                        break;
                                    case "03":
                                        uC_Accelerate.NhanTinHieuTraLoi(4, command[2], int.Parse(command[3]));
                                        break;
                                    case "04":
                                        uC_Decode.NhanTinHieuTraLoi(4, command[2]);
                                        break;
                                    case "05":
                                        uC_Decode.NhanTinHieuTraLoiTuKhoa(4);
                                        break;
                                }
                            }
                            break;
                    }
                }
            });
        }

        //Close listener
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            listener.Stop();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            listener.Start();
            extendedWindow.Show();
        }

        private void button_Clicked(object sender, RoutedEventArgs e)
        {
            int index = int.Parse(((Button)e.Source).Uid);

            switch (index)
            {
                case 0:
                    extendedWindow.main.Content = eW_Start;
                    extendedWindow.summary.Visibility = Visibility.Hidden;
                    MainTab.Items.Clear();
                    _tabUserPage = new TabItem { Content = uC_Start };
                    MainTab.Items.Add(_tabUserPage);   
                    MainTab.Items.Refresh();
                    uC_Start.SetIP();
                    BroadcastData("01_00_0");
                    uC_Start.UpdateInfo();
                    break;

                case 1:
                    extendedWindow.main.Content = eW_Obstacle;
                    extendedWindow.summary.Visibility = Visibility.Hidden;
                    MainTab.Items.Clear();
                    _tabUserPage = new TabItem { Content = uC_Obstacle };
                    MainTab.Items.Add(_tabUserPage);
                    MainTab.Items.Refresh();
                    BroadcastData("02_00_0");
                    uC_Obstacle.UpdateInfo();
                    break;

                case 2:
                    extendedWindow.main.Content = eW_Accelerate;
                    extendedWindow.summary.Visibility = Visibility.Hidden;
                    MainTab.Items.Clear();
                    _tabUserPage = new TabItem { Content = uC_Accelerate };
                    MainTab.Items.Add(_tabUserPage);  
                    MainTab.Items.Refresh();
                    BroadcastData("03_00_0");
                    uC_Accelerate.UpdateInfo();
                    break;

                case 3:
                    extendedWindow.main.Content = eW_Decode;
                    extendedWindow.summary.Visibility = Visibility.Hidden;
                    MainTab.Items.Clear();
                    _tabUserPage = new TabItem { Content = uC_Decode};
                    MainTab.Items.Add(_tabUserPage);
                    MainTab.Items.Refresh();
                    BroadcastData("04_00_0");
                    uC_Decode.UpdateInfo();
                    break;

                case 4:
                    extendedWindow.main.Content = eW_Finish;
                    extendedWindow.summary.Visibility = Visibility.Hidden;
                    MainTab.Items.Clear();
                    _tabUserPage = new TabItem { Content = uC_Finish };
                    MainTab.Items.Add(_tabUserPage);    
                    MainTab.Items.Refresh();
                    students = new List<Student>(uC_Finish.getStudent());
                    BroadcastData("05_00_0");
                    uC_Finish.UpdateInfo();
                    break;

                default:
                    break;
            }
        }

        private void Summary_Click(object sender, RoutedEventArgs e)
        {
            extendedWindow.summary.Content = eW_Summary;
            extendedWindow.summary.Visibility = Visibility.Visible;
            eW_Summary.UpdateInfo();
            eW_Summary.SummarySound.Stop();
            eW_Summary.SummarySound.Play();
        }
    }
}
