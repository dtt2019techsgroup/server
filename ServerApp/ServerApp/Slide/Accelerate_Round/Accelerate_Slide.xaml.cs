﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerApp.Slide.Accelerate_Round
{
    /// <summary>
    /// Interaction logic for Accelerate_Slide.xaml
    /// </summary>
    public partial class Accelerate_Slide : Window
    {
        public Accelerate_Slide()
        {
            InitializeComponent();
        }
        public void openVideo(MediaElement video)
        {
            video.Play();
        }

        private void Hidden_Open(object sender, RoutedEventArgs e)
        {
            MediaElement video = sender as MediaElement;
            video.Visibility = Visibility.Hidden;
        }
    }
}
