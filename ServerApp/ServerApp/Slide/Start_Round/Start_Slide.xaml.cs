﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerApp.Slide.Start_Round
{
    /// <summary>
    /// Interaction logic for Start_Slide.xaml
    /// </summary>
    public partial class Start_Slide : Window
    {
        
        public Start_Slide()
        {
            InitializeComponent();
           
        }
        public void openVideo(MediaElement video)
        {
            video.Play();
        }
        public void showQuestion(String question)
        {
            this.txtQuestion.Text = question;
        }
        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Canvas canvas = sender as Canvas;
            SizeChangedEventArgs canvas_Changed_Args = e;

            if (canvas_Changed_Args.PreviousSize.Width == 0) return;

            double old_Height = canvas_Changed_Args.PreviousSize.Height;

            double new_Height = canvas_Changed_Args.NewSize.Height;

            double old_Width = canvas_Changed_Args.PreviousSize.Width;

            double new_Width = canvas_Changed_Args.NewSize.Width;

            double scale_Width = new_Width / old_Width;

            double scale_Height = new_Height / old_Height;

            foreach (FrameworkElement element in canvas.Children)
            {

                //< get >

                double old_Left = Canvas.GetLeft(element);

                double old_Top = Canvas.GetTop(element);

                double old_Bottom = Canvas.GetBottom(element);
  

                //</ get >



                // < set Left-Top>

                Canvas.SetLeft(element, old_Left * scale_Width);

               

                Canvas.SetTop(element, old_Top * scale_Height);

                Canvas.SetBottom(element, old_Bottom * scale_Height);

                // </ set Left-Top >



                //< set Width-Heigth >

                element.Width = element.Width * scale_Width;

                element.Height = element.Height * scale_Height;

                //</ set Width-Heigth >

            }

            //----</ adapt all children >----



            //----------------</ Canvas_SizeChanged() >----------------
        }

        

       
    }
}
