﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ServerApp.DTO;
namespace ServerApp.Slide.Obstacle_Round
{
    /// <summary>
    /// Interaction logic for Accelerate_Slide.xaml
    /// </summary>
    public partial class Obstacle_Slide : Window
    {
        public List<Question> QuestionList;
        public const int circleWidth = 166;
        public Obstacle_Slide()
        {
            QuestionList = new List<Question>();
            InitializeComponent();            
            
        }
        public void openVideo(MediaElement video)
        {
            video.Play();
        }
        public void receiveAnswer(List<Question> ls)
        {
            this.QuestionList = ls;
        }
        public void showObstacle()
        {
            for (int i = 0; i < 4; i++)
            {
                int numCircle = QuestionList[i].Answer.Length;
                int sumWidth = circleWidth * numCircle;
                float remain = (1920 - sumWidth) / 2;
                Canvas cv = (Canvas) this.canvas.Children[i];
                for (int j = 0; j < numCircle; j++)
                {
                    Shape s = new Ellipse();
                    s.Height = 124;
                    s.Width = circleWidth;
                    s.Stroke = Brushes.Black;
                    s.Fill = Brushes.LightCyan;
                    s.Margin= new Thickness {Left = remain + circleWidth*j};
                    cv.Children.Add(s);
                }

                
            }
        }
        public void ShowAnswer(String answer,int idx)
        {
            Canvas cv = (Canvas)this.canvas.Children[idx];
            float remain = (1920 - circleWidth*answer.Length)/2;
            for (int i = 0; i < answer.Length; i++)
            {
                TextBlock txt = new TextBlock();
                txt.Text = answer[i].ToString();
                txt.Foreground = Brushes.White;
                txt.FontSize = 50;
                txt.Margin = new Thickness { Left = remain + circleWidth/2+i*circleWidth,Top=30};
                cv.Children.Add(txt);
            }
        }
        private void QuestionVideoEnded_Handler(object sender, RoutedEventArgs e)
        {
            this.txtQuestion.Visibility = Visibility.Visible;
        }
        private void MediaEnded_Handler(object sender, RoutedEventArgs e)
        {
            MediaElement video = sender as MediaElement;
            video.Visibility = Visibility.Hidden;
        }

        
    }
   
}
