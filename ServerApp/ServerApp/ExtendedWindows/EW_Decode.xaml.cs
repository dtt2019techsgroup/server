﻿using ServerApp.DAO;
using ServerApp.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for EW_Decode.xaml
    /// </summary>
    public partial class EW_Decode
    {
        public List<DecodeQuestion> decodeQuestions = DecodeQuestionDAO.Instance.getQuestions();
        public System.Windows.Controls.Label[,] txt = new System.Windows.Controls.Label[9, 8];
        public int[,] value = new int[9, 8];
        public string videoPath;
        public bool isKeyQuestion = false;

        public EW_Decode()
        {
            InitializeComponent();

            videoPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            IntroVideo.Source = new Uri(videoPath + @"\Resources\3_Video1.mp4", UriKind.Relative);
            QuestionVideo.Source = new Uri(videoPath + @"\Resources\3_Video2.mp4", UriKind.Relative);
            CountDownVideo.Source = new Uri(videoPath + @"\Resources\3_Video3_15s.mp4", UriKind.Relative);
            AnswerVideo.Source = new Uri(videoPath + @"\Resources\2_Video4.mp4", UriKind.Relative);

            MainMatrix.Visibility = Visibility.Hidden;

            #region Thiết lập thuộc tính cho ma trận
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    value[i, j] = 0;

                    txt[i, j] = new System.Windows.Controls.Label();
                    txt[i, j].Background = Brushes.LightCyan;
                    txt[i, j].VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                    txt[i, j].VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
                    txt[i, j].HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                    txt[i, j].HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
                    txt[i, j].BorderThickness = new Thickness(3, 3, 3, 3);
                    txt[i, j].BorderBrush = Brushes.Navy;
                    txt[i, j].FontSize = 35;
                    txt[i, j].FontWeight = FontWeights.Bold;
                    Grid.SetRow(txt[i, j], i);
                    Grid.SetColumn(txt[i, j], j);
                }
            }
            #endregion

            #region Set thuộc tính ô gợi ý và câu hỏi (x)
            foreach (DecodeQuestion n in decodeQuestions)
            {
                if (n.CellType == 10 || n.CellType == 11)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 30, 160, 117));
                    //if (n.CellType == 11)
                    //{
                    //    txt[n.RowNo - 1, n.ColNo - 1].Content = "x";
                    //    value[n.RowNo - 1, n.ColNo - 1] = 1;
                    //}
                }
                else if (n.CellType == 20 || n.CellType == 21)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 230, 250, 10));
                    //if (n.CellType == 21)
                    //{
                    //    txt[n.RowNo - 1, n.ColNo - 1].Content = "x";
                    //    value[n.RowNo - 1, n.ColNo - 1] = 1;
                    //}
                }
                else if (n.CellType == 30)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = new SolidColorBrush(Color.FromArgb(255, 197, 10, 36));
                }
            }
            #endregion

            #region Set thuộc tính ô bình thường
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (txt[i, j].Background == Brushes.LightCyan)
                    {
                        txt[i, j].Content = numberOfDots(value, i, j).ToString();
                        txt[i, j].Foreground = Brushes.Navy;
                        txt[i, j].IsEnabled = false;
                    }
                }
            }
            #endregion

            #region Thêm danh sách button vào grid chính
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    MatrixContent.Children.Add(txt[i, j]);
                }
            }
            #endregion
        }

        public int numberOfDots(int[,] a, int row_i, int col_i)
        {
            int n = 0;

            #region Chặn 4 góc
            if (row_i == 0 && col_i == 0)
            {
                if (a[0, 1] == 1)
                    n++;
                if (a[1, 0] == 1)
                    n++;
                if (a[1, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 0 && col_i == 7)
            {
                if (a[0, 6] == 1)
                    n++;
                if (a[1, 7] == 1)
                    n++;
                if (a[1, 6] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8 && col_i == 0)
            {
                if (a[7, 0] == 1)
                    n++;
                if (a[8, 1] == 1)
                    n++;
                if (a[7, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8 && col_i == 7)
            {
                if (a[7, 6] == 1)
                    n++;
                if (a[7, 7] == 1)
                    n++;
                if (a[8, 6] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Chặn 4 cạnh
            else if (row_i == 0)
            {
                if (a[0, col_i - 1] == 1)
                    n++;
                if (a[0, col_i + 1] == 1)
                    n++;
                if (a[1, col_i - 1] == 1)
                    n++;
                if (a[1, col_i] == 1)
                    n++;
                if (a[1, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 8)
            {
                if (a[8, col_i - 1] == 1)
                    n++;
                if (a[8, col_i + 1] == 1)
                    n++;
                if (a[7, col_i - 1] == 1)
                    n++;
                if (a[7, col_i] == 1)
                    n++;
                if (a[7, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 0)
            {
                if (a[row_i - 1, 0] == 1)
                    n++;
                if (a[row_i + 1, 0] == 1)
                    n++;
                if (a[row_i - 1, 1] == 1)
                    n++;
                if (a[row_i, 1] == 1)
                    n++;
                if (a[row_i + 1, 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 7)
            {
                if (a[row_i - 1, 7] == 1)
                    n++;
                if (a[row_i + 1, 7] == 1)
                    n++;
                if (a[row_i - 1, 6] == 1)
                    n++;
                if (a[row_i, 6] == 1)
                    n++;
                if (a[row_i + 1, 6] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Xét 8 hướng
            else
            {
                if (a[row_i - 1, col_i - 1] == 1)
                    n++;
                if (a[row_i - 1, col_i] == 1)
                    n++;
                if (a[row_i - 1, col_i + 1] == 1)
                    n++;
                if (a[row_i, col_i - 1] == 1)
                    n++;
                if (a[row_i, col_i + 1] == 1)
                    n++;
                if (a[row_i + 1, col_i - 1] == 1)
                    n++;
                if (a[row_i + 1, col_i] == 1)
                    n++;
                if (a[row_i + 1, col_i + 1] == 1)
                    n++;
                return n;
            }
            #endregion
        }

        private void IntroVideo_MediaEnded(object sender, RoutedEventArgs e)
        {

        }

        private void QuestionVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            QuestionVideo.Stop();
            QuestionVideo.Visibility = Visibility.Hidden;
            QuestionBackground.Visibility = Visibility.Visible;
            if (!isKeyQuestion)
                QuestionContent.Visibility = Visibility.Visible;
        }

        private void CountDownVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            CountDownVideo.Stop();
            CountDownVideo.Visibility = Visibility.Hidden;
        }

        
        private void AnswerVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            ShowAnswer.Visibility = Visibility.Visible;
            AnswerVideo.Stop();
            AnswerVideo.Visibility = Visibility.Hidden;
        }
    }
}
