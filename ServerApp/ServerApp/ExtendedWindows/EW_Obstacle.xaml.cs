﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for EW_Obstacle.xaml
    /// </summary>
    public partial class EW_Obstacle
    {
        public bool first_time = true;
        public bool question1_show = false;
        public bool question2_show = false;
        public bool question3_show = false;
        public bool question4_show = false;

        public EW_Obstacle()
        {
            InitializeComponent();           

            TimeVideo.Visibility = Visibility.Hidden;
            QuestionBackground.Visibility = Visibility.Hidden;
            QuestionText.Visibility = Visibility.Hidden;

            string videoPath = Directory.GetCurrentDirectory();

            IntroVideo.Source = new Uri(videoPath + @"\Resources\2_Video1.mp4", UriKind.Relative);
            TimeVideo.Source = new Uri(videoPath + @"\Resources\timevcnv.mp4", UriKind.Relative);
            QuestionVideo.Source = new Uri(videoPath + @"\Resources\2_Video2.mp4", UriKind.Relative);
            AnswerVideo.Source = new Uri(videoPath + @"\Resources\2_Video4.mp4", UriKind.Relative);
            Ans.Source = new BitmapImage(new Uri(videoPath + @"\Resources\Answer.jpg"));
        }

        private void IntroVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            ContentText.Visibility = Visibility.Visible;
            IntroVideo.Visibility = Visibility.Hidden;
        }

        private void TimeVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeVideo.Visibility = Visibility.Hidden;
        }

        private void QuestionVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            n1.Visibility = Visibility.Visible;
            n2.Visibility = Visibility.Visible;
            n3.Visibility = Visibility.Visible;
            n4.Visibility = Visibility.Visible;

            QuestionVideo.Stop();
            TimeVideo.Visibility = Visibility.Visible;
            QuestionVideo.Visibility = Visibility.Hidden;
            QuestionBackground.Visibility = Visibility.Visible;
            QuestionText.Visibility = Visibility.Visible;            
        }

        private void AnswerVideo_MediaEnded(object sender, RoutedEventArgs e)
        {          

            AnswerVideo.Visibility = Visibility.Hidden;
            Upper_part.Visibility = Visibility.Hidden;
            Lower_part.Visibility = Visibility.Hidden;
            
            ShowAns.Visibility = Visibility.Visible;
        }
    }
}
