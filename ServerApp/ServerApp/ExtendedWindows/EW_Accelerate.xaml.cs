﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for EW_Accelerate.xaml
    /// </summary>
    public partial class EW_Accelerate
    {
        
        public EW_Accelerate()
        {
            InitializeComponent();            
            string videoPath = Directory.GetCurrentDirectory();
            IntroVideo.Source = new Uri(videoPath + @"\Resources\3_Video1.mp4", UriKind.Relative);
            IntroQuestion.Source = new Uri(videoPath + @"\Resources\3_Video2.mp4", UriKind.Relative);
            TimeVideo.Source = new Uri(videoPath + @"\Resources\3_Video3.mp4", UriKind.Relative);
            IntroAnswer.Source = new Uri(videoPath + @"\Resources\3_Video4.mp4", UriKind.Relative);
        }
        private void IntroVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            IntroVideo.Visibility = Visibility.Hidden;
            ContentText.Visibility = Visibility.Visible;
            IntroVideo.Stop();
        }

        private void IntroQuestion_MediaEnded(object sender, RoutedEventArgs e)
        {
            QuestionBackground.Visibility = Visibility.Visible;
            Question.Visibility = Visibility.Visible;
            QuestionVideo.Visibility = Visibility.Visible;
            QuestionImage.Visibility = Visibility.Visible;
            IntroQuestion.Visibility = Visibility.Hidden;
            IntroQuestion.Stop();
        }

        private void TimeVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeVideo.Visibility = Visibility.Hidden;
            TimeVideo.Stop();
        }

        private void QuestionVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            QuestionVideo.Visibility = Visibility.Hidden;
            QuestionVideo.Stop();
        }

        private void IntroAnswer_MediaEnded(object sender, RoutedEventArgs e)
        {
            ShowAns.Visibility = Visibility.Visible;
            IntroAnswer.Visibility = Visibility.Hidden;
            IntroAnswer.Stop();
        }
    }
}
