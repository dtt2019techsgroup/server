﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for EW_Start.xaml
    /// </summary>
    public partial class EW_Start
    {
        public EW_Start()
        {
            InitializeComponent();
            ContentText.Visibility = Visibility.Hidden;
            StudentVideo.Visibility = Visibility.Hidden;
            TimeVideo.Visibility = Visibility.Hidden;
            QuestionBackground.Visibility = Visibility.Hidden;
            QuestionText.Visibility = Visibility.Hidden;

            string videoPath = Directory.GetCurrentDirectory();

            IntroVideo.Source = new Uri(videoPath + @"\Resources\1_Video1.mp4", UriKind.Relative);
            StudentVideo.Source = new Uri(videoPath + @"\Resources\1_Video2.mp4", UriKind.Relative);
            TimeVideo.Source = new Uri(videoPath + @"\Resources\1_Video3.mp4", UriKind.Relative);
        }

        private void IntroVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            IntroVideo.Visibility = Visibility.Hidden;
            ContentText.Visibility = Visibility.Visible;
        }

        private void StudentVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            StudentVideo.Stop();
            StudentVideo.Visibility = Visibility.Hidden;
            QuestionBackground.Visibility = Visibility.Visible;
            QuestionText.Visibility = Visibility.Visible;
            PointText.Visibility = Visibility.Visible;
            PointText.Text = "0";

            StudentName1.Visibility = Visibility.Visible;
            StudentName2.Visibility = Visibility.Visible;
            StudentName3.Visibility = Visibility.Visible;
            StudentName4.Visibility = Visibility.Visible;

        }

        private void TimeVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeVideo.Visibility = Visibility.Hidden;
        }
    }
}
