﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for EW_Start.xaml
    /// </summary>
    public partial class EW_Finish
    {
        public int currentSet = 0;
        public EW_Finish()
        {
            InitializeComponent();
            ContentText.Visibility = Visibility.Hidden;
            StudentVideo.Visibility = Visibility.Hidden;
            TimeVideo.Visibility = Visibility.Hidden;
            QuestionBackground40.Visibility = Visibility.Hidden;
            QuestionBackground60.Visibility = Visibility.Hidden;
            QuestionBackground80.Visibility = Visibility.Hidden;
            QuestionText.Visibility = Visibility.Hidden;

            string videoPath = Directory.GetCurrentDirectory();

            IntroVideo.Source = new Uri(videoPath + @"\Resources\4_Video1.mp4", UriKind.Relative);
            StudentVideo.Source = new Uri(videoPath + @"\Resources\4_Video2.mp4", UriKind.Relative);
            TimeVideo.Source = new Uri(videoPath + @"\Resources\4_Video6.mp4", UriKind.Relative);
            QuestionSelectionVideo.Source = new Uri(videoPath + @"\Resources\4_Video3.mp4", UriKind.Relative);
        }

        private void IntroVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            IntroVideo.Visibility = Visibility.Hidden;
            ContentText.Visibility = Visibility.Visible;
        }

        private void StudentVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            QuestionSelectionBackground.Visibility = Visibility.Visible;
        }

        private void TimeVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            TimeVideo.Stop();
            TimeVideo.Visibility = Visibility.Hidden;
        }

        private void QuestionSelectionVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            StudentVideo.Stop();
            StudentVideo.Visibility = Visibility.Hidden;
            switch(currentSet)
            {
                case 40:
                    QuestionBackground40.Visibility = Visibility.Visible;
                    QuestionBackground60.Visibility = Visibility.Hidden;
                    QuestionBackground80.Visibility = Visibility.Hidden;
                    break;
                case 60:
                    QuestionBackground40.Visibility = Visibility.Hidden;
                    QuestionBackground60.Visibility = Visibility.Visible;
                    QuestionBackground80.Visibility = Visibility.Hidden;
                    break;
                case 80:
                    QuestionBackground40.Visibility = Visibility.Hidden;
                    QuestionBackground60.Visibility = Visibility.Hidden;
                    QuestionBackground80.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }
            QuestionText.Visibility = Visibility.Visible;
            PointText.Visibility = Visibility.Visible;
            PointText.Text = "0";

            StudentName1.Visibility = Visibility.Visible;
            StudentName2.Visibility = Visibility.Visible;
            StudentName3.Visibility = Visibility.Visible;
            StudentName4.Visibility = Visibility.Visible;
        }
    }
}
