﻿using ServerApp.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerApp.ExtendedWindows
{
    /// <summary>
    /// Interaction logic for Summary.xaml
    /// </summary>
    public partial class EW_Summary : UserControl
    {
        public List<Student> students = new List<Student>();
        public EW_Summary(List<Student> s)
        {
            this.students = s;
            InitializeComponent();

            string soundPath = Directory.GetCurrentDirectory();
            SummarySound.Source = new Uri(soundPath + @"\Resources\TongKetDiem.mp3", UriKind.RelativeOrAbsolute);

            Name1.Text = students[0].Name.ToUpper();
            Name2.Text = students[1].Name.ToUpper();
            Name3.Text = students[2].Name.ToUpper();
            Name4.Text = students[3].Name.ToUpper();

            Point1.Text = students[0].Point.ToString();
            Point2.Text = students[1].Point.ToString();
            Point3.Text = students[2].Point.ToString();
            Point4.Text = students[3].Point.ToString();
        }

        private void SummarySound_MediaEnded(object sender, RoutedEventArgs e)
        {
            SummarySound.Stop();
        }

        public void UpdateInfo()
        {
            Name1.Text = students[0].Name.ToUpper();
            Name2.Text = students[1].Name.ToUpper();
            Name3.Text = students[2].Name.ToUpper();
            Name4.Text = students[3].Name.ToUpper();

            Point1.Text = students[0].Point.ToString();
            Point2.Text = students[1].Point.ToString();
            Point3.Text = students[2].Point.ToString();
            Point4.Text = students[3].Point.ToString();
        }
    }
}
