﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
    public class DecodeQuestionDAO
    {
        private static DecodeQuestionDAO instance;
        public static DecodeQuestionDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new DecodeQuestionDAO();
                return instance;
            }
        }
        public DecodeQuestionDAO() { }

        public List<DecodeQuestion> getQuestions()
        {
            string query = string.Format(@"SELECT * FROM DECODEQUESTION");
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<DecodeQuestion> result = new List<DecodeQuestion>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new DecodeQuestion(row));
            }
            return result;
        }

        public DecodeQuestion getQuestion(int row, int col)
        {
            string query = string.Format(@"
                           SELECT * FROM DECODEQUESTION
                           WHERE RowNo = {0} AND ColNo = {1}", row, col);
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            DecodeQuestion result = new DecodeQuestion(data.Rows[0]);
            return result;
        }
    }
}
