﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
	class QuestionDAO
	{
		private static QuestionDAO instance;
		public static QuestionDAO Instance
		{
			get
			{
				if (instance == null)
					instance = new QuestionDAO();
				return instance;
			}
		}
        public QuestionDAO()
        {
            //
        }

        public List<Question> getObstacleQuestion()
        {
            string query = string.Format(@"SELECT * 
                FROM QUESTION q JOIN RECORD r ON q.QuestionTypeID = r.QuestionID 
                WHERE q.QuestionTypeID = 2 OR q.QuestionTypeID = 3");
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<Question> result = new List<Question>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new Question(row));
            }
            return result;
        }

        public List<Question> getAccelerateQuestion()
        {
            string query = string.Format(@"SELECT*
                FROM QUESTION q JOIN RECORD r ON q.QuestionTypeID = r.QuestionID
                WHERE q.QuestionTypeID = 41 OR q.QuestionTypeID = 42");
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<Question> result = new List<Question>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new Question(row));
            }
            return result;
        }


		public List<Question> getStartQuestion(int contestant)
		{
			string query = string.Format(
                @"SELECT * FROM QUESTION q
                JOIN RECORD r ON q.QuestionID = r.QuestionID
                WHERE q.QuestionTypeID = 1 AND r.Contestant = 'TS" + contestant + @"'");
			DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<Question> result = new List<Question>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new Question(row));
            }
            return result;
        }

        public List<Question> getFinishQuestions(int contestant, int set)
        {
            string query = string.Empty;
            switch (set)
            {
                case 40:
                    query = string.Format(
                                    @"SELECT * FROM QUESTION q
                                    JOIN RECORD r ON q.QuestionID = r.QuestionID
                                    WHERE (q.QuestionTypeID = 5 OR q.QuestionTypeID = 6) AND r.Contestant = 'TS{0}'", contestant);
                    break;
                case 60:
                    query = string.Format(
                                    @"SELECT * FROM QUESTION q
                                    JOIN RECORD r ON q.QuestionID = r.QuestionID
                                    WHERE (q.QuestionTypeID = 15 OR q.QuestionTypeID = 16 OR q.QuestionTypeID = 17) AND r.Contestant = 'TS{0}'", contestant);
                    break;
                case 80:
                    query = string.Format(
                                    @"SELECT * FROM QUESTION q
                                    JOIN RECORD r ON q.QuestionID = r.QuestionID
                                    WHERE (q.QuestionTypeID = 26 OR q.QuestionTypeID = 27) AND r.Contestant = 'TS{0}'", contestant);
                    break;
                default:
                    break;
            }

            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<Question> result = new List<Question>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new Question(row));
            }
            return result;
        }

        public bool addQuestion(Int32 questionID, string detail, string answer, Int32 questionTypeID, string note)
		{
			string query = string.Format(
				"INSERT INTO QUESTION " +
				"VALUES ({0}, N'{1}', N'{2}', {3}, N'{4}')",
				questionID,
				detail,
				answer,
				questionTypeID,
				note);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool updateQuestion(Int32 questionID, string detail, string answer, Int32 questionTypeID, string note)
		{
			string query = string.Format(
				"UPDATE QUESTION " +
				"SET " +
				"Detail = N'{1}', " +
				"Answer = N'{2}', " +
				"QuestionTypeID = {3}, " +
				"Note = N'{4}' " +
				"WHERE QuestionID = {0}",
				questionID, detail, answer, questionTypeID, note);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool deleteQuestion(Int32 questionID)
		{
			string query = string.Format(
				"DELETE FROM QUESTION " +
				"WHERE QuestionID = {0}", questionID);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}
	}
}
