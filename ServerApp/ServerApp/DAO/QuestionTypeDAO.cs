﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
	class QuestionTypeDAO
	{
		private static QuestionTypeDAO instance;
		public static QuestionTypeDAO Instance
		{
			get
			{
				if (instance == null)
					instance = new QuestionTypeDAO();
				return instance;
			}
		}
		public QuestionTypeDAO() { }

		public QuestionType getQuestionType(Int32 questionTypeID)
		{
			string query = string.Format(
				"SELECT * FROM QUESTIONTYPE " +
				"WHERE QuestionTypeID = {0}", questionTypeID);
			DataTable data = DataProvider.Instance.ExecuteQuery(query);
			QuestionType result = new QuestionType(data.Rows[0]);
			return result;
		}

		public bool addQuestionType(Int32 questionTypeID, Byte round, Int32 points, string description)
		{
			string query = string.Format(
				"INSERT INTO QUESTIONTYPE " +
				"VALUES ({0}, {1}, {2}, N'{3}')",
				questionTypeID,
				round,
				points,
				questionTypeID,
				description);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool updateQuestionType(Int32 questionTypeID, Byte round, Int32 points, string description)
		{
			string query = string.Format(
				"UPDATE QUESTIONTYPE " +
				"SET " +
				"Round = {1}, " +
				"Points = {2}, " +
				"Description = N'{3}', " +
				"WHERE QuestionTypeID = {0}",
				questionTypeID, 
				round, 
				points, 
				description);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool deleteQuestionType(Int32 questionTypeID)
		{
			string query = string.Format(
				"DELETE FROM QUESTIONTYPE " +
				"WHERE QuestionTypeID = {0}", questionTypeID);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}
	}
}
