﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
	class AccountDAO
	{
		private static AccountDAO instance;
		public static AccountDAO Instance
		{
			get
			{
				if (instance == null)
					instance = new AccountDAO();
				return instance;
			}
		}
		public AccountDAO() { }

		public bool login(string username, string password)
		{
			string md5String = "";
			byte[] passwordArray = System.Text.Encoding.Unicode.GetBytes(password);
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			passwordArray = md5.ComputeHash(passwordArray);

			foreach (byte b in passwordArray)
			{
				md5String += b.ToString("X2");
			}

			string query = "EXECUTE uspLogin @Username , @Password";
			DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { username, md5String });
			return result.Rows.Count > 0;
		}

		public Account getAccount(string username)
		{
			string query = string.Format(
				"SELECT * FROM ACCOUNT " +
				"WHERE Username = '{0}'", username);
			DataTable data = DataProvider.Instance.ExecuteQuery(query);
			Account result = new Account(data.Rows[0]);
			return result;
		}

		public bool addAccount(
			string username,
			string password)
		{
			string md5String = "";
			byte[] passwordArray = System.Text.Encoding.Unicode.GetBytes(password);
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			passwordArray = md5.ComputeHash(passwordArray);

			foreach (byte b in passwordArray)
			{
				md5String += b.ToString("X2");
			}

			string query = string.Format(
				"INSERT INTO ACCOUNT " +
				"VALUES ('{0}', N'{1}')",
				username,
				md5String);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool updateAccount(
			string username,
			string password)
		{
			string md5String = "";
			byte[] passwordArray = System.Text.Encoding.Unicode.GetBytes(password);
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			passwordArray = md5.ComputeHash(passwordArray);

			foreach (byte b in passwordArray)
			{
				md5String += b.ToString("X2");
			}

			string query = string.Format(
				"UPDATE ACCOUNT " +
				"SET Password = N'{1}', " +
				"WHERE Username = '{0}'",
				username,
				md5String);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool deleteAccount(string username)
		{
			string query = string.Format(
				"DELETE FROM ACCOUNT " +
				"WHERE Username = '{0}'", username);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}
	}
}
