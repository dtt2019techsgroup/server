﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
	class RecordDAO
	{
		private static RecordDAO instance;
		public static RecordDAO Instance
		{
			get
			{
				if (instance == null)
					instance = new RecordDAO();
				return instance;
			}
		}
		public RecordDAO() { }

		public Record getRecord(Int32 recordID)
		{
			string query = string.Format(
				"SELECT * FROM RECORD " +
				"WHERE RecordID = {0}", recordID);
			DataTable data = DataProvider.Instance.ExecuteQuery(query);
			Record result = new Record(data.Rows[0]);
			return result;
		}

		public bool addRecord(Int32 recordID, Int32 contestantID, Int32 questionID, Byte result)
		{
			string query = string.Format(
				"INSERT INTO RECORD " +
				"VALUES ({0}, {1}, {2}, {3})",
				recordID,
				contestantID,
				questionID,
				result);
			int _result = DataProvider.Instance.ExecuteNonQuery(query);
			return _result > 0;
		}

		public bool updateRecord(Int32 recordID, Int32 contestantID, Int32 questionID, Byte result)
		{
			string query = string.Format(
				"UPDATE RECORD " +
				"SET " +
				"ContestantID = {1}, " +
				"QuestionID = {2}, " +
				"Result = {3}, " +
				"WHERE RecordID = {0}",
				recordID,
				contestantID,
				questionID,
				result);
			int _result = DataProvider.Instance.ExecuteNonQuery(query);
			return _result > 0;
		}

		public bool deleteRecord(Int32 recordID)
		{
			string query = string.Format(
				"DELETE FROM RECORD " +
				"WHERE RecordID = {0}", recordID);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}
	}
}
