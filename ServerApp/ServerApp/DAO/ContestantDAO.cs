﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Data;
//using ServerApp.Utilities;
//using ServerApp.DTO;

//namespace ServerApp.DAO
//{
//	class ContestantDAO
//	{
//		private static ContestantDAO instance;
//		public static ContestantDAO Instance
//		{
//			get
//			{
//				if (instance == null)
//					instance = new ContestantDAO();
//				return instance;
//			}
//		}
//		public ContestantDAO() { }
		
//		public List<Contestant> getAllContestants()
//		{
//			string query = string.Format("SELECT * FROM CONTESTANT");
//			DataTable data = DataProvider.Instance.ExecuteQuery(query);
//			List<Contestant> result = new List<Contestant>();
//			foreach (DataRow row in data.Rows)
//			{
//				result.Add(new Contestant(row));
//			}
//			return result;
//		}

//		public Contestant getContestant(Int32 contestantID)
//		{
//			string query = string.Format(
//				"SELECT * FROM CONTESTANT " +
//				"WHERE ContestantID = {0}", contestantID);
//			DataTable data = DataProvider.Instance.ExecuteQuery(query);
//			Contestant result = new Contestant(data.Rows[0]);
//			return result;
//		}

//		public bool addContestant(Int32 contestantID, string name)
//		{
//			string query = string.Format(
//				"INSERT INTO CONTESTANT " +
//				"VALUES ({0}, N'{1}')", contestantID, name);
//			int result = DataProvider.Instance.ExecuteNonQuery(query);
//			return result > 0;
//		}

//		public bool updateContestant(Int32 contestantID, string name)
//		{
//			string query = string.Format(
//				"UPDATE CONTESTANT " +
//				"SET " +
//				"Name = N'{1}'" +
//				"WHERE ContestantID = {0}, ",
//				contestantID, name);
//			int result = DataProvider.Instance.ExecuteNonQuery(query);
//			return result > 0;
//		}

//		public bool deleteContestant(Int32 contestantID)
//		{
//			string query = string.Format(
//				"DELETE FROM CONTESTANT " +
//				"WHERE ContestantID = {0}", contestantID);
//			int result = DataProvider.Instance.ExecuteNonQuery(query);
//			return result > 0;
//		}
//	}
//}
