﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ServerApp.Utilities;
using ServerApp.DTO;

namespace ServerApp.DAO
{
	class RoundDAO
	{
		private static RoundDAO instance;
		public static RoundDAO Instance
		{
			get
			{
				if (instance == null)
					instance = new RoundDAO();
				return instance;
			}
		}
		public RoundDAO() { }

		public Round getRound(Byte roundID)
		{
			string query = string.Format(
				"SELECT * FROM ROUND " +
				"WHERE RoundID = {0}", roundID);
			DataTable data = DataProvider.Instance.ExecuteQuery(query);
			Round result = new Round(data.Rows[0]);
			return result;
		}

		public bool addRound(Byte roundID, string name)
		{
			string query = string.Format(
				"INSERT INTO ROUND " +
				"VALUES ({0}, {1})",
				roundID,
				name);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}

		public bool updateRound(Byte roundID, string name)
		{
			string query = string.Format(
				"UPDATE ROUND " +
				"SET " +
				"Name = {1}, " +
				"WHERE RoundID = {0}",
				roundID,
				name);
			int _result = DataProvider.Instance.ExecuteNonQuery(query);
			return _result > 0;
		}

		public bool deleteRound(Int32 roundID)
		{
			string query = string.Format(
				"DELETE FROM ROUND " +
				"WHERE RoundID = {0}", roundID);
			int result = DataProvider.Instance.ExecuteNonQuery(query);
			return result > 0;
		}
	}
}
