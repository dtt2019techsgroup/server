﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.DTO
{
	public class Account
	{
		public string Username { get; set; }
		public string Password { get; set; }

		public Account() { }

		public Account(string username, string password)
		{
			this.Username = username;
			this.Password = password;
		}

		public Account(DataRow row)
		{
			this.Username = row["Username"].ToString();
			this.Password = row["Password"].ToString();
		}
	}
}
