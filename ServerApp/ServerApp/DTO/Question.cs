﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.DTO
{
	public class Question
	{
		public string QuestionID { get; set; }
		public string Detail { get; set; }
		public string Answer { get; set; }
		public Int32 QuestionTypeID { get; set; }
		public string Note { get; set; }


		public Question() { }

		public Question(string questionID, string detail, string answer, Int32 questionTypeID, string note)
		{
			this.QuestionID = questionID;
			this.Detail = detail;
			this.Answer = answer;
			this.QuestionTypeID = questionTypeID;
			this.Note = note;
		}

		public Question(DataRow row)
		{
			this.QuestionID = row["QuestionID"].ToString();
			this.Detail = row["Detail"].ToString();
			this.Answer = row["Answer"].ToString();
			this.QuestionTypeID = (Int32) row["QuestionTypeID"];
			this.Note = row["Note"].ToString();
		}
	}
}
