﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

/*Mã cho QuestiontypeID:
    Khởi động: 1    Backup question: 100
    VCNV: 
        Câu hỏi hàng ngang: 2 
        Câu hỏi từ khóa: 3
    Tăng tốc:      Backup question: 400
        Câu hỏi hình ảnh: 41
        Câu hỏi video: 42
    Về đích:        Backup question: 500 
        10đ: 5 cho gói 40, 15 cho gói 60
        20đ: 6 cho gói 40, 16 cho gói 60, 26 cho gói 80
        30đ: 17 cho gói 60, 27 cho gói 80 
    Phần thi mới:    Backup question: 800
        Dễ: 8
        Trung bình: 9
        Khó: 10
        Gợi ý: 11*/

namespace ServerApp.DTO
{
    public class QuestionType
    {
        public Int32 QuestionTypeID { get; set; }
        public Byte RoundID { get; set; }
        public Int32 Points { get; set; }
        public string Description { get; set; }
        public QuestionType() { }
        public QuestionType(Int32 questionTypeID, Byte roundID, Int32 points, string description)
        {
            this.QuestionTypeID = questionTypeID;
            this.RoundID = roundID;
            this.Points = points;
            this.Description = description;
        }
		public QuestionType(DataRow row)
		{
			this.QuestionTypeID = (Int32)row["QuestionTypeID"];
			this.RoundID = (Byte)row["RoundID"];
			this.Points = (Int32)row["Points"];
			this.Description = row["Description"].ToString();
		}
	}
}
