﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ServerApp.DTO
{
	public class Record
	{
		public Int32 RecordID { get; set; }
		public Int32 QuestionID { get; set; }
        public String Contestant { get; set; }
        public Byte Result { get; set; }
		public Record() { }
		public Record(Int32 recordID, Int32 questionID, String contestant, Byte result)
		{
			this.RecordID = recordID;
			this.QuestionID = questionID;
            this.Contestant = contestant;
            this.Result = result;
		}
		public Record(DataRow row)
		{
			this.RecordID = (Int32)row["RecordID"];
			this.QuestionID = (Int32)row["QuestionID"];
            this.Contestant = row["ContestantID"].ToString();
            this.Result = (Byte)row["Result"];
		}
	}
}
