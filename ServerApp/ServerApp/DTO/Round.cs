﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ServerApp.DTO
{
	public class Round
	{
		public Byte RoundID { get; set; }
		public string Name { get; set; }
		public Round() { }
		public Round(Byte roundID, string name)
		{
			this.RoundID = roundID;
			this.Name = name;
		}
		public Round(DataRow row)
		{
			this.RoundID = (Byte)row["RoundID"];
			this.Name = row["Name"].ToString();
		}
	}
}
