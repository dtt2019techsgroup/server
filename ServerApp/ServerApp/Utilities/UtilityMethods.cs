﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ServerApp.DTO;
//using StreamInfo;

//namespace ServerApp.Utilities
//{
//    class UtilityMethods
//    {
//        //Method for changing from Question to QA 
//        public QA fromQuestionToQA(Question question)
//        {
//            QA qa = new QA();
//            qa.QuestionID = question.QuestionID;
//            qa.QuestionTypeID = question.QuestionTypeID;
//            qa.Detail = question.Detail;
//            qa.Answer = question.Answer;
//            qa.Note = question.Note;
//            return qa;
//        }

//        public List<QA> ListQuestoListQA(List<Question> question)
//        {
//            List<QA> qAs = new List<QA>();
//            foreach (Question _question in question)
//                qAs.Add(fromQuestionToQA(_question));
//            return qAs;
//        }

//        //Connection 
//        /* _dataTypeSent :
//         *      0: Initiate signal; 
//         *      1: Question; 10: Start signal 
//         *      2: Answer
//         *      3: Point
//         *      4: End of part
//         *      
//         * _round: 
//         *      1: KD
//         *      2: VCNV
//         *      3: TT
//         *      4: New round 
//         *      5: VD
//         * _positionSent values: 1,2,3,4 
//         */

//        public Master setDataSent_Code00(Master master, int round)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 0;
//            streamingInfo.Round = round;
//            master.setStreamingInfo(streamingInfo);
//            return master;
//        }

//        public Master setDataSent_Code10(Master master, int round, int currentStudentID)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 10;
//            streamingInfo.Round = round;
//            streamingInfo.PositionSent = currentStudentID;
//            master.setStreamingInfo(streamingInfo);
//            return master;
//        }

//        public Master setDataSent_Code1(Master master, int round, QA qa, int currentStudentID)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 1;
//            streamingInfo.Round = round;
//            streamingInfo.PositionSent = currentStudentID;
//            master.setStreamingInfo(streamingInfo);
//            master.setQA(qa);
//            return master;
//        }

//        public Master setDataSent_Code2(Master master, int round)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 2;
//            streamingInfo.Round = round;
//            master.setStreamingInfo(streamingInfo);
//            return master;
//        }

//        public Master setDataSent_Code3(Master master, int round, List<Student> students)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 3;
//            streamingInfo.Round = round;
//            master.setStudents(students);
//            return master;
//        }

//        public Master setDataSent_Code4(Master master, int round)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 4;
//            streamingInfo.Round = round;
//            master.setStreamingInfo(streamingInfo);
//            return master;
//        }

//        public Master setDataSent_Code5(Master master, int round)
//        {
//            StreamingInfo streamingInfo = new StreamingInfo();
//            streamingInfo.DataTypeSent = 5;
//            streamingInfo.Round = round;
//            master.setStreamingInfo(streamingInfo);
//            return master;
//        }
//    }

    
//}
