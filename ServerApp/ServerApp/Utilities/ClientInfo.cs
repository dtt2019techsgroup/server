﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp.Utilities
{
    public class ClientInfo
    {
        public ClientInfo() { }
        public ClientInfo(string ip, string name, string status, Client tag)
        {
            this.IP = ip;
            this.Nickname = name;
            this.Status = status;
            this.Tag = tag;

            if (ip.Split(':').Count() > 0)
            {
                if (int.Parse(ip.Split(':')[1]) == 59001)
                    this.Position = 1;
                else if (int.Parse(ip.Split(':')[1]) == 59002)
                    this.Position = 2;
                else if (int.Parse(ip.Split(':')[1]) == 59003)
                    this.Position = 3;
                else if (int.Parse(ip.Split(':')[1]) == 59004)
                    this.Position = 4;

            }
        }
        public string IP { get; set; }
        public string Nickname { get; set; }
        public string Status { get; set; }
        public int Position { get; set; }
        public Client Tag { get; set; }
    }
}
