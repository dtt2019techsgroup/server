﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace StreamInfo
{
    //QA class
    [Serializable]
    public class QA : ISerializable
    {
        private QA s;

        public string QuestionID { get; set; }
        public string Detail { get; set; }
        public string Answer { get; set; }
        public Int32 QuestionTypeID { get; set; }
        public string Note { get; set; }

        public void setQID(String QuestionID)
        {
            this.QuestionID = QuestionID;
        }

        public QA() { }

        public QA(string questionID, string detail, string answer, Int32 questionTypeID, string note)
        {
            this.QuestionID = questionID;
            this.Detail = detail;
            this.Answer = answer;
            this.QuestionTypeID = questionTypeID;
            this.Note = note;
        }

        protected QA(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            Detail = (String)info.GetValue("Detail", typeof(String));
            QuestionID = (String)info.GetValue("QuestionID", typeof(String));
            QuestionTypeID = (int)info.GetValue("QuestionTypeID", typeof(int));
        }

        public QA(QA s)
        {
            this.s = s;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            info.AddValue("Detail", Detail);
            info.AddValue("QuestionID", QuestionID);
            info.AddValue("QuestionTypeID", QuestionTypeID);
        }
    }
}
