﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace StreamInfo
{
    public class Master : Send, Receive
    {
        protected static int _port;
        protected static TcpListener _tcpListener;
        protected static TcpClient _connectedTcpClient;
        protected static bool _running;
        protected static BinaryFormatter _binaryFormatter = new BinaryFormatter();
        protected static Thread _thread;
        protected static bool IsQA;
        protected static StreamingInfo _streamingInfo;
        protected static List<Student> _students;
        protected static Student[] _student;

        public void setTriggerQA(bool isQA)
        {
            IsQA = isQA;
        }

        protected static QAControls _qaControl;

        //Constructor for sending questions of the Start part
        public Master(int port, QA_Start qa_Start)
        {
            setupNetwork(port);
            _qaControl = new QA_Start(qa_Start);
            setTriggerQA(true);
        }

        //Constructor for sending questions of the Obstacle part
        public Master(int port, QA_Obstacle qa_Obstacle)
        {
            setupNetwork(port);
            _qaControl = new QA_Obstacle(qa_Obstacle);
            setTriggerQA(true);
        }

        //Constructor for sending questions of the Accelerate part
        public Master(int port, QA_Accelerate qa_Accelerate)
        {
            setupNetwork(port);
            _qaControl = new QA_Accelerate(qa_Accelerate);
            setTriggerQA(true);
        }

        //Constructor for sending questions of the Finish part
        public Master(int port, QA_Finish qa_Finish)
        {
            setupNetwork(port);
            _qaControl = new QA_Finish(qa_Finish);
            setTriggerQA(true);
        }

        //Constructor for sending Students' Information
        public Master(int port, List<Student> students)
        {
            setupNetwork(port);
            _students = new List<Student>(students);
            setTriggerQA(false);
        }

        //Constructor for sending Students' Information (for testing)
        public Master(int port, Student[] students)
        {
            setupNetwork(port);
            _student = new Student[students.Length];
            for (int i = 0; i < students.Length; i++)
                _student[i] = students[i];
            setTriggerQA(false);
        }

        public void setupNetwork(int port)
        {
            _port = port;
            _tcpListener = new TcpListener(IPAddress.Broadcast,
                _port);
            _binaryFormatter = new BinaryFormatter();
        }

        //Constructor for receiving information 
        public Master(int port)
        {
            _port = port;
            _connectedTcpClient = new TcpClient("127.0.0.1", _port); //"192.168.0.8"
            _running = false;
        }

        public void ListenForConnection()
        {
            try
            {
                while (_running)
                {
                    if (!IsQA)
                    {
                        _streamingInfo = (StreamingInfo)_binaryFormatter.Deserialize(_connectedTcpClient.GetStream());
                        /*Console.WriteLine(_streamingInfo.getPosition());
                        Console.WriteLine(_streamingInfo.getAnswer());*/
                    }
                    else
                    {
                        _qaControl = (QAControls)_binaryFormatter.Deserialize(_connectedTcpClient.GetStream());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
            }
        }

        public void ListenForMessage()
        {
            while (_running)
            {
                if (IsQA)
                {
                    _connectedTcpClient = _tcpListener.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    _binaryFormatter.Serialize(_connectedTcpClient.GetStream(), _students);
                    Console.WriteLine("send House!");
                }
                else
                {
                    _connectedTcpClient = _tcpListener.AcceptTcpClient();
                    Console.WriteLine("Connected!");
                    _binaryFormatter.Serialize(_connectedTcpClient.GetStream(), _qaControl.getQA((_qaControl.getCurQuestion())));
                }
            }
        }

        public void Start_Receive()
        {
            lock (this)
            {
                if (!_running)
                {
                    _running = true;
                    _thread = new Thread
                        (new ThreadStart(ListenForConnection));
                    _thread.Start();
                }
                else
                {
                    _running = true;
                    _thread = new Thread
                        (new ThreadStart(ListenForConnection));
                    _thread.Start();
                }
            }
        }

        public void Start_Send()
        {
            if (!_running)
            {
                _tcpListener.Start();
                Console.WriteLine("Waiting for a connection... ");
                _running = true;
                _thread = new Thread
                    (new ThreadStart(ListenForMessage));
                _thread.Start();
            }
        }

        public void Stop_Receive()
        {
            throw new NotImplementedException();
        }

        public void Stop_Send()
        {
            if (_running)
            {
                _tcpListener.Stop();
                _running = false;
            }
        }

        public void setStudents(List<Student> students)
        {
            _students = new List<Student>(students);
        }

        public StreamingInfo getStreamingInfo()
        {
            return _streamingInfo;
        }
    }
}
