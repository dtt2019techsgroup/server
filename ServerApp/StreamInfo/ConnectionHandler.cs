﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StreamInfo
{
    public interface Send
    {
        void ListenForConnection();
        void Start_Send();
        void Stop_Send();
    }

    public interface Receive
    {
        void ListenForMessage();
        void Start_Receive();
        void Stop_Receive();
    }
}
