﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace StreamInfo
{
    [Serializable]
    public class Student : ISerializable
    {
        private String _name;
        public String Name { get => _name; set { _name = value; } }
        private String _class;
        public String Class { get => _class; set { _class = value; } }
        private int _point;
        public int Point { get => _point; set { _point = value; } }
        private int _position;
        private Student student;
        private int _time;

        public int Position { get => _position; set { _position = value; } }

        public int Time { get => _time; set => _time = value; }

        //private default constructor
        public Student() { }

        public Student(String name, String @class, int point, int position)
        {
            Name = name;
            Class = @class;
            Point = point;
            Position = position;
        }

        public int AddPoint(int iPoint)
        {
            _point += iPoint;
            return _point;
        }

        protected Student(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            Name = (String)info.GetValue("Name", typeof(String));
            Class = (String)info.GetValue("Class", typeof(String));
            Point = (int)info.GetValue("Point", typeof(int));
            Position = (int)info.GetValue("Position", typeof(int));
            //Need a method to take the position of the computer from a .txt file
        }

        public Student(Student student)
        {
            this.student = student;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            info.AddValue("Name", Name);
            info.AddValue("Class", Class);
            info.AddValue("Point", Point);
            info.AddValue("Position", Position);
        }
    }
}