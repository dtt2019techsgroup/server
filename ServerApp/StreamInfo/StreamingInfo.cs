﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace StreamInfo
{
    //StreamingInfo class
    [Serializable]
    public class StreamingInfo : ISerializable
    {
        /* _dataTypeSent :
         *      0: Initiate signal
         *      1: Question 
         *      2: Answer
         *      3: End of part
         * _round: 
         *      1: KD
         *      2: VCNV
         *      3: TT
         *      4: New round 
         *      5: VD
         * _positionSent values: 1,2,3,4 
         */
        private int _dataTypeSent;
        private int _round;
        private int _positionSent;
        private float _time;
        public int DataTypeSent { get => _dataTypeSent; set => _dataTypeSent = value; }
        public int Round { get => _round; set => _round = value; }
        public int PositionSent { get => _positionSent; set => _positionSent = value; }
        public float Time { get => _time; set => _time = value; }

        private String _serv_answer;
        public string Serv_answer { get => _serv_answer; set => _serv_answer = value; }


        //Q&A
        private QA _qa;
        public QA Qa { get => _qa; set => _qa = value; }

        //Student
        private List<Student> students;
        public List<Student> Students { get => students; set => students = value; }
        

        //Constructor
        public StreamingInfo(int positionInput)
        {
            PositionSent = positionInput;
        }

        public StreamingInfo()
        {

        }

        public StreamingInfo(StreamingInfo s)
        {
            DataTypeSent = s.DataTypeSent;
            Round = s.Round;
            PositionSent = s.PositionSent;
            Time = s.Time;

            Qa = s.Qa;

            Students = s.Students;
        }

        protected StreamingInfo(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            DataTypeSent = (int)info.GetValue("DataTypeSent", typeof(int));            
            Round = (int)info.GetValue("Round", typeof(int));            
            Time = (float)info.GetValue("Time", typeof(float));
            Serv_answer = (String)info.GetValue("Serv_answer", typeof(String));
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new System.ArgumentNullException("info");
            info.AddValue("DataTypeSent", DataTypeSent);
            info.AddValue("Round", Round);
            info.AddValue("Time", Time);
        }
    }   
}
