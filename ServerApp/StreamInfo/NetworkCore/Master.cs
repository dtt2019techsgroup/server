﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;


namespace StreamInfo
{
    public class Master
    {
        protected static int _port;
        protected static IPAddress _iPAddress;
        protected static TcpListener _tcpListener;
        protected TcpClient _client;
        protected static bool _running;
        protected static BinaryFormatter _binaryFormatter = new BinaryFormatter();
        protected static Thread _thread;
        protected static int _status;

        protected static StreamingInfo _streamingInfo;
        protected static StreamingInfo[] _streamingInfoArray = new StreamingInfo[4];

        protected static string text = String.Empty;
        protected static StreamReader _reader;
        protected static StreamWriter _writer;

        private static QA qa;
        protected static List<Student> _students;

        //Constructor for receiving information 
        public Master(int port)
        {
            setupNetworkForListening(port);
        }

        public void setupNetworkForListening(int port)
        {
            _port = port;
            _tcpListener = new TcpListener(IPAddress.Loopback, _port);
            _tcpListener.Start();
            Console.WriteLine("Master started!");

            _binaryFormatter = new BinaryFormatter();
            _running = true;
        }

        /* _dataTypeSent :
         *      0: Initiate signal
         *      1: Question 
         *      2: Answer
         *      3: Point
         *      4: End of part
         *      
         */

        public void Connect()
        {
            try
            {
                while (_running)
                {
                    Console.WriteLine("Waiting for incoming servant connections...");
                    _client = _tcpListener.AcceptTcpClient();
                    Console.WriteLine("Accepted new servant connection...");

                    Thread thread = new Thread(new ParameterizedThreadStart(ProcessClientRequest));
                    thread.Start(_client);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (_tcpListener != null)
                {
                    _tcpListener.Stop();
                }
            }
        }

        private static void ProcessClientRequest(Object argument)
        {
            TcpClient _client = (TcpClient)argument;
            try
            {

                StreamReader reader = new StreamReader(_client.GetStream());
                StreamWriter writer = new StreamWriter(_client.GetStream());
                String tmp = String.Empty;

                switch (_streamingInfo.DataTypeSent)
                {
                    case 0:
                        writer.WriteLine("0");
                        writer.Flush();
                        _binaryFormatter.Serialize(_client.GetStream(), _streamingInfo);
                        reader.ReadLine();
                        _client.GetStream().Flush();
                        break;
                    case 10:
                        writer.WriteLine("10");
                        writer.Flush();
                        _binaryFormatter.Serialize(_client.GetStream(), _streamingInfo);
                        reader.ReadLine();
                        _client.GetStream().Flush();
                        break;
                    case 1:
                        writer.WriteLine("1");
                        writer.Flush();
                        _binaryFormatter.Serialize(_client.GetStream(), qa);
                        reader.ReadLine();
                        _client.GetStream().Flush();
                        break;
                    case 2:
                        writer.WriteLine("2");
                        writer.Flush();
                        _streamingInfo = new StreamingInfo((StreamingInfo)_binaryFormatter.Deserialize(_client.GetStream()));
                        _streamingInfoArray[_streamingInfo.PositionSent - 1] = _streamingInfo;
                        reader.ReadLine();
                        break;
                    case 3:
                        writer.WriteLine("3");
                        writer.Flush();
                        _binaryFormatter.Serialize(_client.GetStream(), _students);
                        reader.ReadLine();
                        _client.GetStream().Flush();
                        break;
                    case 4:
                        writer.WriteLine("4");
                        writer.Flush();
                        _binaryFormatter.Serialize(_client.GetStream(), _streamingInfo);
                        _client.GetStream().Flush();
                        break;
                    default:                        
                        _client.Close();
                        Console.WriteLine("Client connection closed!");
                        break;
                }
                writer.Close();
                reader.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void setStudents(List<Student> students)
        {
            _students = new List<Student>(students);            
        }

        public List<Student> GetStudents()
        {
            return _students;
        }

        public StreamingInfo[] getStreamingInfoArray()
        {
            return _streamingInfoArray;
        }

        public void setStreamingInfo(StreamingInfo s)
        {
            _streamingInfo = new StreamingInfo(s);
        }

        public StreamingInfo getStreamingInfo()
        {
            return _streamingInfo;
        }

        public void setQA(QA s)
        {
            qa = new QA(s);
        }

        public QA getQA()
        {
            return qa;
        }
    }       
}
 