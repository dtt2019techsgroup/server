﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace StreamInfo
{
    public class Servent 
    {
        protected static int _port;
        protected static TcpClient _tcpClient;
        protected static bool _running;
        protected static BinaryFormatter _binaryFormatter = new BinaryFormatter();
        protected static Thread _thread;
        protected static bool IsQA;
        protected static StreamingInfo _streamingInfo;
        protected static StreamingInfo _streamingInfoOutput;
        protected static string text = String.Empty;
        protected static string server_string = String.Empty;
        protected static StreamReader _reader;
        protected static StreamWriter _writer;
        protected static List<Student> _students;
        protected static Student _student;

        protected static QA _qa;


        /* _dataTypeSent :
         *      0: Initiate signal
         *      1: Question 
         *      2: Answer
         *      3: Point
         *      4: End of part 
         *      5: Disconnect
         */
        public Servent(int port, StreamingInfo streamingInfo)
        {
            _port = port;
            _tcpClient = new TcpClient("127.0.0.1", _port);
            _binaryFormatter = new BinaryFormatter();
            _students = new List<Student>();
            _streamingInfo = new StreamingInfo(streamingInfo);
            _running = true;
        }
        
        public void Connect()
        {
            try
            {
                if (_running)
                {
                    Console.WriteLine("Waiting for incoming master connections...");
                    Console.WriteLine("Accepted master connection...");

                    StreamReader reader = new StreamReader(_tcpClient.GetStream());
                    StreamWriter writer = new StreamWriter(_tcpClient.GetStream());
                    String tmp = String.Empty;

                    Console.WriteLine(tmp);


                    switch (tmp)
                    {
                        case "0":
                        case "4":
                        case "10":
                            _streamingInfo = (StreamingInfo)_binaryFormatter.Deserialize(_tcpClient.GetStream());

                            writer.WriteLine("Information received!");
                            writer.Flush();
                            break;
                        case "1":
                            _qa = (QA)_binaryFormatter.Deserialize(_tcpClient.GetStream());
                            writer.WriteLine("Information received!");
                            writer.Flush();
                            break;
                        case "2":
                            _binaryFormatter.Serialize(_tcpClient.GetStream(), _streamingInfo);
                            _tcpClient.GetStream().Flush();
                            writer.WriteLine("Information sent!");
                            writer.Flush();
                            break;
                        case "3":
                            _students = new List<Student>((List<Student>)_binaryFormatter.Deserialize(_tcpClient.GetStream()));
                            writer.WriteLine("Information received!");

                            writer.Flush();
                            Console.WriteLine("Information received!");
                            break;
                        default:
                            writer.Close();
                            reader.Close();
                            _tcpClient.Close();
                            Console.WriteLine("Server connection closed!");
                            break;
                    }
                        Console.WriteLine("Waiting for incoming master messages...");
                    

                    
                }
                    //    Thread thread = new Thread(ProcessMasterRequest);
                    //    thread.Start();
                    //}

                }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void ProcessMasterRequest(object obj)
        {
            TcpClient _client = (TcpClient)obj;
            try
            {
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void setStreamingInfo(StreamingInfo streamingInfo)
        {
            _streamingInfo = streamingInfo;
        }

        public StreamingInfo getStreamingInfo()
        {
            return _streamingInfoOutput;
        }

        public List<Student> getStudent()
        {
            return _students;
        }

        public void setQA(QA s)
        {
            _qa = new QA(s);
        }

        public QA getQA()
        {
            return _qa;
        }
    }
}

