﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace StreamInfo
{
    public class Servent : Send, Receive
    {
        protected static int _port;
        protected static TcpListener _tcpListener;
        protected static TcpClient _tcpClient;
        protected static bool _running;
        protected static BinaryFormatter _binaryFormatter = new BinaryFormatter();
        protected static Thread _thread;
        protected static bool IsQA;
        protected static StreamingInfo _streamingInfo;
        protected static List<Student> _students;
        protected static Student[] _student;

        protected static QA _qa;
        List<Type> knownTypeList = new List<Type>(); //?

        public Servent(int port, StreamingInfo streamingInfo)
        {
            _port = port;
            _tcpListener = new TcpListener(new IPEndPoint(IPAddress.Loopback, _port));/*3232235528 192.168.0.8*/ //Looback for testing 
            _binaryFormatter = new BinaryFormatter();
            _streamingInfo = streamingInfo;
        }

        public Servent(int port)
        {
            _port = port;
            _tcpClient = new TcpClient("127.0.0.1", _port); //"255.255.255.255"
            _running = false;
        }

        public void ListenForConnection()
        {
            try
            {
                while (_running)
                {
                    if (!IsQA)
                    {
                        _students = (List<Student>)_binaryFormatter.Deserialize(_tcpClient.GetStream());
                        /*Console.WriteLine(_streamingInfo.getPosition());
                        Console.WriteLine(_streamingInfo.getAnswer());*/
                    }
                    else
                    {
                        _qa = (QA)_binaryFormatter.Deserialize(_tcpClient.GetStream());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
            }
        }

        public void ListenForMessage()
        {
            while (_running)
            {
                _tcpClient = _tcpListener.AcceptTcpClient();
                Console.WriteLine("Connected!");
                _binaryFormatter.Serialize(_tcpClient.GetStream(), _streamingInfo);
                //Console.WriteLine("send House!");
            }
        }

        public void Start_Receive()
        {
            lock (this)
            {
                if (!_running)
                {
                    _running = true;
                    _thread = new Thread
                        (new ThreadStart(ListenForConnection));
                    _thread.Start();
                }
                else
                {
                    _running = true;
                    _thread = new Thread
                        (new ThreadStart(ListenForConnection));
                    _thread.Start();
                }
            }
        }

        public void Start_Send()
        {
            if (!_running)
            {
                _tcpListener.Start();
                //Console.WriteLine("Waiting for a connection... ");
                _running = true;
                _thread = new Thread
                    (new ThreadStart(ListenForMessage));
                _thread.Start();
            }
        }

        public void Stop_Receive()
        {
            throw new NotImplementedException();
        }

        public void Stop_Send()
        {
            if (_running)
            {
                _tcpListener.Stop();
                _running = false;
            }
        }

        public void setStreamingInfo(StreamingInfo streamingInfo)
        {
            _streamingInfo = streamingInfo;
        }

        public List<Student> getStudent()
        {
            return _students;
        }

    }
}
