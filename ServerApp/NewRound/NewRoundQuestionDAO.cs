﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NewRound
{
    class NewRoundQuestionDAO
    {
        private static NewRoundQuestionDAO instance;
        public static NewRoundQuestionDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new NewRoundQuestionDAO();
                return instance;
            }
        }
        public NewRoundQuestionDAO() { }

        public List<NewRoundQuestion> getQuestions()
        {
            string query = string.Format(@"SELECT * FROM DECODEQUESTION");
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            List<NewRoundQuestion> result = new List<NewRoundQuestion>();
            foreach (DataRow row in data.Rows)
            {
                result.Add(new NewRoundQuestion(row));
            }
            return result;
        }
    }
}
