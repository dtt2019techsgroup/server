﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRound
{
    class NewRoundQuestion
    {
        public Byte RowNo { get; set; }
        public Byte ColNo { get; set; }
        public Byte CellType { get; set; }
        public String Question { get; set; }
        public String Answer { get; set; }


        public NewRoundQuestion() { }

        public NewRoundQuestion(Byte rowNo, Byte colNo, Byte cellType, String question, String answer)
        {
            this.RowNo = rowNo;
            this.ColNo = colNo;
            this.CellType = cellType;
            this.Question = question;
            this.Answer = answer;
        }

        public NewRoundQuestion(DataRow row)
        {
            this.RowNo = (Byte) row["RowNo"];
            this.ColNo = (Byte) row["ColNo"];
            this.CellType = (Byte) row["CellType"];
            this.Question = row["Question"].ToString();
            this.Answer = row["Answer"].ToString();
        }
    }
}
