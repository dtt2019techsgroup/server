﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewRound
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<NewRoundQuestion> newRoundQuestions = NewRoundQuestionDAO.Instance.getQuestions();
            Label[,] txt = new Label[8,9];

            int[,] value = new int[8, 9];


            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    value[i, j] = 0;

                    txt[i, j] = new Label();
                    txt[i, j].Background = Brushes.LightCyan;
                    txt[i, j].FontSize = 30;
                    txt[i, j].VerticalAlignment = VerticalAlignment.Stretch;
                    txt[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                    txt[i, j].HorizontalAlignment = HorizontalAlignment.Stretch;
                    txt[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                    Grid.SetRow(txt[i, j], i);
                    Grid.SetColumn(txt[i, j], j);
                }
            }

            foreach (NewRoundQuestion n in newRoundQuestions)
            {
                if (n.CellType == 10 || n.CellType == 11)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = Brushes.Aquamarine;
                    if (n.CellType == 11)
                    {
                        txt[n.RowNo - 1, n.ColNo - 1].Content = "x";
                        value[n.RowNo - 1, n.ColNo - 1] = 1;
                    }
                }
                else if (n.CellType == 20 || n.CellType == 21)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = Brushes.Moccasin;
                    if (n.CellType == 21)
                    {
                        txt[n.RowNo - 1, n.ColNo - 1].Content = "x";
                        value[n.RowNo - 1, n.ColNo - 1] = 1;
                    }
                }
                else if (n.CellType == 30)
                {
                    txt[n.RowNo - 1, n.ColNo - 1].Background = Brushes.HotPink;
                }
            }

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (txt[i, j].Background == Brushes.LightCyan)
                    {
                        for(int k = 0; k < numberOfDots(value, i, j); k++)
                        {
                            txt[i, j].Content += "o";
                        }
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    Matrix.Children.Add(txt[i, j]);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        public int numberOfDots(int[,] a, int row_i, int col_i)
        {
            int n = 0;

            #region Chặn 4 góc
            if (row_i == 0 && col_i == 0)
            {
                if (a[0, 1] == 1)
                    n++;
                if (a[1, 0] == 1)
                    n++;
                if (a[1, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 0 && col_i == 8)
            {
                if (a[0, 8] == 1)
                    n++;
                if (a[1, 8] == 1)
                    n++;
                if (a[1, 7] == 1)
                    n++;
                return n;
            }
            else if (row_i == 7 && col_i == 0)
            {
                if (a[6, 0] == 1)
                    n++;
                if (a[7, 1] == 1)
                    n++;
                if (a[6, 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 7 && col_i == 8)
            {
                if (a[6, 8] == 1)
                    n++;
                if (a[7, 7] == 1)
                    n++;
                if (a[6, 7] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Chặn 4 cạnh
            else if (row_i == 0)
            {
                if (a[0, col_i - 1] == 1)
                    n++;
                if (a[0, col_i + 1] == 1)
                    n++;
                if (a[1, col_i - 1] == 1)
                    n++;
                if (a[1, col_i] == 1)
                    n++;
                if (a[1, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (row_i == 7)
            {
                if (a[7, col_i - 1] == 1)
                    n++;
                if (a[7, col_i + 1] == 1)
                    n++;
                if (a[6, col_i - 1] == 1)
                    n++;
                if (a[6, col_i] == 1)
                    n++;
                if (a[6, col_i + 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 0)
            {
                if (a[row_i - 1, 0] == 1)
                    n++;
                if (a[row_i + 1, 0] == 1)
                    n++;
                if (a[row_i - 1, 1] == 1)
                    n++;
                if (a[row_i, 1] == 1)
                    n++;
                if (a[row_i + 1, 1] == 1)
                    n++;
                return n;
            }
            else if (col_i == 8)
            {
                if (a[row_i - 1, 8] == 1)
                    n++;
                if (a[row_i + 1, 8] == 1)
                    n++;
                if (a[row_i - 1, 7] == 1)
                    n++;
                if (a[row_i, 7] == 1)
                    n++;
                if (a[row_i + 1, 7] == 1)
                    n++;
                return n;
            }
            #endregion

            #region Xét 8 hướng
            else
            {
                if (a[row_i - 1, col_i - 1] == 1)
                    n++;
                if (a[row_i - 1, col_i] == 1)
                    n++;
                if (a[row_i - 1, col_i + 1] == 1)
                    n++;
                if (a[row_i, col_i - 1] == 1)
                    n++;
                if (a[row_i, col_i + 1] == 1)
                    n++;
                if (a[row_i + 1, col_i - 1] == 1)
                    n++;
                if (a[row_i + 1, col_i] == 1)
                    n++;
                if (a[row_i + 1, col_i + 1] == 1)
                    n++;
                return n;
            }
            #endregion
        }
    }
}
