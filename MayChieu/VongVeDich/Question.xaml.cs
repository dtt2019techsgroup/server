﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongVeDich
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : Window
    {
        public string[] Questions = new string[3];
        private bool Star = false;
        private bool Steal = false;
        private int Package, CurrentPoint;

        public Question()
        {
            InitializeComponent();
            //Load Point 
            TxtPoint.Text = "50";

        }

        private void Btn40_Click(object sender, RoutedEventArgs e)
        {
            LoadQuestion(40);
        }
        private void Btn60_Click(object sender, RoutedEventArgs e)
        {
            LoadQuestion(60);
        }
        private void Btn80_Click(object sender, RoutedEventArgs e)
        {
            LoadQuestion(80);
        }
        private void LoadQuestion(int Point)
        {
            switch (Point)
            {
                case 40:
                    Package = 40;
                    Btn60.Visibility = Visibility.Hidden;
                    Btn80.Visibility = Visibility.Hidden;
                    Questions[0] = "This is a 10-point-question";
                    Questions[1] = "This is a 10-point-question";
                    Questions[2] = "This is a 20-point-question";
                    break;
                case 60:
                    Package = 60;
                    Btn40.Visibility = Visibility.Hidden;
                    Btn80.Visibility = Visibility.Hidden;
                    Questions[0] = "This is a 10-point-question";
                    Questions[1] = "This is a 20-point-question";
                    Questions[2] = "This is a 30-point-question";
                    break;
                case 80:
                    Package = 80;
                    Btn40.Visibility = Visibility.Hidden;
                    Btn60.Visibility = Visibility.Hidden;
                    Questions[0] = "This is a 20-point-question";
                    Questions[1] = "This is a 30-point-question";
                    Questions[2] = "This is a 30-point-question";
                    break;
                

            }
        }

        private void BtnTimer_Click(object sender, RoutedEventArgs e)
        {
            Video5s.LoadedBehavior = MediaState.Play;
        }

        private void BtnQues1_Click(object sender, RoutedEventArgs e)
        {
            TxtQuestion.Text = Questions[0];
            if (Package == 40 || Package == 60) CurrentPoint = 10;
            else CurrentPoint = 20;
            
        }

        private void BtnQues2_Click(object sender, RoutedEventArgs e)
        {
            TxtQuestion.Text = Questions[1];
            if (Package == 40) CurrentPoint = 10;
            else if (Package == 60) CurrentPoint = 20;
            else CurrentPoint = 30;
        }

        private void BtnQues3_Click(object sender, RoutedEventArgs e)
        {
            TxtQuestion.Text = Questions[2];
            if (Package == 40) CurrentPoint = 20;
            else CurrentPoint = 30;
            
        }

        private void CheckboxStar_Checked(object sender, RoutedEventArgs e)
        {
            Star = true;
        }

        private void CheckboxSteal_Checked(object sender, RoutedEventArgs e)
        {
            Steal = true;
        }

        private void BtnFalse_Click(object sender, RoutedEventArgs e)
        {
            if (Steal)
            {
                CurrentPoint /= 2;
                int Point = int.Parse(TxtPoint.Text);
                Point -= CurrentPoint;
                TxtPoint.Text = Point.ToString();
            }
        }

        private void BtnSummaryPoint_Click(object sender, RoutedEventArgs e)
        {
            SummaryPoint Board = new SummaryPoint();
            this.Close();
            Board.Show();
        }

        private void BtnTrue_Click(object sender, RoutedEventArgs e)
        {
            if (Star)
            {
                CurrentPoint *= 2;
            }
            int Point = int.Parse(TxtPoint.Text);
            Point += CurrentPoint;
            TxtPoint.Text = Point.ToString();
        }
    }
}
