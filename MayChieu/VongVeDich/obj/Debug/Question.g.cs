﻿#pragma checksum "..\..\Question.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A5C1A18EDE6E0389617EF54EC10DDAC4812B51A4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using VongVeDich;


namespace VongVeDich {
    
    
    /// <summary>
    /// Question
    /// </summary>
    public partial class Question : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtName;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn40;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn60;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn80;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtQuestion;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnQues1;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnQues2;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnQues3;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtPoint;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MediaElement Video5s;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnTimer;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnTrue;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnFalse;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CheckboxStar;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CheckboxSteal;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\Question.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSummaryPoint;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/VongVeDich;component/question.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Question.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TxtName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.Btn40 = ((System.Windows.Controls.Button)(target));
            
            #line 11 "..\..\Question.xaml"
            this.Btn40.Click += new System.Windows.RoutedEventHandler(this.Btn40_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Btn60 = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\Question.xaml"
            this.Btn60.Click += new System.Windows.RoutedEventHandler(this.Btn60_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Btn80 = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\Question.xaml"
            this.Btn80.Click += new System.Windows.RoutedEventHandler(this.Btn80_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TxtQuestion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.BtnQues1 = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\Question.xaml"
            this.BtnQues1.Click += new System.Windows.RoutedEventHandler(this.BtnQues1_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BtnQues2 = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\Question.xaml"
            this.BtnQues2.Click += new System.Windows.RoutedEventHandler(this.BtnQues2_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BtnQues3 = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\Question.xaml"
            this.BtnQues3.Click += new System.Windows.RoutedEventHandler(this.BtnQues3_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.TxtPoint = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Video5s = ((System.Windows.Controls.MediaElement)(target));
            return;
            case 11:
            this.BtnTimer = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\Question.xaml"
            this.BtnTimer.Click += new System.Windows.RoutedEventHandler(this.BtnTimer_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BtnTrue = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\Question.xaml"
            this.BtnTrue.Click += new System.Windows.RoutedEventHandler(this.BtnTrue_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.BtnFalse = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\Question.xaml"
            this.BtnFalse.Click += new System.Windows.RoutedEventHandler(this.BtnFalse_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.CheckboxStar = ((System.Windows.Controls.CheckBox)(target));
            
            #line 23 "..\..\Question.xaml"
            this.CheckboxStar.Checked += new System.Windows.RoutedEventHandler(this.CheckboxStar_Checked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.CheckboxSteal = ((System.Windows.Controls.CheckBox)(target));
            
            #line 24 "..\..\Question.xaml"
            this.CheckboxSteal.Checked += new System.Windows.RoutedEventHandler(this.CheckboxSteal_Checked);
            
            #line default
            #line hidden
            return;
            case 16:
            this.BtnSummaryPoint = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\Question.xaml"
            this.BtnSummaryPoint.Click += new System.Windows.RoutedEventHandler(this.BtnSummaryPoint_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

