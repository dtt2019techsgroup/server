﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongVeDich
{
    /// <summary>
    /// Interaction logic for OpeningVideo.xaml
    /// </summary>
    public partial class OpeningVideo : Window
    {
        public OpeningVideo()
        {
            InitializeComponent();
        }
        private void OpenSong_Unloaded(object sender, RoutedEventArgs e)
        {
            Question question = new Question();
            question.Show();
            this.Hide();
        }
    }
}
