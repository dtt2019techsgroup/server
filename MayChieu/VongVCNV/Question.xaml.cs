﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongVCNV
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : Window
    {
        public string[] Questions = new string[4];
        public Question()
        {
            InitializeComponent();
            //Load questions from server
            Questions[0] = "This is question 1";
            Questions[1] = "This is question 2";
            Questions[2] = "This is question 3";
            Questions[3] = "This is question 4";
        }

        private void BtnTimer_Click(object sender, RoutedEventArgs e)
        {
            Timer.LoadedBehavior = MediaState.Play;
        }
        private void Load_Question(object sender, RoutedEventArgs e)
        {
            FrameworkElement feSource = e.Source as FrameworkElement;
            switch (feSource.Name)
            {
                case "BtnLoadQues1":
                    TxtAnswer1.Background = new SolidColorBrush(Colors.Navy);
                    TxtAnswer1.Foreground = new SolidColorBrush(Colors.FloralWhite);
                    TxtQues.Text = Questions[0];
                    break;
                case "BtnLoadQues2":
                    TxtAnswer2.Background = new SolidColorBrush(Colors.Navy);
                    TxtAnswer2.Foreground = new SolidColorBrush(Colors.FloralWhite);
                    TxtQues.Text = Questions[1];
                    break;
                case "BtnLoadQues3":
                    TxtAnswer3.Background = new SolidColorBrush(Colors.Navy);
                    TxtAnswer3.Foreground = new SolidColorBrush(Colors.FloralWhite);
                    TxtQues.Text = Questions[2];
                    break;
                case "BtnLoadQues4":
                    TxtAnswer4.Background = new SolidColorBrush(Colors.Navy);
                    TxtAnswer4.Foreground = new SolidColorBrush(Colors.FloralWhite);
                    TxtQues.Text = Questions[3];
                    break;

            }
            e.Handled = true;

        }

        private void BtnShowAns_Click(object sender, RoutedEventArgs e)
        {
            Answers answer = new Answers();
            this.Hide();
            answer.Show();

        }

        private void BtnSummaryPoint_Click(object sender, RoutedEventArgs e)
        {
            SummaryPoint Board = new SummaryPoint();
            this.Close();
            Board.Show();
        }
    }
}
