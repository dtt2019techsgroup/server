﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongVCNV
{
    /// <summary>
    /// Interaction logic for Answers.xaml
    /// </summary>
    public partial class Answers : Window
    {
        public string[] Ans = new string[4];
        public Answers()
        {
            InitializeComponent();
            //Load Answers from server
            Ans[0] = "This is Answer 1";
            Ans[1] = "This is Answer 2";
            Ans[2] = "This is Answer 3";
            Ans[3] = "This is Answer 4";
            Answer1.Text = Ans[0];
            Answer2.Text = Ans[1];
            Answer3.Text = Ans[2];
            Answer4.Text = Ans[3];

        }   

        private void BtnPieces_Click(object sender, RoutedEventArgs e)
        {
            Pieces pieces = new Pieces();
            pieces.Show();
            this.Close();
        }
    }
}
