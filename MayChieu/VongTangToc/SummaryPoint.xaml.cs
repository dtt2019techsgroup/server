﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongTangToc
{
    /// <summary>
    /// Interaction logic for SummaryPoint.xaml
    /// </summary>
    public partial class SummaryPoint : Window
    {
        public string[] Points = new string[4];
        public SummaryPoint()
        {
            InitializeComponent();
            //fetch Point from Server and save into Points
            Point1.Text = "10";
            Point2.Text = "20";
            Point3.Text = "30";
            Point4.Text = "40";


        }
    }
}
