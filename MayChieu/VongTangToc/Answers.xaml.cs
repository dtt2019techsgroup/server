﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongTangToc
{
    /// <summary>
    /// Interaction logic for Answers.xaml
    /// </summary>
    public partial class Answers : Window
    {
        public Answers()
        {
            InitializeComponent();
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            Question question = new Question();
            question.Show();
            this.Hide();
        }

        private void BtnSummaryPoint_Click(object sender, RoutedEventArgs e)
        {
            SummaryPoint board = new SummaryPoint();
            this.Close();
            board.Show();
        }
    }
}
