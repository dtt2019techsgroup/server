﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VongKhoiDong
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : Window
    {
        public Queue<string> Questions = new Queue<string>(); //Fetch Questions from server
        public Queue<string> NameTS = new Queue<string>();//Name of Candidate
        public int Point;
        
        public Question()
        {
            InitializeComponent();
            Point = 0; 
            for (int i =1; i <= 4; i++)
            {
                NameTS.Enqueue("Thi sinh " + i);
            }
            for (int i =1; i <= 12; i++)
            {
                Questions.Enqueue("Question " + i);

            }
            
            TxtPoint.Text = Point.ToString();
        }

        private void BtnLoadQuestion_Click(object sender, RoutedEventArgs e)
        {
            string question = Questions.Dequeue();
            if (question!="")
            {
                TxtQuestion.Text = question;
            }
        }

        private void BtnIncrease_Click(object sender, RoutedEventArgs e)
        {
            Point += 10;
            TxtPoint.Text = Point.ToString();
        }

        private void BtnLoadName_Click(object sender, RoutedEventArgs e)
        {
            TxtName.Text = NameTS.Dequeue();
        }

        private void BtnPointSummary_Click(object sender, RoutedEventArgs e)
        {
            SummaryPoint Board = new SummaryPoint();
            this.Close();
            Board.Show();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            TxtPoint.Text = "0";
            TxtName.Text = "Name of Candidate";
            TxtQuestion.Text = "";
        }
    }
}
