﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReplicaSide.UserControl
{
    /// <summary>
    /// Interaction logic for KD.xaml
    /// </summary>
    public partial class KD
    {
		public const int DEFAULT_TIME = 60;
		public const int POINTS_PER_QUESTION = 10;
		public const int NUMBER_OF_QUESTION = 12;

        public int cur_time = DEFAULT_TIME;
        public int cur_score = 0;
        public int cur_question = 1;
		public System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public readonly MainWindow Main;

		public KD(MainWindow main)
        {
            InitializeComponent();

            Main = main;

            TbkQuestion.Text = String.Empty;
            TbkQuestionNo.Text = String.Empty;

			dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
			dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
		}

		private void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			if (cur_time > 0)
			{
				cur_time--;
				TbkTime.Text = cur_time.ToString();
			}
			else
			{
				dispatcherTimer.Stop();
                TbkTime.FontSize = 60;
                TbkTime.Text = "Hoàn thành";
				TbkQuestionNo.Text = String.Empty;
				TbkQuestion.Text = String.Empty;
			}
		}

        public void Start(string questionData)
        {
            dispatcherTimer.Start();
            TbkQuestionNo.Text = cur_question.ToString();
            TbkQuestion.Text = questionData;
        }

        public void True(string questionData)
        {
            cur_score += POINTS_PER_QUESTION;
            cur_question++;

            if (cur_question > NUMBER_OF_QUESTION)
            {
                dispatcherTimer.Stop();
                TbkTime.FontSize = 60;
                TbkTime.Text = "Hoàn thành";
                TbkScore.Text = cur_score.ToString();
                TbkQuestionNo.Text = String.Empty;
                TbkQuestion.Text = String.Empty;
                return;
            }

            TbkScore.Text = cur_score.ToString();
            TbkQuestionNo.Text = cur_question.ToString();
            TbkQuestion.Text = questionData;
        }

        public void False(string questionData)
        {
            cur_question++;

            if (cur_question > NUMBER_OF_QUESTION)
            {
                dispatcherTimer.Stop();
                TbkTime.FontSize = 60;
                TbkTime.Text = "Hoàn thành";
                TbkQuestionNo.Text = String.Empty;
                TbkQuestion.Text = String.Empty;
                return;
            }

            TbkQuestionNo.Text = cur_question.ToString();
            TbkQuestion.Text = questionData;
        }

        public void UpdatePoint(int currentStudentID, int points)
        {
            switch(currentStudentID)
            {
                case 1:
                    TbkPoint1.Text = points.ToString();
                    break;
                case 2:
                    TbkPoint2.Text = points.ToString();
                    break;
                case 3:
                    TbkPoint3.Text = points.ToString();
                    break;
                case 4:
                    TbkPoint4.Text = points.ToString();
                    break;
                default:
                    break;
            }
        }

        public void UpdateInfo(string name1, string point1, string name2, string point2, string name3, string point3, string name4, string point4)
        {
            TbkContestant1.Text = name1;
            TbkContestant2.Text = name2;
            TbkContestant3.Text = name3;
            TbkContestant4.Text = name4;
            TbkPoint1.Text = point1;
            TbkPoint2.Text = point2;
            TbkPoint3.Text = point3;
            TbkPoint4.Text = point4;
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            cur_time = DEFAULT_TIME;
            cur_score = 0;
            cur_question = 1;

            TbkTime.Text = cur_time.ToString();
            TbkQuestionNo.Text = String.Empty;
            TbkQuestion.Text = String.Empty;
        }
    }
}
