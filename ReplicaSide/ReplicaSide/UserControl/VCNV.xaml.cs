﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReplicaSide.UserControl
{
    /// <summary>
    /// Interaction logic for VCNV.xaml
    /// </summary>
    public partial class VCNV
    {
		const int DEFAULT_TIME = 15;
		const int NUMBER_OF_QUESTION = 4;

		int cur_time = DEFAULT_TIME;
		int cur_score = 0;
		int cur_question = 1;
		int count_key_pressed = 0;
		System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        bool IsObstacleFail = false;

        public readonly MainWindow Main;

		public VCNV(MainWindow main)
		{
			InitializeComponent();

            this.Main = main;

			//DispatcherTimer settings
			dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
			dispatcherTimer.Interval = new TimeSpan(0, 0, 1);

			//Set background color of border
			BdrObstacles.Background = new SolidColorBrush(Color.FromArgb(255, 19, 15, 64));
			BdrContestant.Background = new SolidColorBrush(Color.FromArgb(255, 19, 15, 64));
			BdrQuestion.Background = new SolidColorBrush(Color.FromArgb(255, 19, 15, 64));
			BdrScore.Background = new SolidColorBrush(Color.FromArgb(255, 19, 15, 64));
		}

		private void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			if (cur_time > 0)
			{
				//countdown
				cur_time--;
				TbkTime.Text = cur_time.ToString();
			}
			else
			{
				dispatcherTimer.Stop();
				TbkTime.Text = "Hết giờ";
                TbxAnswer.IsEnabled = false;
            }
		}

		public void CountDown()
		{
            if (IsObstacleFail)
                return;

			dispatcherTimer.Start();
            TbxAnswer.IsEnabled = true;
            FocusManager.SetFocusedElement(GridVCNV, TbxAnswer);
		}

		public void True(int questionID, string key)
		{
            if (IsObstacleFail)
                return;

            switch (questionID)
			{
				case 1:
					TbkObstacle1.Text = key;
					break;
				case 2:
					TbkObstacle2.Text = key;
					break;
				case 3:
					TbkObstacle3.Text = key;
					break;
				case 4:
					TbkObstacle4.Text = key;
					break;
				default:
					return;
			}
		}

		public void False(int questionID)
		{
            if (IsObstacleFail)
                return;

            switch (questionID)
			{
				case 1:
					TbkObstacle1.Foreground = Brushes.DarkGray;
					break;
				case 2:
					TbkObstacle2.Foreground = Brushes.DarkGray;
					break;
				case 3:
					TbkObstacle3.Foreground = Brushes.DarkGray;
					break;
				case 4:
					TbkObstacle4.Foreground = Brushes.DarkGray;
					break;
				default:
					return;
			}
		}

        public void ObstacleFail()
        {
            IsObstacleFail = true;
        }

		private void BtnReset_Click(object sender, RoutedEventArgs e)
		{
			cur_time = DEFAULT_TIME;
			cur_score = 0;
			cur_question = 1;

			TbkTime.Text = cur_time.ToString();
			TbkQuestionNo.Text = String.Empty;
			TbkQuestion.Text = String.Empty;
			TbkAnswer.Text = String.Empty;
			TbxAnswer.Text = String.Empty;

			TbkObstacle1.Text = "Hàng ngang 1";
			TbkObstacle1.Foreground = Brushes.White;
			TbkObstacle1.FontWeight = FontWeights.Normal;
			TbkObstacle2.Text = "Hàng ngang 2";
			TbkObstacle2.Foreground = Brushes.White;
			TbkObstacle2.FontWeight = FontWeights.Normal;
			TbkObstacle3.Text = "Hàng ngang 3";
			TbkObstacle3.Foreground = Brushes.White;
			TbkObstacle3.FontWeight = FontWeights.Normal;
			TbkObstacle4.Text = "Hàng ngang 4";
			TbkObstacle4.Foreground = Brushes.White;
			TbkObstacle4.FontWeight = FontWeights.Normal;
		}

        public void SelectQuestion(int questionID, string questionDetail)
        {
            if (IsObstacleFail)
                return;

            switch (questionID)
            {
                case 1:
                    cur_question = 1;
                    ObstacleText1(questionDetail);
                    break;
                case 2:
                    cur_question = 2;
                    ObstacleText2(questionDetail);
                    break;
                case 3:
                    cur_question = 3;
                    ObstacleText3(questionDetail);
                    break;
                case 4:
                    cur_question = 4;
                    ObstacleText4(questionDetail);
                    break;
                case 5:
                    cur_question = 5;
                    ObstacleText5(questionDetail);
                    break;
                default:
                    break;
            }
            TbkAnswer.Text = string.Empty;
        }

		private void ObstacleText1(string questionDetail)
		{
			cur_question = 1;
			cur_time = DEFAULT_TIME;
			TbkTime.Text = DEFAULT_TIME.ToString();
			TbkAnswer.Text = String.Empty;	//Text Block
			TbxAnswer.Text = String.Empty;	//Text Box

			TbkObstacle1.FontWeight = FontWeights.Bold;
			TbkObstacle1.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 0, 0));
			TbkQuestionNo.Text = "1";
			TbkQuestion.Text = questionDetail;
		}

		private void ObstacleText2(string questionDetail)
		{
			cur_question = 2;
			cur_time = DEFAULT_TIME;
			TbkTime.Text = DEFAULT_TIME.ToString();
			TbkAnswer.Text = String.Empty;
			TbxAnswer.Text = String.Empty;

			TbkObstacle2.FontWeight = FontWeights.Bold;
			TbkObstacle2.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 0, 0));
            TbkQuestionNo.Text = "2";
			TbkQuestion.Text = questionDetail;
		}

		private void ObstacleText3(string questionDetail)
		{
			cur_question = 3;
			cur_time = DEFAULT_TIME;
			TbkTime.Text = DEFAULT_TIME.ToString();
			TbkAnswer.Text = String.Empty;
			TbxAnswer.Text = String.Empty;

			TbkObstacle3.FontWeight = FontWeights.Bold;
			TbkObstacle3.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 0, 0));
            TbkQuestionNo.Text = "3";
			TbkQuestion.Text = questionDetail;
		}

		private void ObstacleText4(string questionDetail)
		{
			cur_question = 4;
			cur_time = DEFAULT_TIME;
			TbkTime.Text = DEFAULT_TIME.ToString();
			TbkAnswer.Text = String.Empty;
			TbxAnswer.Text = String.Empty;

			TbkObstacle4.FontWeight = FontWeights.Bold;
			TbkObstacle4.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 0, 0));
            TbkQuestionNo.Text = "4";
			TbkQuestion.Text = questionDetail;
		}

        private void ObstacleText5(string questionDetail)
        {
            cur_question = 5;
            cur_time = DEFAULT_TIME;
            TbkTime.Text = DEFAULT_TIME.ToString();
            TbkAnswer.Text = String.Empty;
            TbxAnswer.Text = String.Empty;

            TbkQuestionNo.Text = String.Empty;
            TbkQuestion.Text = questionDetail;
        }

        private void TbxAnswer_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				TbkAnswer.Text = TbxAnswer.Text;
				count_key_pressed++;
                Main.Client.Send(Main.position.ToString() + "0_01_" + TbxAnswer.Text);
			}
		}

        public void UpdateInfo(string name1, string point1, string name2, string point2, string name3, string point3, string name4, string point4)
        {
            TbkContestant1.Text = name1;
            TbkContestant2.Text = name2;
            TbkContestant3.Text = name3;
            TbkContestant4.Text = name4;
            TbkPoint1.Text = point1;
            TbkPoint2.Text = point2;
            TbkPoint3.Text = point3;
            TbkPoint4.Text = point4;
        }

        private void BtnTraLoiChuongNgaiVat_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn trả lời chướng ngại vật?", "Thông báo", MessageBoxButton.YesNo, MessageBoxImage.Question)
                == MessageBoxResult.Yes)
            {
                Main.Client.Send(Main.position.ToString() + "0_02");
                BtnTraLoiChuongNgaiVat.IsEnabled = false;
            }
        }
    }
}
