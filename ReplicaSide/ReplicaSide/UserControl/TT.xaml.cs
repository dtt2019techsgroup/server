﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReplicaSide.UserControl
{
    /// <summary>
    /// Interaction logic for TT.xaml
    /// </summary>
    public partial class TT
    {
		const int DEFAULT_TIME = 30;

        int cur_time = DEFAULT_TIME;
        int cur_score = 0;
		int cur_question = 0;
		int count_key_pressed = 0;

        public readonly MainWindow Main;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public TT(MainWindow main)
        {
            this.Main = main;

			InitializeComponent();

			//DispatcherTimer
			dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
			dispatcherTimer.Interval = new TimeSpan(0, 0, 1);

        }

        public void CountDown()
		{
			cur_time = DEFAULT_TIME;
			TbkTime.Text = cur_time.ToString();

			dispatcherTimer.Start();

            TbxAnswer.IsEnabled = true;

            FocusManager.SetFocusedElement(GridTT, TbxAnswer);
		}

        public void ShowQuestion(int questionID, string questionDetail)
		{
			cur_question = questionID;
			TbkQuestionNo.Text = cur_question.ToString();
			TbkQuestion.Text = questionDetail;
            TbkAnswer.Text = String.Empty;
            TbxAnswer.Text = String.Empty;
		}

		private void BtnReset_Click(object sender, RoutedEventArgs e)
		{
			TbkQuestion.Text = String.Empty;
			TbkQuestionNo.Text = String.Empty;
			TbkTime.Text = DEFAULT_TIME.ToString();
			TbkAnswer.Text = String.Empty;
		}

		private void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			if (cur_time > 0)
			{
				cur_time -= 1;
                TbkTime.Text = cur_time.ToString();
            }
			else
			{
				dispatcherTimer.Stop();
				TbkTime.Text = "Hết giờ";
                TbxAnswer.IsEnabled = false;
			}
		}

        private void TbxAnswer_KeyDown(object sender, KeyEventArgs e)
		{
			if ((e.Key == Key.Return) && (TbxAnswer.IsEnabled == true))
			{
				TbkAnswer.Text = TbxAnswer.Text;
				count_key_pressed++;
                Main.Client.Send(Main.position.ToString() + "0_03_" + TbkAnswer.Text + "_" + (30 - cur_time).ToString());
			}
		}

        public void UpdateInfo(string name1, string point1, string name2, string point2, string name3, string point3, string name4, string point4)
        {
            TbkContestant1.Text = name1;
            TbkContestant2.Text = name2;
            TbkContestant3.Text = name3;
            TbkContestant4.Text = name4;
            TbkPoint1.Text = point1;
            TbkPoint2.Text = point2;
            TbkPoint3.Text = point3;
            TbkPoint4.Text = point4;
        }

        private void Btn40pts_Click(object sender, RoutedEventArgs e)
        {
            cur_score += 40;
            TbkScore.Text = cur_score.ToString();
        }

        private void Btn30pts_Click(object sender, RoutedEventArgs e)
        {
            cur_score += 30;
            TbkScore.Text = cur_score.ToString();
        }

        private void Btn20pts_Click(object sender, RoutedEventArgs e)
        {
            cur_score += 20;
            TbkScore.Text = cur_score.ToString();
        }

        private void Btn10pts_Click(object sender, RoutedEventArgs e)
        {
            cur_score += 10;
            TbkScore.Text = cur_score.ToString();
        }
    }
}
