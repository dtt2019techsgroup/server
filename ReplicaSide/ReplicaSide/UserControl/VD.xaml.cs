﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReplicaSide.UserControl
{
    /// <summary>
    /// Interaction logic for VD.xaml
    /// </summary>
    public partial class VD
    {
        public const int DEFAULT_TIME = 5;

        public int cur_time = DEFAULT_TIME;

        public System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public readonly MainWindow Main;

        public VD(MainWindow main)
        {
            InitializeComponent();

            Main = main;

            TbkQuestion.Text = String.Empty;
            TbkQuestionNo.Text = String.Empty;

            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (cur_time > 0)
            {
                cur_time--;
                TbkTime.Text = cur_time.ToString();
            }
            else
            {
                dispatcherTimer.Stop();
                BtnGianhQuyenTraLoi.IsEnabled = false;
                TbkTime.FontSize = 60;
                TbkTime.Text = "Hết giờ";
            }
        }

        public void Start()
        {
            TbkTime.FontSize = 100;
            cur_time = DEFAULT_TIME;
            TbkTime.Text = cur_time.ToString();
            dispatcherTimer.Start();
            BtnGianhQuyenTraLoi.IsEnabled = true;
            BtnGianhQuyenTraLoi.Foreground = Brushes.White;
        }

        public void ShowQuestion(string question)
        {
            TbkQuestion.Text = question;
        }

        public void HideQuestion()
        {
            TbkTime.Text = DEFAULT_TIME.ToString();
            TbkQuestion.Text = String.Empty;
            TbkQuestionNo.Text = String.Empty;
        }

        public void True(int point)
        {

        }

        public void False(string questionData)
        {
            
        }

        public void UpdatePoint(int currentStudentID, int points)
        {
            switch (currentStudentID)
            {
                case 1:
                    TbkPoint1.Text = points.ToString();
                    break;
                case 2:
                    TbkPoint1.Text = points.ToString();
                    break;
                case 3:
                    TbkPoint1.Text = points.ToString();
                    break;
                case 4:
                    TbkPoint1.Text = points.ToString();
                    break;
                default:
                    break;
            }
        }

        public void UpdateInfo(string name1, string point1, string name2, string point2, string name3, string point3, string name4, string point4)
        {
            TbkContestant1.Text = name1;
            TbkContestant2.Text = name2;
            TbkContestant3.Text = name3;
            TbkContestant4.Text = name4;
            TbkPoint1.Text = point1;
            TbkPoint2.Text = point2;
            TbkPoint3.Text = point3;
            TbkPoint4.Text = point4;
        }

        private void BtnGianhQuyenTraLoi_Click(object sender, RoutedEventArgs e)
        {
            BtnGianhQuyenTraLoi.IsEnabled = false;
            Main.Client.Send(Main.position.ToString() + "0_00");
        }
    }
}
