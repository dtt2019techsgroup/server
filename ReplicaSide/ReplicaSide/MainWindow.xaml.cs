﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReplicaSide.Utilities;

namespace ReplicaSide
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ClientSettings Client { get; set; }

        public UserControl.KD ucKD;
        public UserControl.VCNV ucVCNV;
        public UserControl.TT ucTT;
        public UserControl.GM ucGM;
        public UserControl.VD ucVD;

        public ManHinhDiem manHinhDiem;

        public int position;
        public int currentRound;

        public MainWindow()
        {
            InitializeComponent();

            currentRound = 0;

            ucKD = new UserControl.KD(this);
            ucVCNV = new UserControl.VCNV(this);
            ucTT = new UserControl.TT(this);
            ucGM = new UserControl.GM(this);
            ucVD = new UserControl.VD(this);

            manHinhDiem = new ManHinhDiem(this);

            //BtnTT.PerformClick();
            string directory = Directory.GetCurrentDirectory();
            string[] text = System.IO.File.ReadAllLines(directory + @"\IP.txt");
            string replica_ip = string.Empty;
            int replica_port = 0;
            string master_ip = string.Empty;
            int master_port = 0;
            if (text.Count() == 2)
            {
                replica_ip = text[0].Split(':')[0];
                replica_port = int.Parse(text[0].Split(':')[1]);
                master_ip = text[1].Split(':')[0];
                master_port = int.Parse(text[1].Split(':')[1]);
            }
            else if (text.Count() == 0)
            {
                replica_ip = "127.0.0.1";
                replica_port = 59001;
                master_ip = "127.0.0.1";
                master_port = 59000;
            }
            else
            {
                Application.Current.Shutdown();
            }

            Client = new ClientSettings(replica_ip, replica_port);
            Client.Connected += Client_Connected;
            Client.Connect(master_ip, master_port);
            Client.Received += _client_Received;
            Client.Disconnected += Client_Disconnected;

            if (replica_port == 59001)
                position = 1;
            else if (replica_port == 59002)
                position = 2;
            else if (replica_port == 59003)
                position = 3;
            else if (replica_port == 59004)
                position = 4;
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            
        }

        private static void Client_Disconnected(ClientSettings cs)
        {
            //Need something here to handle the disconnection in order to prevent the system crash
        }

        public void _client_Received(ClientSettings cs, string received)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                var cmd = received.Split('_');

                switch (cmd[0])
                {
                    case "01":
                        {
                            switch (cmd[1])
                            {
                                case "00":
                                    currentRound = 1;
                                    main.Content = null;
                                    main.Content = ucKD;
                                    break;
                                case "01":
                                    ucKD.Start(cmd[2]);
                                    break;
                                case "02":
                                    ucKD.True(cmd[2]);
                                    break;
                                case "03":
                                    ucKD.False(cmd[2]);
                                    break;
                                case "04":
                                    ucKD.UpdatePoint(int.Parse(cmd[2]), int.Parse(cmd[3]));
                                    break;
                            }
                        }
                        break;
                    case "02":
                        {
                            switch (cmd[1])
                            {
                                case "00":
                                    currentRound = 2;
                                    main.Content = null;
                                    main.Content = ucVCNV;
                                    break;
                                case "01":
                                    ucVCNV.SelectQuestion(int.Parse(cmd[2]), cmd[3]);
                                    break;
                                case "02":
                                    ucVCNV.CountDown();
                                    break;
                                case "03":
                                    ucVCNV.True(int.Parse(cmd[2]), cmd[3]);
                                    break;
                                case "04":
                                    ucVCNV.False(int.Parse(cmd[2]));
                                    break;
                                case "05":
                                    ucVCNV.ObstacleFail();
                                    break;
                            }
                        }
                        break;
                    case "03":
                        {
                            switch (cmd[1])
                            {
                                case "00":
                                    currentRound = 3;
                                    main.Content = null;
                                    main.Content = ucTT;
                                    break;
                                case "01":
                                    ucTT.ShowQuestion(int.Parse(cmd[2]), cmd[3]);
                                    break;
                                case "02":
                                    ucTT.CountDown();
                                    break;
                            }
                            break;
                        }
                    case "04":
                        {
                            switch (cmd[1])
                            {
                                case "00":
                                    currentRound = 4;
                                    main.Content = null;
                                    main.Content = ucGM;
                                    break;
                                case "01":
                                    ucGM.ShowQuestion(cmd[2]);
                                    break;
                                case "02":
                                    ucGM.CountDown(int.Parse(cmd[2]));
                                    break;
                            }
                            break;
                        }
                    case "05":
                        {
                            switch (cmd[1])
                            {
                                case "00":
                                    currentRound = 5;
                                    main.Content = null;
                                    main.Content = ucVD;
                                    break;
                                case "01":
                                    ucVD.ShowQuestion(cmd[2]);
                                    break;
                                case "02":
                                    ucVD.Start();
                                    break;
                                case "03":
                                    ucVD.HideQuestion();
                                    break;
                                case "04":
                                    ucVD.BtnGianhQuyenTraLoi.IsEnabled = false;
                                    break;
                            }
                        }
                        break;
                    case "06":
                        switch (currentRound)
                        {
                            case 1:
                                ucKD.UpdateInfo(cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8]);
                                break;
                            case 2:
                                ucVCNV.UpdateInfo(cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8]);
                                break;
                            case 3:
                                ucTT.UpdateInfo(cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8]);
                                break;
                            case 4:
                                ucGM.UpdateInfo(cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8]);
                                break;
                            case 5:
                                ucVD.UpdateInfo(cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8]);
                                break;
                        }
                        switch (position)
                        {
                            case 1:
                                manHinhDiem.TbkName.Text = cmd[1].ToString().ToUpper();
                                manHinhDiem.TbkPoints.Text = cmd[2].ToString();
                                break;
                            case 2:
                                manHinhDiem.TbkName.Text = cmd[3].ToString().ToUpper();
                                manHinhDiem.TbkPoints.Text = cmd[4].ToString();
                                break;
                            case 3:
                                manHinhDiem.TbkName.Text = cmd[5].ToString().ToUpper();
                                manHinhDiem.TbkPoints.Text = cmd[6].ToString();
                                break;
                            case 4:
                                manHinhDiem.TbkName.Text = cmd[7].ToString().ToUpper();
                                manHinhDiem.TbkPoints.Text = cmd[8].ToString();
                                break;
                        }
                        break;

                }
            });
        }

        private void BtnKD_Click(object sender, RoutedEventArgs e)
        {
            main.Content = ucKD;
        }

        private void BtnVCNV_Click(object sender, RoutedEventArgs e)
        {
            main.Content = ucVCNV;
        }

        private void BtnTT_Click(object sender, RoutedEventArgs e)
        {
            main.Content = ucTT;
        }

        private void BtnVD_Click(object sender, RoutedEventArgs e)
        {
            main.Content = ucVD;
        }

        private void BtnThoat_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            manHinhDiem.Show();
        }
    }
}

namespace System.Windows.Controls
{
    public static class MyExt
    {
        public static void PerformClick(this Button btn)
        {
            btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }
    }
}